# Maintainer: 
pkgname=openrc
pkgver=0.24.1
pkgrel=14
pkgdesc="OpenRC manages the services, startup and shutdown of a host"
url="https://github.com/OpenRC/openrc"
arch="all"
options="!check"  # https://bugs.gentoo.org/575958
license="BSD-2-Clause"
depends="psmisc /sbin/init"
makedepends="bsd-compat-headers"
subpackages="$pkgname-doc $pkgname-dev"
install="$pkgname.post-install $pkgname.post-upgrade"
source="$pkgname-$pkgver.tar.gz::https://github.com/OpenRC/openrc/archive/$pkgver.tar.gz

	0001-call-sbin-mkmntdirs-in-localmount-OpenRC-service.patch
	0002-force-root-be-rw-before-localmount.patch
	0003-sysctl-add-compatibility-for-busybox-sysctl.patch
	0004-hide-error-when-migrating-var-run-to-run.patch
	0005-rc-pull-in-sysinit-and-boot-as-stacked-levels-when-n.patch
	hwclock-grep.patch

	modern-gcc.patch
	openrc-configuration.patch

	openrc.logrotate
	hostname.initd
	hwdrivers.initd
	modules.initd
	modloop.initd
	modloop.confd
	sysfsconf.initd
	"

prepare() {
	default_prepare
	sed -i -e '/^sed/d' "$builddir"/pkgconfig/Makefile
}

build() {
	export BRANDING="Adélie Linux"
	make LIBEXECDIR=/lib/rc MKNET=no
}

check() {
	make check
}

package() {
	make LIBEXECDIR=/lib/rc DESTDIR="$pkgdir/" install

	# we cannot have anything turned on by default
	mkdir -p "$pkgdir"/usr/share/openrc || true
	mv "$pkgdir"/etc/runlevels "$pkgdir"/usr/share/openrc/runlevels

	# we override some of the scripts
	for i in "$srcdir"/*.initd; do
		j=${i##*/}
		install -Dm755 $i "$pkgdir"/etc/init.d/${j%.initd}
	done

	# we override some of the conf.d files
	for i in "$srcdir"/*.confd; do
		j=${i##*/}
		install -Dm644 $i "$pkgdir"/etc/conf.d/${j%.confd}
	done

	# we do not use their default networking stuff
	rm "$pkgdir"/etc/conf.d/network
	rm "$pkgdir"/etc/init.d/network

	# we ship these in system/kbd
	for i in consolefont keymaps; do
		rm "$pkgdir"/etc/conf.d/$i
		rm "$pkgdir"/etc/init.d/$i
	done

	# we use gettys-openrc for TTY
	rm "$pkgdir"/etc/init.d/agetty

	install -Dm644 "$srcdir/$pkgname.logrotate" "$pkgdir/etc/logrotate.d/$pkgname"
	install -d "$pkgdir"/etc/local.d "$pkgdir"/run
}

sha512sums="8d2aec029cb675ae5d446fe4a2f9613fac2fc5ea74b091d93e62b1f7bd4f8e3a96893bafa39a301129dad4623cc30acdcfd9e383a74f98c69f29820adb6d9aa0  openrc-0.24.1.tar.gz
71fce711adbcb411189a089f1d49567c50348e12c42b7a9c9b582dae5d18051f88ccf81c768337e87d6792d953e84d1e8b93d7978a1947d7d20ef3b1cd330875  0001-call-sbin-mkmntdirs-in-localmount-OpenRC-service.patch
aedf77f9159fefb4bd5f30a29a33b6aedbc986c9a0f993aa928cc79fbe24aac76bd9e5974dcce52ee8736c22d7e90375930d4bb5c66af3519d8e46590df00fe1  0002-force-root-be-rw-before-localmount.patch
5494f5ee520b26219048586882919dc0b45e839bd9543edc974f7b283d7b9326dc0362f9d2608b6acf043613399ea16c8287b854da874f49801963165e239dba  0003-sysctl-add-compatibility-for-busybox-sysctl.patch
d54630d40a2d6b10a325cb012d4efcda997a60c008ca953ce5d60059d3f267308a59dabddf93a5fc0d301aa91967137d144effbe5f574394af768ce4ebc48738  0004-hide-error-when-migrating-var-run-to-run.patch
39a35c54ec9112fe84c901ed155a711cec8e194af02d5483ee60b80743dab12391e6fdc7b3da2f86844dd4edcf53e681ff95bd4d6fa1101a89ce54dce2ddbb7c  0005-rc-pull-in-sysinit-and-boot-as-stacked-levels-when-n.patch
43355d72ad82858787e240450ae556cdd40aa9a3719545991d76719df80cc5c851c21374b339a6953f0344f1468e08e99c4da11334dc0b72647f4aa1db08558c  hwclock-grep.patch
aeb63843b69bf5dc3e513a9b0fa1c5631b509b2ecc9e127d6d5e1ac18254375a45afb6e82429d2c559e29aef70e01286b82cabbdc7c3907a5cce6cdfb32be4f9  modern-gcc.patch
634c09a36d542a69b147a292418fa99a5d071d7a384bd2d2b30917a986cdc82ef4e96e380bc3a154cb0709c57d5308320c30fd6973972d6e6d7f8ac5b2e31bea  openrc-configuration.patch
12bb6354e808fbf47bbab963de55ee7901738b4a912659982c57ef2777fff9a670e867fcb8ec316a76b151032c92dc89a950d7d1d835ef53f753a8f3b41d2cec  openrc.logrotate
99b542c0903ad6874b8c308b2e0660a4fe2ff9db962dfec65325cd12c368873a2ae800d5e6d42dc4deff775e1d5c0068869eb72581f7ab16e88d5738afe1d3dd  hostname.initd
c06eac7264f6cc6888563feeae5ca745aae538323077903de1b19102e4f16baa34c18b8c27af5dd5423e7670834e2261e9aa55f2b1ec8d8fdc2be105fe894d55  hwdrivers.initd
b04058ec630e19de0bafefe06198dc1bff8c8d5d2c89e4660dd83dda8bb82a76cdb1d8661cce88e4a406aa6b4152e17efff52d3eb18ffaec0751d0b6cdbcc48a  modules.initd
328e1f4cf3d9542ae47a517b900b57f383e0c0e24e24c52437667b72fa11d7986329dac176168ca9b6729cb412286f1ad8327771c85aa821c8b534b1bd860712  modloop.initd
aa702a7da8e6c0e5d8738febaf6b4e4cb021b30ce5c1809b530abf2b36739079446b16fc054740da8d86ed099942cf5deed6597cedb64c058f3def587a8b4689  modloop.confd
02eb7a0b430f0dd985dc1a6cdcca41b1f0dca1a2c164364713ff6706511884295a1ccbd51c87686ffc197ca5cd093be5404fba98d21cf4e4076535706a67f106  sysfsconf.initd"
