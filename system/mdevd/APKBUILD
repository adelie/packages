# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=mdevd
pkgver=0.1.6.5
pkgrel=0
pkgdesc="A small uevent manager daemon"
url="https://skarnet.org/software/mdevd/"
arch="all"
options="!check"  # No test suite.
license="ISC"
_skalibs_version=2.14.3
makedepends="skalibs-dev>=$_skalibs_version"
subpackages="$pkgname-doc $pkgname-openrc"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz mdev.conf mdevd.run mdevd.initd"

build() {
	./configure --enable-allstatic --enable-static-libc
	make
}

package() {
	make DESTDIR="$pkgdir" install
	mkdir -p -m 0755 "$pkgdir/etc/init.d" "$pkgdir/usr/share/doc"
	cp -f "$srcdir/mdev.conf" "$pkgdir/etc/"
	chmod 0644 "$pkgdir/etc/mdev.conf"
	cp -f "$srcdir/mdevd.initd" "$pkgdir/etc/init.d/mdevd"
	chmod 0755 "$pkgdir/etc/init.d/mdevd"
	cp -a "$builddir/doc" "$pkgdir/usr/share/doc/$pkgname"
}

openrc() {
	rldir="$subpkgdir"/etc/runlevels/sysinit
	svcdir="$subpkgdir/etc/s6-linux-init/current/run-image/service/mdevd"
        default_openrc
        mkdir -p "$rldir" "$svcdir"
	cp -f "$srcdir/mdevd.run" "$svcdir/run"
	chmod 0755 "$svcdir/run"
	echo 3 > "$svcdir/notification-fd"
	touch "$svcdir/down"
        ln -s ../../init.d/mdevd "$rldir/mdevd"
}

sha512sums="a6d4fd9802e46746b7c677bf1c5d496f7af46eb3d827248781989c0cfcd81e14b5eb7ab6af6aef9be6f3b3f1bc4fa6de1a61735f2fc2aa6b7dc994b0f73999f7  mdevd-0.1.6.5.tar.gz
f966d66366eac3b9b9eeb9b8523ea0924ada2b858c85ca0c0151d0fb374dfbf56c49ec2210d2d5ca19aa4d9f24371c85d777050eb8bf9f57821ec65704f18717  mdev.conf
427a5903fa2126060955dcce8144d59255a679c4973f2dbc3145a4d646e879fc241ebcaa53289498719d343c746fc376c41defa87932dcbe91192b2d6f4ed1c4  mdevd.run
e7599f51a4280243a5be459c6fad7eb8ba3b5f65fae8cad923ccca2addab55787819909fea311c998e1126e6802a81ab000ee6de7474f3245ce72521244c22ba  mdevd.initd"
