# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=adelie-base
pkgver=0.9.8
pkgrel=0
pkgdesc="The Adélie Linux Base System"
url="https://www.adelielinux.org/"
arch="noarch"
options="!check !fhs"
license="NCSA"
_core_depends="apk-tools adelie-keys musl-utils shadow
	shimmy grep procps /bin/sh
	coreutils diffutils findutils
	util-linux net-tools
	nvi sed psmisc less tzdata"
depends="adelie-core binutils curl debianutils file gettys-openrc
	kbd libarchive-tools mandoc patch sysklogd utmps zsh"
makedepends=""
makedepends_host="$depends"
subpackages="adelie-core:core $pkgname-doc $pkgname-posix $pkgname-lsb dev
	dev-kit:devkit docs lang symbols"
provides="alpine-base"
source="https://distfiles.adelielinux.org/source/$pkgname/$pkgname-$pkgver.tar.xz
	group
	passwd
	addgroup
	adduser
	"

build() {
	# taken from Alpine Base Layout: generate shadow
	awk -F: '{
		pw = ":!:"
		if ($1 == "root") { pw = "::" }
		print($1 pw ":0:::::")
	}' "$srcdir"/passwd > "$builddir"/tree/etc/shadow
	chmod 640 "$builddir"/tree/etc/shadow
}

package() {
	mkdir -p "$pkgdir"
	make install DESTDIR="$pkgdir"
	install -m644 "$srcdir"/group "$pkgdir"/etc/group
	install -m644 "$srcdir"/passwd "$pkgdir"/etc/passwd
	install -m640 -g shadow "$builddir"/tree/etc/shadow "$pkgdir"/etc/shadow
	chmod 1777 "$pkgdir"/var/tmp
	echo '' > "$pkgdir"/etc/shells

	# stupid inflatable stupid busybox
	for _cmd in adduser addgroup; do
		install -m755 "$srcdir"/$_cmd "$pkgdir"/usr/sbin/$_cmd
	done
}

core() {
	depends=$_core_depends
	pkgdesc="The Adélie Linux minimal runtime environment"
	replaces="adelie-base"
	mkdir -p "$subpkgdir"
	mv -i "$pkgdir"/* "$subpkgdir"/
}

doc() {
	DOCS="CONTRIBUTING.rst LICENSE README.rst"
	for _doc in $DOCS; do
		install -Dm644 "$srcdir"/$pkgname-$pkgver/$_doc \
			"$subpkgdir"/usr/share/doc/$pkgname/$_doc
	done
}

posix() {
	# We pull in vim for /usr/bin/ex, until apk has an alternatives system
	depends="adelie-base at bc cflow cxref ed fcron heirloom-devtools
		heirloom-pax mailx mawk sharutils uucp vim"
	pkgdesc="$pkgdesc - Additional POSIX tools"
	mkdir -p "$subpkgdir"
	return 0
}

lsb() {
	depends="adelie-base-posix gettext-tiny gzip libarchive-tools linux-pam
		make ncurses nspr nss"
	pkgdesc="$pkgdesc - Additional LSB tools"
	mkdir -p "$subpkgdir"
	return 0
}

devkit() {
	depends="build-tools libarchive-tools"
	pkgdesc="The Adélie Linux Development Kit"
	mkdir -p "$subpkgdir"
	return 0
}

dev() {
	depends=""
	pkgdesc="Metapackage that will magically install all development files for your system"
	mkdir -p "$subpkgdir"
	return 0
}

docs() {
	depends=""
	pkgdesc="Metapackage that will magically install all documentation for your system"
	mkdir -p "$subpkgdir"
	return 0
}

lang() {
	depends=""
	pkgdesc="Metapackage that will magically install all translations for your system"
	mkdir -p "$subpkgdir"
	return 0
}

symbols() {
	depends=""
	pkgdesc="Metapackage that will magically install all debugging symbols for your system"
	mkdir -p "$subpkgdir"
	return 0
}

sha512sums="2f79f6bcd4239280ab10f7e46fb6a03187c74c3637be9010ac3708e1ffbca6dc4a8925ee904a988b9da82a4fe95bc76239807910f7083ec9fffc053646210d0b  adelie-base-0.9.8.tar.xz
37260d9315fd8d8b0df3dfb5bd3e1035a47ba1e43f33aa195e3feca6d169da282c1b067ef4603e7e9acaedbfef8b58cf490c00bdca444d43c9d570355ad29eac  group
33aca5423feadd02cce1ac5598ab26ce4f10f2e0d955d953c5ec12b61e605d92b25c2449aec224923bf8affb4fa918458ab67e4292a29c357ad15b74ebc68183  passwd
f2437ebfbc83aa6eaeec3a111aae98751215cebfaec3991ccc511be8c8938778ae46f15e07c7306bd431d036bc4ba49b33b724b839c125bd882d2e93c0314ae8  addgroup
2f97bb4b24d9ea8d8d3001bb0cd0aac4b65caff75d25b172f9e925c55185183c2d4b54d9949cd8d43ca74c003a8590f2b98a119696075507f513b549fac94476  adduser"
