# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=xz
pkgver=5.4.2
pkgrel=0
pkgdesc="Library and command line tools for XZ and LZMA compressed files"
url="https://tukaani.org/xz/"
arch="all"
license="Public-Domain AND LGPL-2.1+"
depends=""
makedepends=""
subpackages="$pkgname-doc $pkgname-dev $pkgname-lang $pkgname-libs"
source="https://tukaani.org/xz/xz-$pkgver.tar.gz
	dont-use-libdir-for-pkgconfig.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libdir=/lib \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-rpath \
		--disable-werror

	sed 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' \
		-i libtool

	make
}

check() {
	make check
}

package() {
	make -C "$builddir" DESTDIR="$pkgdir" install
	install -Dm644 "$builddir"/COPYING \
		"$pkgdir"/usr/share/licenses/$pkgname
}

sha512sums="149f980338bea3d66de1ff5994b2b236ae1773135eda68b62b009df0c9dcdf5467f8cb2c06da95a71b6556d60bd3d21f475feced34d5dfdb80ee95416a2f9737  xz-5.4.2.tar.gz
54bbe1f8aae954d2550941f69a509e210d0f6bee2393494dcf445a14d14046953c125177b4cc9fa79ec55b81379dfe4ae0187f106abd2f3cc4331782a5c0b4fd  dont-use-libdir-for-pkgconfig.patch"
