# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: Adelie Platform Group <adelie-devel@lists.adelielinux.org>
# KEEP THIS IN SYNC with the other easy-kernel packages.
_kflavour=""
_patchver=2  # must match 1000-version.patch
_pkgname=easy-kernel$_kflavour
pkgver=6.6.58
pkgrel=1
pkgname=$_pkgname-$pkgver-mc$_patchver
pkgdesc="The Linux kernel, packaged for your convenience"
url="https://kernel.org/"
arch="all"
options="!check !dbg !strip !tracedeps"
license="GPL-2.0-only"
depends="kernel-boot"
makedepends="bc bison elfutils-dev flex gzip kmod lzop openssl-dev xz"
provides="easy-kernel$_kflavour=$pkgver-r$pkgrel"
replaces="easy-kernel-power8 easy-kernel-power8-64k"
subpackages="$_pkgname-modules-$pkgver-mc$_patchver:modules
	$_pkgname-src-$pkgver-mc$_patchver:src
	linux-headers:headers"
_pkgmajver=${pkgver%%.*}
_pkgminver=${pkgver%.*}
source="https://cdn.kernel.org/pub/linux/kernel/v${_pkgmajver}.x/linux-${_pkgminver}.tar.xz
	config-aarch64
	config-armv7
	config-m68k
	config-pmmx
	config-ppc
	config-ppc64
	config-sparc64
	config-x86_64
	kernel.h

	0100-linux-6.6.58.patch
	0122-link-security-restrictions.patch
	0124-bluetooth-keysize-check.patch
	0126-sign-file-libressl.patch
	0200-x86-compile.patch
	0202-parisc-disable-prctl.patch
	0204-sparc-warray-fix.patch
	0208-gcc14-objtool-fix.patch
	0250-expose-per-process-ksm.patch
	0252-rectify-ksm-inheritance.patch
	0260-reduce-swappiness.patch
	0262-boot-order.patch
	0300-correct-max98388-includes.patch
	0302-i915-gcc14-fix.patch
	0304-fix-powerbook6-5-audio.patch
	0400-reduce-pageblock-size-nonhugetlb.patch
	0402-mm-optimise-slub.patch
	0404-page-cache-not-found.patch
	0500-print-fw-info.patch
	0502-gcc9-kcflags.patch
	0504-update-zstd-to-v1_5_6.patch
	1000-version.patch

	no-require-gnu-tar.patch
	bsdtar.patch
	no-require-lilo.patch

	no-autoload-fb.conf

	mkimage-missing-not-fatal.patch
	"
builddir="$srcdir/linux-${_pkgminver}"

prepare() {
	default_prepare

	cd "$srcdir"
	cp config-$CARCH linux-${_pkgminver}/.config
	cp -pr linux-${_pkgminver} linux-src
	if [ -f $HOME/kernel_key.pem ]; then
		cp $HOME/kernel_key.pem "$builddir"/certs/signing_key.pem
	fi

	# see #1316; note that the "|| true" is required to prevent failure
	# if 'grep' matches no lines and has a nonzero return code
	cd "$srcdir/linux-src"
	_newconfig=$(make LDFLAGS="" listnewconfig | grep ^CONFIG_ || true)
	if [ -n "${_newconfig}" ]; then
		# the 'printf' is because 'error' doesn't accept '\n'
		error ".config does not account for NEW options:";
		printf "%s\n" "${_newconfig}";
		return 1;
	fi
}

build() {
	make LDFLAGS=""

	cd "$srcdir/linux-src"
	make LDFLAGS="" modules_prepare clean
	cp "$builddir/Module.symvers" .

	# Kernel bug: crtsavres.o is required to build modules, but modules_prepare doesn't create it.
	if [ $CARCH = ppc ]; then
		cp "$builddir/arch/powerpc/lib/crtsavres.o" arch/powerpc/lib/
	fi
}

package() {
	mkdir -p "$pkgdir"/boot
	make INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		install modules_install

	if [ -f "$pkgdir"/boot/vmlinuz ]; then
		mv "$pkgdir"/boot/vmlinuz \
			"$pkgdir"/boot/vmlinuz-$pkgver-mc$_patchver-easy$_kflavour
	fi
	if [ -f "$pkgdir"/boot/vmlinux ]; then
		mv "$pkgdir"/boot/vmlinux \
			"$pkgdir"/boot/vmlinux-$pkgver-mc$_patchver-easy$_kflavour
	fi

	if [ -f "$pkgdir"/boot/System.map ]; then
		mv "$pkgdir"/boot/System.map \
			"$pkgdir"/boot/System.map-$pkgver-mc$_patchver-easy$_kflavour
	fi

	case $CARCH in
	aarch64|arm*)	make INSTALL_PATH="$pkgdir"/boot dtbs_install ;;
	esac

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/easy-$pkgver-mc$_patchver$_kflavour/kernel.release
}

modules() {
	pkgdesc="Modules / device drivers for easy-kernel"
	provides="easy-kernel$_kflavour-modules=$pkgver-r$pkgrel"
	autodeps=0  # modules should not depend on src just for symlink
	mkdir -p "$subpkgdir"/lib
	mv "$pkgdir"/lib/modules "$subpkgdir"/lib
	rm "$subpkgdir"/lib/modules/$pkgver-mc$_patchver-easy$_kflavour/build
	ln -s "../../../usr/src/linux-$pkgver-mc$_patchver$_kflavour" \
		"$subpkgdir"/lib/modules/$pkgver-mc$_patchver-easy$_kflavour/build
	ln -s "../../../usr/src/linux-$pkgver-mc$_patchver$_kflavour" \
		"$subpkgdir"/lib/modules/$pkgver-mc$_patchver-easy$_kflavour/source

	mkdir -p "$subpkgdir"/etc/modprobe.d
	install -m644 "$srcdir"/no-autoload-fb.conf \
		"$subpkgdir"/etc/modprobe.d/no-autoload-fb.conf
}

headers() {
	pkgdesc="System headers provided by the Linux kernel"

	mkdir -p "$subpkgdir"/usr
	make -C "$builddir" headers
	find "$builddir"/usr/include -name '.*' -delete
	rm "$builddir"/usr/include/Makefile
	cp -rv "$builddir"/usr/include "$subpkgdir/usr"

	find "$subpkgdir/usr" \( -name .install -o -name ..install.cmd \) -exec \
		rm -f {} \;

	# provided by libdrm
	rm -rf "$subpkgdir"/usr/include/drm

	# needed for spl, VMware on x86, etc
	install -D -m644 "$builddir"/include/generated/utsrelease.h \
		"$subpkgdir"/usr/include/linux/utsrelease.h

	install -m644 "$srcdir"/kernel.h "$subpkgdir"/usr/include/linux/kernel.h
}

src() {
	pkgdesc="Kernel source code used to build the kernel"
	provides="easy-kernel$_kflavour-src=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir"/usr/src
	mv "$srcdir"/linux-src "$subpkgdir"/usr/src/linux-$pkgver-mc$_patchver$_kflavour
}

sha512sums="458b2c34d46206f9b4ccbac54cc57aeca1eaecaf831bc441e59701bac6eadffc17f6ce24af6eadd0454964e843186539ac0d63295ad2cc32d112b60360c39a35  linux-6.6.tar.xz
c16e726450602b271802b74bcfced1e2373c21b7cea8108bb722f9b4abcad44ae6e135a26296d76ad869c554360d1e043d163934592e5899e4c9514f179ac094  config-aarch64
237b7eff4ecab68600497761eb3716c158f1f3fc6e55f70559189cd15ecc381017cb0017a44c766eb016b50d091410e590312d5eaebebb2e6e13495f5602281b  config-armv7
af1495f11e7252ee65af2ce00b551b4715a9d663e1bf8cff1a1235ba922e7d0b9e90dcdacd469db1d7831d8580071f0f7fef24c89362afccde63058393d115f5  config-m68k
52d65ad133b66550e651b9165f647b094730e42d9d2e7036879f32b7c6f4f9c2320cddb0506efa832e7365156b70ed09cc66ed213bfbc67e94d60a916291e1aa  config-pmmx
fd1c8b0fa7bf0122fe670e54fa592ebb792415fba528011c83bdbfbbf16bd442339c9e1338214cb8e7522ac3ab51f45c1360dd3ce40607da902eb99ec03bc62d  config-ppc
95796cf8c2416d12fa61d72ffe1403b33f0b35dead111c94cc6835d74586761ec47b562ad1516a917a5411de043164daf13a35b5d86ee03266db2a0b146d507b  config-ppc64
17a07b7563acba1f5b99b9055198f5f93e7c65432ec46c35fa6c2b5307107ad9d4e8ffea9800f50cf8a8445b69561b9a95ba1ac8cd5bb2b6814dab540edb17d4  config-sparc64
137b549a61a241c21f956c9f13f476858bbffd4b393a3630e39d7c810f25d71cdcfd5307788c5c40fb3a92552ee1081f06ccb2379a9c0868ce25cb36f31cbda2  config-x86_64
1ca1bc094a8106b33176ab6cfb55c9514e7d69f1ed30c580eee419a3c422a2e6625d71c4581702e911f36c4bbac175010f46b7123bb81fda3303d4decdc57287  kernel.h
d0a0498c0200ff65e1feecf04dfad915fe4cc96bedaa6ed41b920d3385a67998649abc85254fd092d59bac756d67e93eabed4e909694fff13b12a1b7da7dddfd  0100-linux-6.6.58.patch
d333494e1a261175ab11d84ace49ad3dcb010614e61d0bfe1d39d7c330d1c0e0311699423fbec5841c9c6ff514f4f5b1e63072f0289450ac2883f1d3a80c2961  0122-link-security-restrictions.patch
dc47b18749d95a456f8bc47fd6a0618c286b646b38466c3d950dfbeb25adf3fc1a794e95552e4da1abb58e49f0bd841f7222e71c4d04cb0264ca23476ca9caef  0124-bluetooth-keysize-check.patch
79eaf814d76402a445efc961666a7c7c74207e552b0cb32d93d5cb828da580f7dbe93509dc9f53321c7844663205a8dce4e518ba047e4c57fc55f5c3498088ec  0126-sign-file-libressl.patch
8ee913a4187740457a2de64708edf757666c6a8a7f8ef30aaa8eee22290a30fa5d636d10de1fad334a30b4acdb733ffe556fb046d5d1769bde3b4e85906189d5  0200-x86-compile.patch
aa5636e778b053a65b739056f300bbc2056dabc215dc110ac35bf3ce0f1cd35d9eafb4db87e804993200fa2de4e84e6410d7d77794abbb446ef7fc83c22d3296  0202-parisc-disable-prctl.patch
c68d72ba638f04a35d484b996a0beb08fc4166ad6c5d261138097e5ed4851663fa013ec65995b59b0045e7c9db66c9efe8f168de4025e9f913b8d216134242cf  0204-sparc-warray-fix.patch
8f3120486ec8bbe2c82efc56888a08f73c99573c420f2b6d6f13fe71981418164eddb63b7f60d551b72da64a7854ee738138fb0e5010d3d1b265c3c4b088d22b  0208-gcc14-objtool-fix.patch
e74b043c4e5a590cef274e8b869c19159c7b0eeec7e2e382776526f042e7f13715a2b42c29305c299be22d2e81e373ef82c7ada3b6057d56df3d20580106cd2e  0250-expose-per-process-ksm.patch
341f249092877d573a9f2dd32cc347138e9f684283a085ee413a9d5391640de3b8d3ff0f61aebb78f63a54217d28393fb3761c8a94abd4d0718458b102e06cac  0252-rectify-ksm-inheritance.patch
9450d34f0a0efa6b11c5c84b4f8aed6a9095e8737673cdc731fd4c6ff9afa0b3e75e3d38a2542f72279a8be16aba8e8d4fd47535ca947302ed68a98e724c71b6  0260-reduce-swappiness.patch
7796055bbbaf5eb421991cd2b9e7ea030e1da0484855a2405c0c71070246a83d04165f9b61e1a9cc45aea84bfe150da3441d7cd241a5dd3a06ea03dada1cfc37  0262-boot-order.patch
70d8c777a9975e2677ace0a18452a63eb1117f24f738f905e2d0a3ba4a394046a45758d7f5509ef7de13a2ffc22aa1c65c21ff010ad6b550978044c76912a3f8  0300-correct-max98388-includes.patch
865b463092bb824e4eb6dd38821f063d2906bafdafe807f1d4af17567f82681903975c35c732e6526230f64ce99562243baaba30c52b019c73bbae1b98e6359f  0302-i915-gcc14-fix.patch
05376bf6307dc26d64f6cf7ee809deb81caf8b3040cb80170844505e3d5b10220d51879f047304bea4bde7c001c1924ae52dfc01f292890dab7f06dfb10264d9  0304-fix-powerbook6-5-audio.patch
34f7fc726caedabdf0cee350c8eb9fb84041388d537bb3cfcd143dcf89dca8b8a75dd2611896987d4ce31aa2935598b860ea93fc8bbd65ce47385f3468f7f650  0400-reduce-pageblock-size-nonhugetlb.patch
1d4391cf8f34e6898da303803961364474aa175b0a95dd13b284e88a9590c657083db78efb65d9fa9d40c22e0948da0bdc1063ee84ae0a699292a4d953d6bf65  0402-mm-optimise-slub.patch
882cc2a0692a3b4841a5814a1b552db6dab10e49528050f265d04b6d34fe517e2f3f6436de82f889b5471192f87ecfdff9ea142c651154622821579429519b4c  0404-page-cache-not-found.patch
dfd940af1d14e8a2cc1fcffbfa0fa8db52accf67cad3a7f435fc84047c945ba64dc3c45eb8dbbcc82715d8b8f2fbafa1e22b123158029016c5506cc134a7842c  0500-print-fw-info.patch
13377ff9df08f93a9d1be62081e8724010b423c7b21a48fc540e963df393c0b9238ff552aa67fbec604b30ec5dc9e78e3b5d480662d5acb2a9cb5d9dec337c7d  0502-gcc9-kcflags.patch
ad92d5e21e61811a1f27cc9595f9870ca6d3d22d008b78238a0223ac504aeb0f5c8574a1d38951cefde331ed18fe70c4b086078db55ae15a5a285f733c66235e  0504-update-zstd-to-v1_5_6.patch
5e71f65d2f96fd3f7f531a8c8c37e5ffbaf921bbc54236eeb8b8d76f425cb53418e5d55e7c9567754d17eff4c40bb85b0b559c8d6ce0b45837bdf744be44880b  1000-version.patch
5a60551ce859f591ffd772b37cb46be3194ac6ba7e08e2dddc6df967add96c5cf51d6a92aed3a7b6805dde691da5f7670bcbcc68ce1f51763f59f725cbc28f9d  no-require-gnu-tar.patch
191cf9dfad4778977fe1d0127bd0463caef98bead48a06ef68c4ba0134b97e3ebb461403bd2f16a941304bd61b005010ae159810f2c597ad4dbe26408919bb51  bsdtar.patch
d3b9e580db6006d25297b2dc17c4dc97be35992f9a02dd3bc37afa7d8054f8828a5c2060a5ffbd8e540e9d8babdca369b2af4106961e74652687e53d5bc7887b  no-require-lilo.patch
7bb07eb22002cc48caf0cd55d17ce4097aa583e0ca4048c11c92e1519761b2ae982ffe98311543d4b0dfc991c8bc411b2e1c7be9488b6c6f19ffaa08e69e2f47  no-autoload-fb.conf
b0e0634c84440a480be208da5157b03cb914790faab08fd3fdc2faeceed2c0a03d52c0e029084190708190f80028648923c4fd6feb11ec68ab4f740488161b0a  mkimage-missing-not-fatal.patch"
