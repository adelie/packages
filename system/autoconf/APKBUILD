# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=autoconf
pkgver=2.71
pkgrel=0
pkgdesc="A GNU tool for automatically configuring source code"
arch="noarch"
license="GPL-2.0+"
url="https://www.gnu.org/software/autoconf"
depends="m4 perl"
subpackages="$pkgname-doc"
source="https://ftp.gnu.org/pub/gnu/$pkgname/$pkgname-$pkgver.tar.gz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr
	make
}

check() {
	# 351 known broken since 2012:
	# https://lists.gnu.org/archive/html/bug-autoconf/2013-01/msg00000.html
	make check TESTSUITEFLAGS="1-350 352-503"
}

package() {
	make DESTDIR="$pkgdir" install
	rm -f "$pkgdir"/usr/share/info/dir
	# conflict with bintuils
	rm -f "$pkgdir"/usr/share/info/standards.info
}

sha512sums="2bc5331f9807da8754b2ee623a30299cc0d103d6f98068a4c22263aab67ff148b7ad3a1646bd274e604bc08a8ef0ac2601e6422e641ad0cfab2222d60a58c5a8  autoconf-2.71.tar.gz"
