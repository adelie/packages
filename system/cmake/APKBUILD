# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=cmake
pkgver=3.23.5
pkgrel=0
pkgdesc="Cross-platform build system"
url="https://cmake.org"
arch="all"
license="CMake"
depends=""
checkdepends="musl-utils file"
makedepends="ncurses-dev curl-dev expat-dev zlib-dev bzip2-dev libarchive-dev
	libuv-dev xz-dev rhash-dev"
subpackages="$pkgname-doc"

case $pkgver in
*.*.*.*) _v=v${pkgver%.*.*};;
*.*.*) _v=v${pkgver%.*};;
esac

source="https://cmake.org/files/$_v/cmake-${pkgver}.tar.gz
	fix-tests-git-file-protocol-permission.patch
	"

_parallel_opt() {
	local i n
	for i in $MAKEFLAGS; do
		case "$i" in
			-j*) n=${i#-j};;
		esac;
	done
	[ -n "$n" ] && echo "--parallel=$n"
}

build() {
	# jsoncpp needs cmake to build so to avoid recursive build
	# dependency, we use the bundled version of jsoncpp
	./bootstrap \
		--prefix=/usr \
		--mandir=/share/man \
		--datadir=/share/$pkgname \
		--docdir=/share/doc/$pkgname \
		--system-libs \
		--no-system-jsoncpp \
		$(_parallel_opt)
	make
}

check() {
	# disable only one part of this test (see #417)
	sed -i Tests/RunCMake/ctest_submit/RunCMakeTest.cmake \
		-e '/^run_ctest_submit_FailDrop(https)/d' \
		;

	# skip CTestTestUpload: tries to upload something during check...
	#CTEST_PARALLEL_LEVEL=${JOBS} \
	CTEST_OUTPUT_ON_FAILURE=TRUE \
	bin/ctest \
		-E CTestTestUpload \
		-E BundleUtilities \
		;
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="298f02cd4a0b332184b3e49f343d8a03dcfa637004b0ee9d0a81f72b1ee568aca9caa7322dbf9fe82d9660a8c617f572404cef0c34b2f63297e9211e953cca12  cmake-3.23.5.tar.gz
353cae903076760c77a902256ea6c61102ada60c1ebd05227670cb2bb9aa8f49a4e9946513650f4e58ff94f9a1d108c427d88340b9a633df5376e8036498245e  fix-tests-git-file-protocol-permission.patch"
