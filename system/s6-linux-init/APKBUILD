# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=s6-linux-init
pkgver=1.1.2.1
pkgrel=0
pkgdesc="A s6-based init system"
url="https://skarnet.org/software/s6-linux-init/"
arch="all"
options="!check"  # No test suite.
license="ISC"
_skalibs_version=2.14.3
_utmps_version=0.1.2
_execline_version=2.9.6
_s6_version=2.13.1
depends="execline s6>=$_s6_version s6-linux-init-common"
depends_dev="skalibs-dev>=$_skalibs_version"
makedepends="$depends_dev execline-dev>=$_execline_version s6-dev>=$_s6_version utmps-dev>=$_utmps_version"
subpackages="
	$pkgname-common:common:noarch
	$pkgname-early-getty:earlygetty:noarch
	$pkgname-dracut:dracut:noarch
	$pkgname-libs
	$pkgname-dev
	$pkgname-doc"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz
	rc.init
	runlevel
	rc.shutdown
	reboot.sh
	earlygetty.run
	s6-linux-init.pc.in
	dracut_module.sh"
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.pre-deinstall"
provides="/sbin/init=0"

build() {
	./configure \
		--enable-shared \
		--enable-static-libc \
		--libexecdir="/lib/$pkgname" \
		--enable-utmps
	make
}

package() {
	make DESTDIR="$pkgdir" install

	for i in rc.init runlevel rc.shutdown ; do
		cp -f "$srcdir/$i" "$pkgdir/etc/s6-linux-init/skel/"
		chmod 0755 "$pkgdir/etc/s6-linux-init/skel/$i"
	done

	# static binaries don't work under fakeroot, so build a dynamic one just for us
	touch src/init/s6-linux-init-maker.o
	make s6-linux-init-maker LDFLAGS_NOSHARED=""

	./s6-linux-init-maker \
	  -u catchlog \
	  -G "sleep 86400" \
	  -1 \
	  -L \
	  -p "/usr/bin:/usr/sbin:/bin:/sbin" \
	  -m 022 \
	  -s /run/kernel_env \
	  -f "$pkgdir/etc/s6-linux-init/skel" \
	  -- "$pkgdir/etc/s6-linux-init/current"
	mkdir -p -m 0755 "$pkgdir/sbin" "$pkgdir/etc/runlevels/empty" "$pkgdir/usr/share/doc"
	for i in init halt poweroff reboot shutdown telinit ; do
		ln -sf "../etc/s6-linux-init/current/bin/$i" "$pkgdir/sbin/$i"
	done
	sed -e "s/@@VERSION@@/$pkgver/g; s/@@SKALIBS_VERSION@@/${_skalibs_version}/g;" "$srcdir/$pkgname.pc.in" > "$srcdir/$pkgname.pc"
	install -D -m 0644 "$srcdir/$pkgname.pc" "$pkgdir/usr/lib/pkgconfig/$pkgname.pc"
	cp -a "$builddir/doc" "$pkgdir/usr/share/doc/$pkgname"
}


common() {
	pkgdesc="Files for an s6 supervision tree, common to s6-linux-init and sysvinit"
	depends="execline s6"
	runimg="$pkgdir/etc/s6-linux-init/current/run-image"
	subrunimg="$subpkgdir/etc/s6-linux-init/current/run-image"
	install="$subpkgname.post-upgrade $subpkgname.pre-deinstall"
	mkdir -p -m 0755 "$subrunimg/service/.s6-svscan" "$subrunimg/service/s6-svscan-log" "$subpkgdir/usr/share/s6-linux-init-common"
	mv "$runimg/uncaught-logs" "$subrunimg/"
	mv "$runimg/service/s6-svscan-log" "$subrunimg/service/"
	mkdir -m 0755 "$runimg/service/s6-svscan-log"
	mv "$subrunimg/service/s6-svscan-log/run" "$runimg/service/s6-svscan-log/"
	cp -f "$srcdir/reboot.sh" "$subpkgdir/usr/share/s6-linux-init-common/"
}


earlygetty() {
	pkgdesc="Files for a configurable early getty"
        depends="s6-linux-init-common"
        svcimg="$pkgdir/etc/s6-linux-init/current/run-image/service"
        subsvcimg="$subpkgdir/etc/s6-linux-init/current/run-image/service"
	mkdir -p -m 0755 "$subsvcimg"
	mv "$svcimg/s6-linux-init-early-getty" "$subsvcimg/"
	cp -f "$srcdir/earlygetty.run" "$subsvcimg/s6-linux-init-early-getty/run"
	chmod 0755 "$subsvcimg/s6-linux-init-early-getty/run"
}

dracut() {
	pkgdesc="$pkgdesc (Dracut module)"
	install_if="$pkgname=$pkgver-r$pkgrel dracut"
	install -D -m755 "$srcdir"/dracut_module.sh "$subpkgdir"/usr/lib/dracut/modules.d/99s6li/module-setup.sh
}

sha512sums="343b6a9bdcf3b1abb544409bb2d7a6c8a1dbcb07d1b941ae74e8800e48ad605c6427f724d7692569b3ade4829948d0e57024c764c18a60c3bb2eb2efefedb622  s6-linux-init-1.1.2.1.tar.gz
756b0cbbe5dabb4631380c3c7ea199cc213224b2e36e50a2d012a61948170078b78bf49b85d886319fecf59843087f937d3d804723b2553ac9f94d088a2f0fd8  rc.init
e73c3c32b118831074288d23fadace2158a2b15d5a13ffa73290b92a9e39c2a21c73d3b0eabea29bcbaa5f6381611fd8d0aaa6aa691ec7de91b8ef6ae404b6da  runlevel
7bb050248a5c2ab6a56c50c35f87cde724f97ff9882f5e60b0f0f2f14bd93c1df7d99fedc3d81c8519cf1a1ed90e03f1cbb9bf891c7b3618aa9a5f5738d262f4  rc.shutdown
6fb2a1112988fd2322b4bc4862bfb948a1c2e43921c5d01ae873c0d31b39fe74fc2934a5018c08b1704a2b2199b31d3a3d7365be369bba734f153b74e000aa74  reboot.sh
dfff483b61370ce2c8fe653cb4e9b6ec7ef678f26b128eab8e677548a48b668b532b12a2e4618c85bf95777a666ac30189780708803d6ea12e43ab0072399212  earlygetty.run
1e617f0a1ae724c80d0f577c1484297b4aea3107cad6b2589ffeabcad3a12a8ba5de876fd2a6d7ddc03c4e6e9d34938dd0356b273fe62c475ff31da9aef2596d  s6-linux-init.pc.in
a62cf543e64aaf24f16c0e3d23497890de7a4a621c9d8f93c447001ea56a21324046ee589188d11b3d4dbfea4a707b5a1889d2b703c00ea6a81207a0442f9799  dracut_module.sh"
