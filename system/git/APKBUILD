# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=git
pkgver=2.40.0
pkgrel=1
pkgdesc="Distributed version control system"
url="https://www.git-scm.com/"
arch="all"
license="GPL-2.0+"
depends="perl-error"
checkdepends="python3"
makedepends="zlib-dev openssl-dev curl-dev expat-dev perl-dev
	pcre2-dev asciidoctor xmlto perl-error docbook-xsl-ns"
# note that order matters
subpackages="$pkgname-doc
	$pkgname-bash-completion:completion:noarch
	$pkgname-email
	$pkgname-fast-import:_fast_import
	$pkgname-cvs::noarch
	$pkgname-p4::noarch
	$pkgname-daemon
	$pkgname-gitweb::noarch
	$pkgname-subtree::noarch
	$pkgname-lang
	"
replaces="git-perl perl-git"
source="https://www.kernel.org/pub/software/scm/git/git-$pkgver.tar.xz
	dont-test-other-encodings.patch
	git-daemon.initd
	git-daemon.confd

	disable-t2082-2.patch
	perl-getopt-long.patch
	"
_gitcoredir=/usr/libexec/git-core

# secfixes:
#   2.25.4-r0:
#     - CVE-2020-11008
#   2.25.3-r0:
#     - CVE-2020-5260
#   2.24.1-r0:
#     - CVE-2019-1348
#     - CVE-2019-1349
#     - CVE-2019-1350
#     - CVE-2019-1351
#     - CVE-2019-1352
#     - CVE-2019-1353
#     - CVE-2019-1354
#     - CVE-2019-1387
#     - CVE-2019-19604
#   2.19.2-r0:
#     - CVE-2018-19486
#   2.14.1-r0:
#     - CVE-2017-1000117

prepare() {
	default_prepare
	cat >> config.mak <<-EOF
		NO_SVN_TESTS=YesPlease
		NO_REGEX=YesPlease
		USE_ASCIIDOCTOR=1
		USE_LIBPCRE2=YesPlease
		NO_NSEC=YesPlease
		NO_SYS_POLL_H=1
		CFLAGS=$CFLAGS
	EOF
}

build() {
	make prefix=/usr DESTDIR="$pkgdir"
}

check() {
	make prefix=/usr DESTDIR="$pkgdir" -j1 test
}

package() {
	make -j1 prefix=/usr \
		DESTDIR="$pkgdir" \
		INSTALLDIRS=vendor \
		install
	mkdir -p "$pkgdir"/var/git
	install -Dm755 "$srcdir"/git-daemon.initd \
		"$pkgdir"/etc/init.d/git-daemon
	install -Dm644 "$srcdir"/git-daemon.confd \
		"$pkgdir"/etc/conf.d/git-daemon

	make prefix=/usr DESTDIR="$pkgdir" install-man
	find "$pkgdir" -name perllocal.pod -delete
}

email() {
	depends="perl perl-net-smtp-ssl perl-authen-sasl"
	pkgdesc="Git tools for sending email"
	replaces="git"
	mkdir -p "$subpkgdir"/$_gitcoredir
	mv "$pkgdir"/$_gitcoredir/*email* "$pkgdir"/$_gitcoredir/*imap* \
		"$subpkgdir"/$_gitcoredir
}

cvs() {
	pkgdesc="Git tools for importing CVS repositories"
	depends="perl cvs perl-dbd-sqlite"
	replaces="git-perl"
	mkdir -p "$subpkgdir"/usr/bin "$subpkgdir"/$_gitcoredir
	mv "$pkgdir"/usr/bin/git-cvs* "$subpkgdir"/usr/bin/
	mv "$pkgdir"/$_gitcoredir/*cvs* "$subpkgdir"/$_gitcoredir \

}

_fast_import() {
	pkgdesc="Git backend for fast Git data importers"
	depends="git=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir"/$_gitcoredir
	mv "$pkgdir"/$_gitcoredir/git-fast-import "$subpkgdir"/$_gitcoredir/
}

p4() {
	pkgdesc="Git tools for working with Perforce depots"
	depends="git=$pkgver-r$pkgrel git-fast-import=$pkgver-r$pkgrel"
	replaces="git"
	mkdir -p "$subpkgdir"/$_gitcoredir/mergetools
	mv "$pkgdir"/$_gitcoredir/*p4* "$subpkgdir"/$_gitcoredir/
	mv "$pkgdir"/$_gitcoredir/mergetools/*p4* \
		"$subpkgdir"/$_gitcoredir/mergetools/
}

daemon() {
	pkgdesc="Git protocol daemon"
	depends="git=$pkgver-r$pkgrel"
	replaces="git"
	mkdir -p "$subpkgdir"/$_gitcoredir
	mv "$pkgdir"/$_gitcoredir/git-daemon \
		"$pkgdir"/$_gitcoredir/git-http-backend \
		"$pkgdir"/$_gitcoredir/git-shell \
		"$subpkgdir"/$_gitcoredir \

	mv "$pkgdir"/etc "$subpkgdir"/
}

gitweb() {
	pkgdesc="Simple web interface to git repositories"
	depends="git=$pkgver-r$pkgrel perl"
	replaces="git"
	mkdir -p "$subpkgdir"/usr/share "$subpkgdir"$_gitcoredir
	mv "$pkgdir"/usr/share/gitweb "$subpkgdir"/usr/share/
	mv "$pkgdir"/$_gitcoredir/git-instaweb "$subpkgdir"$_gitcoredir
}

completion() {
	pkgdesc="Bash completion for $pkgname"
	depends=""
	replaces=""
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	install -Dm644 "$builddir"/contrib/completion/git-completion.bash \
		"$subpkgdir"/usr/share/bash-completion/completions/git
}

subtree() {
	depends="git=$pkgver-r$pkgrel"
	pkgdesc="Split git repository into subtrees"
	replaces=""

	cd "$builddir"/contrib/subtree
	make prefix=/usr DESTDIR="$pkgdir"
	make install prefix=/usr DESTDIR="$subpkgdir"
}

sha512sums="a2720f8f9a0258c0bb5e23badcfd68a147682e45a5d039a42c47128296c508109d5039029db89311a35db97a9008585e84ed11b400846502c9be913d67f0fd90  git-2.40.0.tar.xz
4bcc8367478601c856e0977d46fc4842f62daf300093a576704ad27ccd9fae975f95d3fbfcb00e9fa7254b1db64cd074f49a94fb5cf0abd8d72d7edc9ab8798c  dont-test-other-encodings.patch
89528cdd14c51fd568aa61cf6c5eae08ea0844e59f9af9292da5fc6c268261f4166017d002d494400945e248df6b844e2f9f9cd2d9345d516983f5a110e4c42a  git-daemon.initd
fbf1f425206a76e2a8f82342537ed939ff7e623d644c086ca2ced5f69b36734695f9f80ebda1728f75a94d6cd2fcb71bf845b64239368caab418e4d368c141ec  git-daemon.confd
6d7cbb701584a078328056a67bfd32dde5795a80c0911734b38bd534699fb0165ac2b486b267c5c39b90bbb0d7c5ab0ab6ada1d068748865617326da28304eb4  disable-t2082-2.patch
9800318f9e6a8b6bfd8c700cce5cc326522a607b89236a868ef46940efe0566fdadf5d69dc3e72f989d61df66be8510b8989bd4ce3fc780f017f30652c7e9efa  perl-getopt-long.patch"
