# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=iproute2
pkgver=5.17.0
pkgrel=0
pkgdesc="Advanced IP routing and network device configuration tools"
url="https://wiki.linuxfoundation.org/networking/iproute2"
arch="all"
# the testsuite in this package seems to be geared towards kernel developers
options="!check"
license="GPL-2.0-only"
depends=""
makedepends="bison flex bash db-dev libelf-dev libmnl-dev libcap-dev bsd-compat-headers
	libbpf-dev"
subpackages="$pkgname-doc $pkgname-bash-completion:bashcomp:noarch"
source="https://kernel.org/pub/linux/utils/net/iproute2/iproute2-$pkgver.tar.xz"

prepare() {
	default_prepare

	sed -i '/^TARGETS=/s: arpd : :' misc/Makefile
	sed -i 's:/usr/local:/usr:' tc/m_ipt.c include/iptables.h
	sed -i -e 's:=/share:=/usr/share:' \
		-e 's:-Werror::' Makefile
}

build() {
	./configure
	make CCOPTS="-D_GNU_SOURCE $CFLAGS" LIBDIR=/lib
}

package() {
	make -j1 DESTDIR="$pkgdir" install
	mkdir "$pkgdir"/bin
	mv "$pkgdir"/sbin/ip "$pkgdir"/bin/ip
	mkdir "$pkgdir"/usr/share/man/man1
	mv "$pkgdir"/usr/share/man/man8/ip.8 \
		"$pkgdir"/usr/share/man/man1/ip.1
}

bashcomp() {
	depends=""
	pkgdesc="Bash completions for $pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	mkdir -p "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/bash-completion \
		"$subpkgdir"/usr/share

	rmdir -p "$pkgdir"/usr/share 2>/dev/null || true
}

sha512sums="fcffe96fb4819305ddf5c3764b100bd1d204069cf53a6bd776c2716144f574b4fc17963fc231a83ad253cce6a563814556eeb60b211ba9b0b87330186259b34d  iproute2-5.17.0.tar.xz"
