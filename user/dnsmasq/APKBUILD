# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=dnsmasq
pkgver=2.89
pkgrel=0
pkgdesc="Network infrastructure (DNS, DHCP, PXE) for small networks"
url="https://dnsmasq.org/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0 OR GPL-3.0"
depends=""
makedepends="libidn2-dev nettle-dev"
subpackages="$pkgname-doc $pkgname-lang $pkgname-openrc"
install="$pkgname.pre-install"
source="https://thekelleys.org.uk/$pkgname/$pkgname-$pkgver.tar.xz
	dnsmasq.initd
	dnsmasq.confd
	"

_conf="-DNO_CONNTRACK -DNO_DBUS -DHAVE_IDN -DHAVE_LIBIDN2 -DHAVE_DNSSEC -DNO_LUA"

build() {
	make PREFIX=/usr CFLAGS="${CFLAGS}" LDFLAGS="${LDFLAGS}" COPTS="$_conf"\
		all-i18n
}

package() {
	make PREFIX=/usr CFLAGS="${CFLAGS}" LDFLAGS="${LDFLAGS}" COPTS="$_conf"\
		DESTDIR="$pkgdir" install-i18n
}

openrc() {
	install -D -m755 "$srcdir"/dnsmasq.initd "$subpkgdir"/etc/init.d/dnsmasq
	install -D -m755 "$srcdir"/dnsmasq.confd "$subpkgdir"/etc/conf.d/dnsmasq
}

sha512sums="4384ed5b673e10eaf6532e6eaeb5c0a6b817581433cc28c632bdcbadbfc050a0ab73bc5b73c98d708cd39515bb3f72168714b0aa5f16436cebdd18020648d428  dnsmasq-2.89.tar.xz
72ae659a62c61495dc575fa590cbf1352c4bd4f374a6e0a56fce9c641c163b882d8b8097d27605d102e47df49eb68c456baf7b11009a7bb7db98e99429bd80a6  dnsmasq.initd
9a401bfc408bf1638645c61b8ca734bea0a09ef79fb36648ec7ef21666257234254bbe6c73c82cc23aa1779ddcdda0e6baa2c041866f16dfb9c4e0ba9133eab8  dnsmasq.confd"
