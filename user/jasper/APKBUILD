# Maintainer: Zach van Rijn <me@zv.io>
pkgname=jasper
pkgver=2.0.33
pkgrel=0
pkgdesc="Library implementing JPEG-2000"
url="http://www.ece.uvic.ca/~mdadams/jasper/"
arch="all"
license="JasPer-2.0"
depends=""
makedepends="libjpeg-turbo-dev cmake"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs"
source="https://github.com/jasper-software/$pkgname/releases/download/version-$pkgver/$pkgname-$pkgver.tar.gz"

# secfixes:
#   2.0.12-r1:
#     - CVE-2017-1000050

build() {
	# default of 16 causes stack overflow
	export CFLAGS="${CFLAGS} -DJPC_QMFB_COLGRPSIZE=6"
	cmake \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DJAS_ENABLE_OPENGL=OFF \
		-DJAS_ENABLE_LIBJPEG=ON \
		-DJAS_ENABLE_AUTOMATIC_DEPENDENCIES=OFF \
		-Bbuild
	make -C build
}

check() {
	make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

libs() {
	pkgdesc="JPEG-2000 library"
	install -d "$subpkgdir"/usr/
	mv "$pkgdir"/usr/lib "$subpkgdir"/usr
}

sha512sums="b243be296b9e7adc86c5eee1deb50afab9e9361861fa3bc010d0fb10d29712fb872f685e7caeb15a6ecf25a7684ecd03bc2ebf851573e3930e8ee9bb2fa65a22  jasper-2.0.33.tar.gz"
