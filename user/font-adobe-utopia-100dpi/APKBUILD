# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=font-adobe-utopia-100dpi
pkgver=1.0.5
pkgrel=0
pkgdesc="100dpi Utopia X11 font from Adobe"
url="https://www.X.Org/"
arch="noarch"
# Okay.
# This is really hairy, but Fedora Legal says the TUG license can apply to the
# X11 distribution[1][2]; it's almost MIT style, but you have to rename the
# font if you modify it in any way.
# [1]: https://fedoraproject.org/wiki/Legal_considerations_for_fonts
# [2]: https://src.fedoraproject.org/cgit/rpms/xorg-x11-fonts.git/tree/xorg-x11-fonts.spec
license="Utopia"
depends="encodings font-alias fontconfig mkfontscale"
makedepends="bdftopcf font-util-dev util-macros"
subpackages=""
source="https://www.x.org/releases/individual/font/font-adobe-utopia-100dpi-$pkgver.tar.xz"

prepare() {
	default_prepare
	update_config_sub
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	find "$pkgdir" -name fonts.dir -exec rm {} +
}

sha512sums="9685d66de107ab3142aa9322bbded20f4f538d7bcd8c97bfb4db8eb1fc440724b0c7574ee8b4b5e96c81044c8ee4da175dd638f03dc3e1e650200fdf7b8a169b  font-adobe-utopia-100dpi-1.0.5.tar.xz"
