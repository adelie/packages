# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=artikulate
pkgver=22.04.2
pkgrel=0
pkgdesc="Pronunciation trainer for languages"
url="https://www.kde.org/applications/education/artikulate/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtxmlpatterns-dev
	karchive-dev kconfig-dev kcrash-dev kdoctools-dev ki18n-dev
	kirigami2-dev knewstuff-dev kxmlgui-dev qt5-qtmultimedia-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/artikulate-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTING=OFF \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# TestCourseFiles needs X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E TestCourseFiles
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="a6c19200869f6c693ca28c130a6b62674ef3908169fa01e1e406f83ae3d2d616bfe14417c61c28766f42708c842e061c218c00a37c9a07103cf33d8df773e5d0  artikulate-22.04.2.tar.xz"
