# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libva
pkgver=2.14.0
pkgrel=2
pkgdesc="Video Acceleration (VA) API for Linux"
url="https://github.com/intel/libva"
arch="all"
options="!check"  # No test suite.
license="MIT"
depends=""
makedepends="libdrm-dev libglvnd-dev libx11-dev libxext-dev libxfixes-dev
	cmd:which wayland-dev wayland-protocols"
subpackages="$pkgname-dev $pkgname-wayland $pkgname-x11"
source="https://github.com/intel/$pkgname/releases/download/$pkgver/$pkgname-$pkgver.tar.bz2"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-x11 \
		--enable-wayland \
		--disable-static \
		--enable-shared
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

wayland() {
	pkgdesc="$pkgdesc (Wayland library)"
	install_if="$pkgname=$pkgver-r$pkgrel wayland"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libva-wayland.so* "$subpkgdir"/usr/lib/
}

x11() {
	pkgdesc="$pkgdesc (X11 library)"
	install_if="$pkgname=$pkgver-r$pkgrel libx11"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libva-glx.so* "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libva-x11.so* "$subpkgdir"/usr/lib/
}

sha512sums="7ee6f0711929d1c6f6436b69ad0fedcee7e8a450be907d90a7c9741d55de824de471efbc20a398a47af03f5bbacb927daa1f2f780fce6ddfe6bbee21729c95ca  libva-2.14.0.tar.bz2"
