# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=font-adobe-utopia-type1
pkgver=1.0.5
pkgrel=0
pkgdesc="Type 1 Utopia X11 font from Adobe"
url="https://www.X.Org/"
arch="noarch"
# Okay.
# This is really hairy, but Fedora Legal says the TUG license can apply to the
# X11 distribution[1][2]; it's almost MIT style, but you have to rename the
# font if you modify it in any way.
# [1]: https://fedoraproject.org/wiki/Legal_considerations_for_fonts
# [2]: https://src.fedoraproject.org/cgit/rpms/xorg-x11-fonts.git/tree/xorg-x11-fonts.spec
license="Utopia"
depends="encodings font-alias fontconfig mkfontscale"
makedepends="util-macros"
subpackages=""
source="https://www.x.org/releases/individual/font/font-adobe-utopia-type1-$pkgver.tar.xz"

prepare() {
	default_prepare
	update_config_sub
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	find "$pkgdir" -name fonts.dir -exec rm {} +
	find "$pkgdir" -name fonts.scale -exec rm {} +
}

sha512sums="e30f4c7702f47a3a42206975fbcfec058317681956d246411e50e372b669bdc875c5d7fe28bad7d298bc61b71a997c7836fb6ac16effd223a96723a6e9ece649  font-adobe-utopia-type1-1.0.5.tar.xz"
