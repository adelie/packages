# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=rocs
pkgver=22.04.2
pkgrel=0
pkgdesc="Graph theory IDE"
url="https://www.kde.org/applications/education/rocs/"
arch="all"
options="!check"  # All tests require X11.
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kdoctools-dev boost-dev
	grantlee-dev qt5-qtsvg-dev qt5-qtxmlpatterns-dev karchive-dev
	kconfig-dev kcoreaddons-dev kcrash-dev kdeclarative-dev ki18n-dev
	kitemviews-dev ktexteditor-dev kxmlgui-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/rocs-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="c1129600fab5d67d87ce02d5194bdaa673f3b13edae75626ed67a1d86fb562cf1e7a613b58d95dc3110327c9192f907aa7749e3d93fb34d85a7befe7481f5a00  rocs-22.04.2.tar.xz"
