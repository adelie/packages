# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=i3wm
pkgver=4.19.2
pkgrel=0
pkgdesc="Improved dynamic tiling window manager"
url="https://i3wm.org"
arch="all"
options="!check"  # The test suite requires X
license="BSD-3-Clause"
depends=""
checkdepends="perl-x11-xcb perl-anyevent perl-json-xs perl-ipc-run
	perl-inline-c perl-dev libxcb-dev xcb-util-dev xorg-server-xephyr"
makedepends="meson ninja bison flex libxcb-dev xcb-util-cursor-dev
	xcb-util-keysyms-dev xcb-util-wm-dev libev-dev pango-dev cairo-dev
	yajl-dev startup-notification-dev pcre-dev libxkbcommon-dev
	xcb-util-xrm-dev"
subpackages="$pkgname-doc"
source="https://i3wm.org/downloads/i3-$pkgver.tar.xz
	i3wm-musl-glob-tilde.patch
	i3wm-test-fix-off_t.patch"
builddir="$srcdir/i3-$pkgver"

build() {
	# docs require asciidoc
	meson -Dprefix=/usr -Ddocs=false -Dmans=false build
	ninja -C build
}

check() {
	ninja -C build test
}

package() {
	DESTDIR="$pkgdir" ninja -C build install

	install -d "$pkgdir/usr/share/man/man1"
	install -m644 man/*.1 "$pkgdir"/usr/share/man/man1/
}

sha512sums="58f08228d842cd3c2bc417b72f6279fcd7050879e5fd4064b2cb00edaf98b6605d5b2361608f9534991a0f8357d3294e881c7e314f33dc075241cc45ca5ed94d  i3-4.19.2.tar.xz
6378e3619076c03345b4faa1f9d54cab2e7173068bc4d5f2f2894af9cc0e5792fe45ce95cb06328f5040f0ba6d43f3e49c523968732ac2d2046b698042338caa  i3wm-musl-glob-tilde.patch
77224b994397b2e2487ae28dfd5781b3630654191813eb3c685f05ebf446e65c36e53a665ff3cc8323ea67e87f7cf977044025dade0a6ed22cbd84f0e6b4cbc7  i3wm-test-fix-off_t.patch"
