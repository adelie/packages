# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gst-plugins-bad
pkgver=1.20.2
pkgrel=3
pkgdesc="GStreamer multimedia framework - Lower-quality plugins"
url="https://gstreamer.freedesktop.org/modules/gst-plugins-good.html"
arch="all"
license="LGPL-2.1+"
# rtpmanagerbad depends on rtpmanager
# symptom: tests 'rtpsrc' and 'rtpsink' will fail
depends="gst-plugins-good"
makedepends="gst-plugins-base-dev gstreamer-dev gobject-introspection-dev meson
	libva-dev libxkbcommon-dev orc-compiler orc-dev bluez-dev cairo-dev
	curl-dev fluidsynth-dev gtk+3.0-dev ladspa-dev libdrm-dev libgudev-dev
	libsndfile-dev libqrencode-dev librsvg-dev libsoup-dev libusb-dev
	mesa-dev neon-dev nettle-dev openal-soft-dev opus-dev pango-dev sbc-dev
	wayland-dev wayland-protocols gsm-dev vulkan-loader-dev libglvnd-dev"
subpackages="$pkgname-dev $pkgname-lang"
ldpath="/usr/lib/gstreamer-1.0"
source="https://gstreamer.freedesktop.org/src/$pkgname/$pkgname-$pkgver.tar.xz
	camerabin.patch
	curlhttpsrc.patch
	mpegts.patch
	fix-arm-test-alignment.patch
	fix-arm-parser-alignment.patch
	disable-failing-test.patch
	"

build() {
	meson \
		-Dprefix=/usr \
		-Dnls=enabled \
		-Dpackage-name="GStreamer Bad Plugins (${DISTRO_NAME:-Adélie Linux})" \
		-Dpackage-origin="${DISTRO_URL:-https://www.adelielinux.org/}" \
		build
	ninja -C build
}

check() {
	ninja -C build test
}

package() {
	DESTDIR="$pkgdir" ninja -C build install
}

sha512sums="3f98973dc07ead745418e0a30f9f6b5c8d328e3d126f54d92c10ab5da04271768a5c5dffc36ea24ccf8fb516b1e3733be9fb18dc0db419dea4d37d17018f8a70  gst-plugins-bad-1.20.2.tar.xz
7f6023e766895ac8213f9dabd4d8acbeb03f18c2d98fa27865e7656c439e0ea97d02f9e9f0ea98205238ddb8f6473062af524b799d1da0cbe41a3055a11df250  camerabin.patch
d4bf36560db0e6391cfca84f928d7147bb6b170431c521e78253b29f6bccf122309e1f92e241808855dfa112b9a06e17c1b6ed4363e011a0693f82e1bfe7ea3e  curlhttpsrc.patch
d70f409f86b072d689d51b87db6e9fb161c0914a381eccbf66f1ffcd61b9a793f9e4087cdd071e615aaa63687c6ee476f023239920dd1ebd0d3cde346031dc86  mpegts.patch
566fbdc37c9f9fb5b671ecffa2f596a95f6431668ff8dd8b91bc827a280b5fb18a8fac3122a38f060263cb3f2d2138805af1e1c324eb619188f982c8cc575221  fix-arm-test-alignment.patch
f394b1643a5d0943e8acd7e9ee11702ce3bbfb27242f3eac140b884ce7bb5dea725ab4bb0a28cbf9d435622dcc68ea0ee0bee1c51a520787c382167dd1e43583  fix-arm-parser-alignment.patch
3c900a5cbe3ac129172f36acd4d9ede2610ceab500515c32a380dabd2d3d3f78d26be0c9521db855d13283b5be154c9407cd6bf5ec59524305170fc2db2572cb  disable-failing-test.patch"
