# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=nfs-utils
pkgver=2.4.3
pkgrel=0
pkgdesc="NFS file sharing client and server"
url="http://nfs.sourceforge.net/"
arch="all"
options="!check suid"  # No functional test suite.
license="GPL-2.0-only"
depends="rpcbind"
makedepends="keyutils-dev krb5-dev libevent-dev libtirpc-dev lvm2-dev
	rpcsvc-proto sqlite-dev util-linux-dev"
subpackages="$pkgname-doc $pkgname-openrc"
source="https://downloads.sourceforge.net/nfs/nfs-utils-$pkgver.tar.xz
	posixish.patch
	time64.patch

	exports
	nfs.confd nfs.initd nfsclient.confd nfsclient.initd nfsmount.confd
	nfsmount.initd rpc.gssd.initd rpc.idmapd.initd rpc.pipefs.initd
	rpc.statd.initd rpc.svcgssd.initd
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-ipv6 \
		--enable-nfsv4 \
		--enable-nfsv41 \
		--enable-gss \
		--enable-svcgss \
		--with-statedir=/var/lib/nfs
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	install -D -m644 "$srcdir"/exports "$pkgdir"/etc/exports

	for _initd in nfs nfsclient nfsmount rpc.gssd rpc.idmapd rpc.pipefs \
		rpc.statd rpc.svcgssd; do
		install -D -m755 "$srcdir"/$_initd.initd "$pkgdir"/etc/init.d/$_initd
	done
	for _confd in nfs nfsclient nfsmount; do
		install -D -m644 "$srcdir"/$_confd.confd "$pkgdir"/etc/conf.d/$_confd
	done
}

sha512sums="9b8065d9d3958e45021898e24574d1e6d59353f94c2f760df18a793a734d79d0047df95e2f9cdebfda58e3f68f52b2ef723df2261c10bbeb5b16fe1413c1b8d7  nfs-utils-2.4.3.tar.xz
8ed36d22094eaca1a7b83fd5e840e774ee172ca81bf15215cf04127e45fc32ac75c3e0f3d8914370ac09c93d08a0fe8aa6d8f3eb7a8318db1e9ab7b2a05dee06  posixish.patch
e85afa3ba90eeb48d3a571543ab1361a27b9c4148799548a6b0cd304cff3231235c09753268a1601bf793b1589eb320d7196d00e86ef5eb33aee466fa260dafd  time64.patch
fd8052dc8e17eccb0bb1443f341c97ec7c9dac5824294cadf486d91475bc728d4bb69300034b528a690707520590f6675ee371f92e9838afc8ed88092ee0f220  exports
bc11b073735ee86c96b9c249ee187f4d16329c279a4e26760875059d2b5fa1d510ef3e4df5ee9dfb2a3133d755e1eb5c323d2fadc432a0cf630191ec353ac535  nfs.confd
f7feb79cfcab0478affb640d1e5ad059757c88d51cc790fd54cde2fd7ed2e3cfd8f7f4c2de993d99da03e8ce3bdfb2750a4cb997b850fe33d0ef76d9b91c9018  nfs.initd
f12e67728ac4e76357685e2fe38bd7f6fcd90064165758ffeca145e6962ac44d4ff9d8d57e18cc6925a6ada8e95ad8321e9fc355931a804dd455aae1e4850e92  nfsclient.confd
85078e2cd38b37a1e6fbaf0e40cf7690f403e71d9c8188d542d12fe56bfc02a49763c7bcc7742691754acc37928468c215db2795bebc29a5dfb052ba08f407bb  nfsclient.initd
6e23897885cc33c49d9c7353b456585a1e0c7300822edba81bc48ba4ccc18297adce137260cc0aa9487aa5ef0aab3eecf931532cfa5bd40fd03bc9e0ddacfb28  nfsmount.confd
89259b9f0878658d48792b5b2f42b43c966ed098dba1fecf9e07fb0de4aab37ad67655ea8dbcc2361ddab2b5013b2de35a03048a513aaeedf790e4b416a35a54  nfsmount.initd
d9d7eeebbe153d3c4784112ed6d50734b5619b3315bb1454f8163de2b78ed8f958029a0e1088de58e9b1b8069184aeab2c8f19af4b957b6be25e4f138960aeac  rpc.gssd.initd
f3e88038cb040ffcbca76166f4322d37e39da00d144bc0a17aed4467ab10c6e626fcde6c407d0911d135bfab55622ad2e38ed93788b11459c5882ce60577fb45  rpc.idmapd.initd
3d3fa6e7ae01e27d27d88aba1e307c5293dd1cbc9d555737e03c4f1e17988518f4f1e7f7a38a7337f8e8961e8378cc4611769b04db7368460589a3b218ac6d6b  rpc.pipefs.initd
a0fe91b4d7d288cb4793335c79e715b3726f573674ef29525bdab20c2053d1ae19e53a76852ef038aee0ad24fb18827a4ff333a0a179263b1ba47c9a6e2d337c  rpc.statd.initd
819cce4ae0a0f26bc74ce546e149b98420584240a594c9bb4a0a49413750320bde050866f3753c499425ea8d8df094211fb1f7e020ce281791e1933e1bc6a47f  rpc.svcgssd.initd"
