# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=py3-random2
_pkgname=random2
_p="${_pkgname#?}"
_p="${_pkgname%"$_p"}"
pkgver=1.0.1
pkgrel=1
pkgdesc="A Python module implementing Python 2's RNG"
url="https://pypi.org/project/random2/"
arch="noarch"
license="Python-2.0"
depends="python3"
makedepends="python3-dev"
source="$pkgname-$pkgver.zip::https://files.pythonhosted.org/packages/source/$_p/$_pkgname/$_pkgname-$pkgver.zip"
builddir="$srcdir/$_pkgname-$pkgver"

prepare() {
	default_prepare

	# Remove a test that is invalid as of python 3.9
	# Taken from Fedora .spec
	# https://src.fedoraproject.org/rpms/python-random2/blob/rawhide/f/python-random2.spec
	sed -i '/self\.gen\.getrandbits, 0/d' src/tests.py
}

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="build/lib" python3 setup.py test
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="31de31c27bc6e07e99c24d30e97d9ddc3085fe557bcb0725881d4ab0b4c06e6208f64ba6267098c7b1be4f115ad45b399e841beecc90f71da2fb7d82982e75ff  py3-random2-1.0.1.zip"
