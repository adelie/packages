# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=prison
pkgver=5.94.0
pkgrel=0
pkgdesc="Qt Barcode library for programatically creating QR codes"
url="https://www.kde.org/"
arch="all"
license="MIT"
depends=""
depends_dev="qt5-qtbase-dev libqrencode-dev libdmtx-dev"
makedepends="$depends_dev cmake extra-cmake-modules doxygen graphviz
	qt5-qtdeclarative-dev qt5-qtmultimedia-dev qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-quick"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/prison-$pkgver.tar.xz
	code128.patch
	endian.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

quick() {
	pkgdesc="$pkgdesc (QML binding)"
	mkdir -p "$subpkgdir"/usr/lib/qt5/
	mv "$pkgdir"/usr/lib/qt5/qml "$subpkgdir"/usr/lib/qt5/
}

sha512sums="7de7f528d686ddcf1ff987786ca06d431944deed49f89075a0438982408843249882fc931003f91b4646513ff4515df9104d97ba662a87eeee6394cc93bd3969  prison-5.94.0.tar.xz
a9d43722cab473ce132047523162f6532b5c84ff1446fdec262a434396aaf1fd83e87fff06c37ead01037488863805989ccd39412d72116485a2f9713733173f  code128.patch
b0614c9045864781daab75c7bb6e352431b3a4cdb167a7cfeafe8ff5a8a62c8f28f78db3a42175d230d3655877c7f49cf7f2774b189c1b11107217d74c0d7820  endian.patch"
