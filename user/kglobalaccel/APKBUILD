# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kglobalaccel
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework for implementing global shortcuts/accelerators"
url="https://www.kde.org/"
arch="all"
options="!check"  # Only test requires X11.
license="LGPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev kconfig-dev kcoreaddons-dev kcrash-dev
	kdbusaddons-dev"
makedepends="$depends_dev cmake extra-cmake-modules xcb-util-keysyms-dev doxygen
	libxcb-dev libx11-dev libxext-dev libice-dev qt5-qttools-dev graphviz
	qt5-qtdeclarative-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kglobalaccel-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="9e0b81716acaf27a5a6f6f64e7bafca96a3820eaec5627193f45cd95122f46b3a02d2bd18791c23bf74b3c84eba7eef9b331bd2ae093f7590e56c65bee66f78f  kglobalaccel-5.94.0.tar.xz"
