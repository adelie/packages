# Maintainer: Max Rees <maxcrees@me.com>
pkgname=yajl
pkgver=2.1.0
pkgrel=1
pkgdesc="Yet Another JSON Library (YAJL)"
url="https://lloyd.github.io/yajl/"
arch="all"
license="ISC"
depends=""
checkdepends="cmd:which"
makedepends="cmake"
subpackages="$pkgname-dev $pkgname-tools"
source="https://distfiles.adelielinux.org/source/$pkgname-$pkgver.tar.gz"

# How to create archive from git:
#
#  abuild snapshot
#

_gitrepo=yajl.git
_giturl="https://github.com/lloyd/$_gitrepo"
_gittag=$pkgver

snapshot() {
	_tarball=$pkgname-$pkgver.tar.gz
	msg "Creating $_tarball.."
	mkdir -p "$srcdir"
	cd "${SRCDEST:-$srcdir}"
	if ! [ -d "$_gitrepo" ]; then
		git clone --bare  $_giturl
	else
		git --git-dir=$_gitrepo fetch
	fi

	git --git-dir=$_gitrepo archive --prefix=$pkgname-$pkgver/ \
		-o "$SRCDEST"/$_tarball $_gittag

	msg "Upload $_tarball to mirrormaster now."
}

build() {
	cmake \
		-DCMAKE_INSTALL_PREFIX="/usr" \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		.
	make
}

check() {
	make test
	# make test-api just checks the return status of gen-extra-close
	# and also forkbombs, so elide that
}

package() {
	make install DESTDIR="$pkgdir"
	mv "$pkgdir"/usr/share/pkgconfig "$pkgdir"/usr/lib/
}

tools() {
	pkgdesc="JSON tools based on Yajl"
	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="9e786d080803df80ec03a9c2f447501e6e8e433a6baf636824bc1d50ecf4f5f80d7dfb1d47958aeb0a30fe459bd0ef033d41bc6a79e1dc6e6b5eade930b19b02  yajl-2.1.0.tar.gz"
