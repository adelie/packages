#!/bin/bash

# called by dracut
check() {
    require_binaries /lib/elogind/elogind-uaccess-command
}

# called by dracut
depends() {
    echo base
    return 0
}

# called by dracut
install() {
    inst /lib/elogind/elogind-uaccess-command
}

