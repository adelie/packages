# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=quassel
pkgver=0.14.0
pkgrel=1
pkgdesc="Modern, cross-platform IRC client"
url="https://quassel-irc.org/"
arch="all"
license="(GPL-2.0-only OR GPL-3.0-only) AND GPL-2.0+ AND LGPL-2.0+"
depends="$pkgname-client $pkgname-core"
makedepends="qt5-qtbase-dev qt5-qtmultimedia-dev libdbusmenu-qt-dev sonnet-dev
	extra-cmake-modules kconfigwidgets-dev kcoreaddons-dev kxmlgui-dev
	knotifications-dev knotifyconfig-dev ktextwidgets-dev qt5-qtscript-dev
	kwidgetsaddons-dev qca-dev qt5-qttools-dev zlib-dev libexecinfo-dev
	cmake boost-dev"
langdir="/usr/share/quassel/translations"
subpackages="$pkgname-core $pkgname-client $pkgname-lang $pkgname-libs"
source="https://quassel-irc.org/pub/quassel-$pkgver.tar.bz2"

# secfixes:
#   0.12.5-r0:
#     - CVE-2018-1000178
#     - CVE-2018-1000179

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DUSE_QT5=ON \
		-DWITH_KDE=ON \
		-DWITH_WEBENGINE=OFF \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

client() {
	pkgdesc="Modern, cross-platform IRC client (Thin client only)"
	depends=""
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/quasselclient "$subpkgdir"/usr/bin/

	for dir in applications icons knotifications5; do
		mkdir -p "$subpkgdir"/usr/share/$dir
		mv "$pkgdir"/usr/share/$dir "$subpkgdir"/usr/share/
	done

	mkdir -p "$subpkgdir"/usr/share/quassel
	mv "$pkgdir"/usr/share/quassel/stylesheets \
		"$subpkgdir"/usr/share/quassel/
	mv "$pkgdir"/usr/share/quassel/icons \
		"$subpkgdir"/usr/share/quassel/

	mkdir -p "$subpkgdir"/usr/lib
	for component in client qtui uisupport; do
		mv "$pkgdir"/usr/lib/libquassel-$component.* \
			"$subpkgdir"/usr/lib/
	done
}

core() {
	pkgdesc="Modern, cross-platform IRC daemon (core only)"
	depends=""

	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/quasselcore "$subpkgdir"/usr/bin/
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libquassel-core* "$subpkgdir"/usr/lib/
}

libs() {
	pkgdesc="Modern, cross-platform IRC daemon (libraries)"
	depends=""

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libquassel-common* "$subpkgdir"/usr/lib/

	mkdir -p "$subpkgdir"/usr/share/quassel
	for component in networks.ini scripts; do
		mv "$pkgdir"/usr/share/quassel/$component \
			"$subpkgdir"/usr/share/quassel/
	done
}

sha512sums="ea6b9723acab5ce73f760692770c1340c03bf277d2c99a2520345bfb6a7bb6fdc64a01dccfd7026341b46ee727821e1bcc2f487be72dfbc155f1de1ad264763f  quassel-0.14.0.tar.bz2"
