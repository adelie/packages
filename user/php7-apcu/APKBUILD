# Contributor: zlg <zlg+adelie@zlg.space>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=php7-apcu
_pkgname=${pkgname#php7-}
pkgver=5.1.18
pkgrel=0
pkgdesc="Userland cache for PHP"
url="https://pecl.php.net/package/APCu"
arch="all"
license="PHP-3.01"
depends="php7"
makedepends="php7-dev"
subpackages=""
source="https://pecl.php.net/get/$_pkgname-$pkgver.tgz"
builddir="$srcdir/$_pkgname-$pkgver"

build() {
	phpize
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--enable-apcu
	make
}

check() {
	# The tests have a prompt at the end for sending stats
	make NO_INTERACTION=1 test
}

package() {
	# It didn't listen to DESTDIR
	make INSTALL_ROOT="$pkgdir" install

	install -d "$pkgdir"/etc/php/conf.d/
	echo 'extension=apcu.so' > "$pkgdir"/etc/php/conf.d/00_apcu.ini
}

sha512sums="5e9c5b4540be7abdf2f473d2f8955d8708934a1d6e05cb2c99154cc7ba8a2bbf9afde51769e20f46ef278efd6f60b9172e0864c31e0976b9d3096a337035e7a9  apcu-5.1.18.tgz"
