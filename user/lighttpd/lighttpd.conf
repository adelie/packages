###################################
# Default lighttpd.conf for Adélie
###################################


######## Variables ########
var.basedir  = "/var/www/localhost"
var.logdir   = "/var/log/lighttpd"
var.statedir = "/var/lib/lighttpd"


######## Modules ########
# NOTE: the order of modules is important.
server.modules = (
#	"mod_rewrite",
#	"mod_redirect",
#	"mod_alias",
	"mod_access",
#	"mod_auth",
#	"mod_status",
	"mod_setenv",
#	"mod_magnet",
#	"mod_proxy",
#	"mod_simple_vhost",
#	"mod_evhost",
#	"mod_userdir",
	"mod_deflate",
#	"mod_ssi",
	"mod_expire",
#	"mod_rrdtool",
#	"mod_webdav",
	"mod_accesslog",
#       "mod_openssl"   # You need this if you want TLS!
)


######## Inclusions ########
include "mime-types.conf"
# uncomment for cgi support
#   include "mod_cgi.conf"
# uncomment for php/fastcgi support
#   include "mod_fastcgi.conf"
# uncomment for php/fastcgi fpm support
#   include "mod_fastcgi_fpm.conf"


######## Global Settings ########
server.username      = "lighttpd"
server.groupname     = "lighttpd"

server.document-root = var.basedir + "/htdocs"
server.pid-file      = "/run/lighttpd.pid"

server.errorlog      = var.logdir  + "/error.log"
# To log errors to syslog instead, use:
# server.errorlog-use-syslog = "enable"

index-file.names     = ("index.html", "index.htm", "default.htm")

# server.tag           = "lighttpd"

# To chroot to a directory:
# server.chroot      = "/"

# Default bind port is 80.  To change:
# server.port          = 81

# Default bind address is global (0.0.0.0).  To change:
# server.bind          = "grisu.home.kneschke.de"

# error-handler for status 404
# server.error-handler-404 = "/error-handler.html"

# Format: <errorfile-prefix><status-code>.html
# -> ..../status-404.html for 'File not found'
# server.errorfile-prefix    = var.basedir + "/error/status-"

# inotify support for caching stat() calls
#server.stat-cache-engine = "inotify"

# which extensions should not be handled via static-file transfer
# (extensions that are usually handled by mod_cgi, mod_fastcgi, etc).
static-file.exclude-extensions = (".php", ".pl", ".cgi", ".fcgi")


######## mod_accesslog config ########
accesslog.filename   = var.logdir + "/access.log"


######## mod_dirlisting config ########
# Enable directory listings if no indexfile is present.
#dir-listing.activate      = "enable"

# Don't list hidden files/directories (beginning with '.')
#dir-listing.hide-dotfiles = "enable"
#
# Specify a path here for custom directory listing CSS:
#dir-listing.external-css  = "/path/to/dir-listing.css"
#
# Exclude files that match any regex in this list from directory listings:
#dir-listing.exclude = ("^\.", "~$")


######## mod_access config ########
# See access.txt in lighttpd-doc package for more info.

url.access-deny = ("~", ".inc")


######## mod_userdir config ########
# This will give all users with valid homedirs the chance to publish a
# webpage from this server using traditional /~username/ paths.
# See userdir.txt in lighttpd-doc package for more info.
#
#userdir.path = "public_html"
#userdir.exclude-user = ("root")


######## mod_ssi config ########
# This allows you to use server-side includes.
#ssi.extension = (".shtml")


######## SSL config ########
# See ssl.txt in lighttpd-doc package for more info.
#
#ssl.engine				= "enable"
# pemfile is certificate + intermediate chain in one file
# privkey is certificate private key
#ssl.pemfile				= "/path/to/fullchain.pem"
#ssl.privkey				= "/path/to/privkey.pem"
# ECDH/ECDHE ciphers curve strength (see `openssl ecparam -list_curves`)
#ssl.openssl.ssl-conf-cmd += ("Groups" => "secp384r1")
# Modern configuration, tweak to your needs
# HSTS(15768000 seconds = 6 months)
#setenv.add-response-header  = (
#	"Strict-Transport-Security" => "max-age=15768000;"
#)


######## mod_status config ########
# This is generally handy and won't expose any sensitive info.
#status.status-url  = "/server-status"
# This will expose some of your configuration to the world!
#status.config-url  = "/server-config"


######## mod_deflate config ########
deflate.cache-dir   = var.statedir + "/cache/compress"
deflate.mimetypes   = ("text/plain", "text/html")
deflate.allowed-encodings = ( "zstd", "gzip", "deflate" )


######## mod_magnet config ########
# programmatic manipulation of requests using lua
# https://wiki.lighttpd.net/mod_magnet


######## mod_proxy config ########
# See proxy.txt in lighttpd-doc package for more info.
# proxy.server               = ( ".php" =>
#                               ( "localhost" =>
#                                 (
#                                   "host" => "192.168.0.101",
#                                   "port" => 80
#                                 )
#                               )
#                             )
# }}}


######## mod_auth config ########
# See authentication.txt in lighttpd-doc package for more info.
#auth.backend               = "plain"
#auth.backend.plain.userfile = "lighttpd.user"
#auth.backend.plain.groupfile = "lighttpd.group"

#auth.backend.ldap.hostname = "localhost"
#auth.backend.ldap.base-dn  = "dc=my-domain,dc=com"
#auth.backend.ldap.filter   = "(uid=$)"

#auth.require               = ( "/server-status" =>
#                               (
#                                 "method"  => "digest",
#                                 "realm"   => "download archiv",
#                                 "require" => "user=jan"
#                               ),
#                               "/server-info" =>
#                               (
#                                 "method"  => "digest",
#                                 "realm"   => "download archiv",
#                                 "require" => "valid-user"
#                               )
#                             )


######## mod_rewrite config ########
# Apache-style mod_rewrite for implementing URL rewriting.
# See rewrite.txt in lighttpd-doc package for more info.
#
#url.rewrite = (
#	"^/$"		=>		"/server-status"
#)


######## mod_redirect config ########
# See redirect.txt in lighttpd-doc package for more info.
#
#url.redirect = (
#	"^/wishlist/(.+)"		=>		"http://www.123.org/$1"
#)


######## mod_expire config ########
# It is highly recommended you configure Expire: headers correctly to
# conserve bandwidth, especially for users on slow links.
#expire.url = (
#	"/buggy/"		=>		"access 2 hours",
#	"/asdhas/"		=>		"access plus 1 seconds 2 minutes"
#)


######## mod_webdav config ########
# lighttpd can act as a WebDAV server.
# See webdav.txt in lighttpd-doc package for more info.
#
#$HTTP["url"] =~ "^/dav($|/)" {
#	webdav.activate = "enable"
#	webdav.is-readonly = "enable"
#}


######## Debugging options ########
# debug.log-request-header   = "enable"
# debug.log-response-header  = "enable"
# debug.log-request-handling = "enable"
# debug.log-file-not-found   = "enable"

# vim: set ft=conf foldmethod=marker et :
