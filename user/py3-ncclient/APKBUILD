# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=py3-ncclient
_pkgname=ncclient
_p="${_pkgname#?}"
_p="${_pkgname%"$_p"}"
pkgver=0.6.9
pkgrel=0
pkgdesc="Python library for NETCONF clients"
url="https://pypi.org/project/ncclient/"
arch="noarch"
license="Apache-2.0"
depends="python3 py3-lxml py3-paramiko"
checkdepends="py3-pytest py3-mock py3-iniconfig py3-toml"
makedepends="libxml2-dev libxslt-dev"
subpackages=""
source="https://files.pythonhosted.org/packages/source/$_p/$_pkgname/$_pkgname-$pkgver.tar.gz
	https://raw.githubusercontent.com/ncclient/ncclient/v${pkgver}/test/unit/transport/rpc-reply/get-software-information-rfc.xml
	https://raw.githubusercontent.com/ncclient/ncclient/v${pkgver}/test/unit/transport/rpc-reply/get-software-information.xml
	https://raw.githubusercontent.com/ncclient/ncclient/v${pkgver}/test/unit/ssh_config
	"
builddir="$srcdir/ncclient-$pkgver"

unpack() {
	default_unpack
	mkdir -p "$builddir"/test/unit/transport/rpc-reply
	mv "$srcdir"/*.xml "$builddir"/test/unit/transport/rpc-reply/
	mv "$srcdir"/ssh_config "$builddir"/test/unit/
}

build() {
	python3 setup.py build
}

check() {
	# Requires nose
	rm "$builddir"/test/unit/test_xml_.py

	pytest
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="ea5abc0dae81c337dbb22b8a15a63b6af8a3d5fbae9b621137957cc33c4688c4930a941f568a02be10256bcead9047bbe4a3fd8b29639f6a7626bb28244358a9  ncclient-0.6.9.tar.gz
88cdb37bcc1991abdb09d71ee182754f881247ae580fdb866e16047d7ec0117daa2916405504ee92e9defb398794dc8fb8e5d2fb937498c5cfacae203b721979  get-software-information-rfc.xml
c89ef648818e0c7e815b96a923fb48f8a7f3f72ef4d3e4a1dc6d5d5a368327ff06a4a2870d81bd5076ad0f25e8849ae00faa5fbf74294c7ebada8a5a84aa9a1a  get-software-information.xml
d94e0263c757647a14771fb5e12e843a4bcda1cc4f5ac0afa87d07f5abe8050c464867dc0a70f1f4c6503be844b2fb0701b449d1d89a358bc74139a543087be3  ssh_config"
