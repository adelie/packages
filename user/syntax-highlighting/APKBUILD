# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=syntax-highlighting
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework for rendering programming code with formatting"
url="https://www.kde.org/"
arch="all"
license="MIT AND CC0-1.0 AND GPL-2.0-only AND LGPL-2.0+ AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
checkdepends="qt5-qtxmlpatterns-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev perl
	doxygen graphviz qt5-qtdeclarative-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/syntax-highlighting-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="34e3ec926ae576e0366216c67ee73ef62c538e38369d664ca104951cf08a52848fd7b4d75e29ef23650259b113af727bf71837b6b6ced0f63d4264c0c903acb2  syntax-highlighting-5.94.0.tar.xz"
