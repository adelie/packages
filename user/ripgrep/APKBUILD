# Contributor: Gentoo Rust Maintainers <rust@gentoo.org>
# Contributor: Molly Miller <adelie@m-squa.red>
# Maintainer: Samuel Holland <samuel@sholland.org>
pkgname=ripgrep
pkgver=14.1.0
pkgrel=0
pkgdesc="Recursively searches directories for a regex pattern"
url="https://github.com/BurntSushi/ripgrep"
arch="all"
license="Unlicense"
depends=""
makedepends="cargo pcre2-dev"
subpackages=""
source=""

# dependencies taken from Cargo.lock
cargo_deps="
$pkgname $pkgver
aho-corasick 1.1.2
anyhow 1.0.79
autocfg 1.1.0
bstr 1.9.0
cc 1.0.83
cfg-if 1.0.0
crossbeam-deque 0.8.4
crossbeam-epoch 0.9.17
crossbeam-utils 0.8.18
encoding_rs 0.8.33
encoding_rs_io 0.1.7
globset 0.4.14
grep 0.3.1
grep-cli 0.1.10
grep-matcher 0.1.7
grep-pcre2 0.1.7
grep-printer 0.2.1
grep-regex 0.1.12
grep-searcher 0.1.13
ignore 0.4.22
itoa 1.0.10
jemalloc-sys 0.5.4+5.3.0-patched
jemallocator 0.5.4
jobserver 0.1.27
lexopt 0.3.0
libc 0.2.151
libm 0.2.8
log 0.4.20
memchr 2.7.1
memmap2 0.9.3
num-traits 0.2.17
packed_simd 0.3.9
pcre2 0.2.6
pcre2-sys 0.2.8
pkg-config 0.3.28
proc-macro2 1.0.76
quote 1.0.35
regex-automata 0.4.3
regex-syntax 0.8.2
ryu 1.0.16
same-file 1.0.6
serde 1.0.195
serde_derive 1.0.195
serde_json 1.0.111
syn 2.0.48
termcolor 1.4.0
textwrap 0.16.0
unicode-ident 1.0.12
walkdir 2.4.0
winapi 0.3.9
winapi-i686-pc-windows-gnu 0.4.0
winapi-util 0.1.6
winapi-x86_64-pc-windows-gnu 0.4.0
"

source="$source $(echo $cargo_deps | sed -E 's#([[:graph:]]+) ([[:graph:]]+)#\1-\2.tar.gz::https://crates.io/api/v1/crates/\1/\2/download#g')"

prepare() {
	export CARGO_HOME="$srcdir/cargo-home"
	export CARGO_VENDOR="$CARGO_HOME/adelie"

	(builddir=$srcdir; default_prepare)

	mkdir -p "$CARGO_VENDOR"
	cat <<- EOF > "$CARGO_HOME/config.toml"
		[source.adelie]
		directory = "${CARGO_VENDOR}"

		[source.crates-io]
		replace-with = "adelie"
		local-registry = "/nonexistant"
	EOF

	for _dep in $(echo $cargo_deps | sed -E 's#([[:graph:]]+) ([[:graph:]]+)#\1-\2#g'); do
		ln -s "$srcdir/$_dep" "$CARGO_VENDOR/$_dep"
		_sum=$(sha256sum "$srcdir/$_dep.tar.gz" | cut -d' ' -f1)
		cat <<- EOF > "$CARGO_VENDOR/$_dep/.cargo-checksum.json"
		{
			"package":"$_sum",
			"files":{}
		}
		EOF
	done
}

build() {
	export CARGO_HOME="$srcdir/cargo-home"
	export PCRE2_SYS_STATIC=0
	cargo build -j $JOBS --features pcre2 --release
}

check() {
	export CARGO_HOME="$srcdir/cargo-home"
	cargo test -j $JOBS --features pcre2 --release
}

package() {
	export CARGO_HOME="$srcdir/cargo-home"
	cargo install --no-track --features pcre2 --path . --root="$pkgdir"/usr
}

sha512sums="eb0234269164758d6a9051f405771f259636a79094aeee0b667931747c1c3f0f03a764b3eab0be227b8242923a5777b1a6ef31fdd410f4ba4c674764e4fac1fa  ripgrep-14.1.0.tar.gz
61ef5092673ab5a60bec4e92df28a91fe6171ba59d5829ffe41fc55aff3bfb755533a4ad53dc7bf827a0b789fcce593b17e69d1fcfb3694f06ed3b1bd535d40c  aho-corasick-1.1.2.tar.gz
ecd6fb1367d494df18c0e274b336a133f3acf7b6a5487d20bdd06e08c7f1f729877086a0966e998221daff120504fadd2be2dc4219ed621f81b0a50c2bbc2011  anyhow-1.0.79.tar.gz
df972c09abbdc0b6cb6bb55b1e29c7fed706ece38a62613d9e275bac46a19574a7f96f0152cccb0239efea04ee90083a146b58b15307696c4c81878cd12de28f  autocfg-1.1.0.tar.gz
dc313a16c38ad881128977a20bb390e7c95a96d9530596433a7c4fd7f77d5fffd079d436006dd8d2bfc4aacdd7f0aff229504444250418f6aa3f8d6d4df9abba  bstr-1.9.0.tar.gz
742a248c3a7547bb220a0b9c97b67a831fab9b4ac21daa08c85a3966b9fe576088def33e16132fcabec9a2828a6fc437088bb045bfc98b2cea829df6742565a7  cc-1.0.83.tar.gz
0fb16a8882fd30e86b62c5143b1cb18ab564e84e75bd1f28fd12f24ffdc4a42e0d2e012a99abb606c12efe3c11061ff5bf8e24ab053e550ae083f7d90f6576ff  cfg-if-1.0.0.tar.gz
cc036613727a3aa8933bec419ba8a5fd2f506770ad2cf874ff757b048e0d29ea3f1b0e2b72d2d5352ed4b62c6764c7bbb10d0d4e217176da26bf5ae4bca0b68b  crossbeam-deque-0.8.4.tar.gz
9bdeb0415ea26250547ed9f34a8dd4c17f9379e3c56f8f17315e48f9bf63ce1b79c37786ead0f4df026e31b0b5965d181c36839d891c14149529ac42b5cedea4  crossbeam-epoch-0.9.17.tar.gz
977f41d7596a8d206fab665570f155479e15a83698fa54e714305d56ce32d78abc6f778b5f6e3c379ed1da27e241639cf4d0cd12922383f774ae85b32a4dc550  crossbeam-utils-0.8.18.tar.gz
20d28a4c0ff0710f911f510be56c2bc5e7514b76d370493d2b89b7f25d1c0cd46ffa64862e54fc472f07f928a2cc24cf9d790a37751cafafc81f263705aac4bc  encoding_rs-0.8.33.tar.gz
0c7d33616e96fcdf225599dbf469232d150fa4bef5809346c4003ff7fa0362a1cded8f3392fb5945f4950e2b2cd71b60c93025d73ecfdd2ba4fda5e03a16902c  encoding_rs_io-0.1.7.tar.gz
e83e4f5e42a97609e2579b09f49c2cb0a76a94e7e2975c5871f2c5af5d3b809736e0a7b220404c582f4a0c7eebdbfad6cb432e89b26401b58f2b253f8c0151bc  globset-0.4.14.tar.gz
2ce36f84a561f5a66b67e2f0b79ecb9d7a3220fd48f4b0215c4964d2e686f096aa52890b79ae770f4857a1393a2e482750e171606a1c25f1cc6295bf439d694f  grep-0.3.1.tar.gz
057bc0192bc2dab55babdc40c94d421f2e30d3001801469480bbdd77eeee896c3b4a2f17e7a648e7d21f178636ab1363eb3b3abbd3a6e453d07d1c2d60a53112  grep-cli-0.1.10.tar.gz
bc15eec0b6f409926099f2aa46f040c8de240e50537358a50d12ce7cfaff4894ec43910d2e01ec087c2c0777773ef25e2f0d653c33a59c5162e23e85e76ec9f8  grep-matcher-0.1.7.tar.gz
2efcf39decd5472fedde41bc73be15e3145bf860aa8b5635f7c0fd47bcd8498ad75a32f8ec09cac872eaea7c4bcc14ee22be9612f0415d9fb0a86f041567cf92  grep-pcre2-0.1.7.tar.gz
2dc876143cd8480dd2cc4be5397de21eaddd5cce4a23494a5a68c8d9814d0cdec01fd14d5cd6aa23a17b3a43fcfc2843d4057b5dce144dd1cb7a936c53ee326d  grep-printer-0.2.1.tar.gz
95df80807aa14cdbefa4d8d8cb8fd91263fe26009ba3ecb4a30acff1332bd51b87c94935a489405c6bf98e76126a5a06cfe7f5b677001da4e18c18375c00f142  grep-regex-0.1.12.tar.gz
8e7ecbcbe09f5a57fad71e0010d2cc29f3b622bf6ff80cd2d75a8898b43ec7ca43a1d8efb4921032f865a81c38176c694faf6627cc747350a31510bef662cf9f  grep-searcher-0.1.13.tar.gz
9647a4887258dd970c87798dee32a16c1cda75ce3e352829f78d97e1b786d67f74d89b2fd76d48a607fe795213965c04c9dfb47713f8fa80561f87ac465956d9  ignore-0.4.22.tar.gz
fc496be0873e31afa6ddf7f2958030f8fcb517cadb86a48280a0c50ce50460afda189af1e6c26e5ff6f85c750d772c21503864c32b526c4fe4bcdb5b07918538  itoa-1.0.10.tar.gz
b81c75efa70b869abe08c0805fd9545436ccd4e6ce24bb7355c1b3684a03bbf234af0680854f3fd9b519e7d59296d32ef04497a1f45584ed8de462e4e21d60c1  jemalloc-sys-0.5.4+5.3.0-patched.tar.gz
23b0107ce5fa6a31cbe2cd07beeee973543c1e5f6f2a57fde5a313dfdf0b7138a0675fd00b1b4823d60eeffe02b9159d0261579c9ee4ce4246fcf799f9c1f853  jemallocator-0.5.4.tar.gz
2681234952be7a5ae67770d45f4d52e8278347f79b349bda594626712b1fba595a4cf46e8fb5426b548c325a23243facf7fbdc01ae0e853e492aff39444879a5  jobserver-0.1.27.tar.gz
7ce3856a54fa6274ea3d5a4fe353f4a0411f5f6754fe8a5e7a114705f063300a93aa2efad765bc1dd0244caee15a8cf44a3de540a4db0626ed2ac6476bdb6843  lexopt-0.3.0.tar.gz
2039bdfd704c5a876ff7d609622bb968260d6e0fd264ca457e92b75660503eca10e1ff21864b3c919d2656de521f03ef443895f5c0150271ae2502857742b0ec  libc-0.2.151.tar.gz
753df71bb2c838abbac360db2e4400c2d931185ecff993da51a979870f2c3f7ac017380fadee4622102fb4b37ebcc1960b0cbd295afc9cd1cb3307524e1f39c5  libm-0.2.8.tar.gz
8661b0c71d3b7fc0d679aa3d7f06910e6d3da1c53862aa06526000e1bcaa0b0b068415a1a9ab317c318f00d15346dba8a4f5d2a60d8850790bed9cfaaf757b3e  log-0.4.20.tar.gz
5120496faa31fc427c8b4178461a262b3a34d70eddb7ad17a19d6db8b9969c9e113d3625b5e6dc677087fc80907377b00ba0421aba9a92cf73ca2849d932f473  memchr-2.7.1.tar.gz
025068aaf27296731c3a4fd8df3b745cd37e86a564aa5ab9a8eccddb99fa054820eff5a6a858c3f443666bf594b149c84045f43a555353286b066b3f1ff514cf  memmap2-0.9.3.tar.gz
4d47d3e2f5a31019e038e609897cb0cef1ba061b35cee7e2a02e65179dcdd4960bd5b9bc759b5c013d699b3fbd9b014940a15e36658f7d4fd12cb0c7841c5b4e  num-traits-0.2.17.tar.gz
1336ed8aee50a580784f38388f0cc2f0a8639149333e94b7f83aa7cc314a80c60b66ac203a7302cd4cf01f3d0b89da58d2ce9038b7685840fbc1b758217dbd1b  packed_simd-0.3.9.tar.gz
101a0c4f1300ef7184aafba9048e0a7ad310d86d5236871e2b2477d4cedb23ac8bd6c6cd48970145eccbfb684f3efb5a80100e056e0c6052bc6fdbc4ad308317  pcre2-0.2.6.tar.gz
4f0b21a0dff41ccbfb1dd6387d63b066e71002a7b96f76d8ab5ff34c056cd862337c1ef08daf30436fdf68eba4eb98f6e8a1ad6d2903e2e348ed9b85e2a7f616  pcre2-sys-0.2.8.tar.gz
1ef92c5cf5320fb2e7d9e337edae08eaef01c762cb18406f732af731e5a94bef95be36cc0e0f5643f47a925aa127c5520a961c034e7b86357715656707a6e0e4  pkg-config-0.3.28.tar.gz
2ea7ade475171166489ab3e745e8c526e49c7521bc39b1bfec6dd2fd0807fd3cc5579235f77534be855f9ecab481205e77e66b14ebb22e66d2c3cff842567247  proc-macro2-1.0.76.tar.gz
f5314fb6af17cf36c228e1970c569c29ec248954a450a5f90ba9e2896d04f74904c9cec5a1f74325f2489295a94491eee4ce8fb461e22cd4b34e53f1f881efd2  quote-1.0.35.tar.gz
4fc82fe3556f829956c3172447589555ef286fd66ee9a445cbdcdbe57970655e35b6eb0895ba02c344d826609257e0c95d3f7f51858aa260103bed7b08d8c1a8  regex-automata-0.4.3.tar.gz
301dde555f300298f2594490ccd8b92033e4917fe9b8671b8a97db6c827793c73969be85a92999964dcaf3177edda51abeb576811ad6cab9772964dc0a77e728  regex-syntax-0.8.2.tar.gz
89e3e18346ae6e4035787766f286a6eb62f5294e39ad902cc29e95c53781c4e5dd629f567f22ecc7e9fe9d1c273323da9b867aadc9cd8a066727c58330b51b10  ryu-1.0.16.tar.gz
3ba35309742c8db63210d9ea78bff4ecd80471d69e6238eb96c7bf0673814f221e2d838fe6311bfc5a0e71b4a7ccba33e07859c0b9cff2171969ff08a4214a7c  same-file-1.0.6.tar.gz
0002ecb933d08fbdc8585ee74efb4048d385459f629261f5a4a8401b794f1c02b8296db79f479013a0b2a47ba99bee6c02d60d0fc2454e32f1b038b4071e8727  serde-1.0.195.tar.gz
8a82ae4e7cfe0c3ac27688cc95d74bf4c2e10c1fe845ab28fb57579b2f47f36d13218c2ae603f93f00114065d47c3e46166dea39b205be673f443c34305dacce  serde_derive-1.0.195.tar.gz
11851e34a9a40de32654a12ada1d1bd01abb11531c5804f5d0747bbb01bf59025dd92be916104fbc278bbba5c89c77920941b83c0ac2f0da23f5d61a7788e83f  serde_json-1.0.111.tar.gz
926c0ad55cc743df20f9166b6d71fd44421aeb1f073fd86b6ddb53e13cd508b539c0360ce64b07b65597bb14639c8f33acf7e60951e1e9128624517aa3aee325  syn-2.0.48.tar.gz
36c8550fe85cb90c4fdd1a6e3f60b42b6c71f65b6128da97d5ad6aac8350d907410899ad4a47bb4dc72ea6d00bc69e5a1ccc80ee9b0e80a1a27912e233f3e416  termcolor-1.4.0.tar.gz
97ae8acece5663e1a6b08b827179e96d5ad0ee67d635888cc3d83454b52cf48fce97eb0eba374ba4747834099c74f43d66d9fec868e84be45369a42c1aaec2c3  textwrap-0.16.0.tar.gz
bc1824e1e4452a40732fc69874d7e1a66f7803717a314790dcf48867eba34bc9441331ef031e386912e52c385645c25b6ed39d4f149973b5b97371b1b96b1920  unicode-ident-1.0.12.tar.gz
09e1bc852c01b452c95b26a369831a97bc5c9e0ada3111c73774570dd73bb5b9e4735317d5572304fb48dca44ce7b9f77bbd17c418b6b047b2ab17b8bb42d9d9  walkdir-2.4.0.tar.gz
ff8b7b78065f3d8999ec03c725a0460ebc059771bf071c7a3df3f0ecd733edf3b0a2450024d4e24e1aedddaecd9038ce1376c0d8bbf45132068cf45cf4a53a97  winapi-0.3.9.tar.gz
a672ccefd0730a8166fef1d4e39f9034d9ae426a3f5e28d1f4169fa5c5790767693f281d890e7804773b34acdb0ae1febac33cde8c50c0044a5a6152c7209ec2  winapi-i686-pc-windows-gnu-0.4.0.tar.gz
b1c949f9bcd34c1949a9d3a7bde6ce62fcf3d2cb66df60af41fe67a9d1acb24e571cdd5ac721be9f1ee4b3af5ef5149b5724ad6e02b558e124ef2a4412d12db9  winapi-util-0.1.6.tar.gz
4a654af6a5d649dc87e00497245096b35a2894ae66f155cb62389902c3b93ddcc5cf7d0d8b9dd97b291d2d80bc686af2298e80abef6ac69883f4a54e79712513  winapi-x86_64-pc-windows-gnu-0.4.0.tar.gz"
