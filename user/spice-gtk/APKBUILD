# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=spice-gtk
pkgver=0.42
pkgrel=0
pkgdesc="A GTK+ widget for SPICE clients"
url="https://www.spice-space.org/"
arch="all"
# suid: ACL helper for USB redirection
options="suid"
license="LGPL-2.1+ AND LGPL-2.0+ AND BSD-3-Clause AND MIT AND GPL-3.0+ AND LGPL-2.0-only AND GPL-2.0+"
depends="gst-plugins-good"
depends_dev="gobject-introspection-dev gtk+3.0-dev"
makedepends="$depends_dev acl-dev bash cyrus-sasl-dev eudev-dev
	gst-plugins-base-dev gstreamer-dev gstreamer-tools gtk-doc
	json-glib-dev libjpeg-turbo-dev
	libxrandr-dev lz4-dev meson openssl-dev opus-dev polkit-dev
	py3-pyparsing py3-six spice-protocol
	zlib-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang spicy
	spice-glib:glib"
source="https://www.spice-space.org/download/gtk/$pkgname-$pkgver.tar.xz
	fix-dirty-spicy-corruption.patch
	"

build() {
	# Note: pulseaudio support is disabled because it's deprecated.
	# Audio is still supported through gstreamer.
	#
	# USB redirection is disabled until there is reasonable belief
	# that it is endian safe.
	# https://gitlab.freedesktop.org/spice/spice-gtk/-/issues/120
	meson setup \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--buildtype=release \
		-Dusbredir=disabled \
		-Dcoroutine=gthread \
		. output
	ninja -C output
}

check() {
	ninja -C output test
}

package() {
	DESTDIR="$pkgdir" ninja -C output install
}

spicy() {
	pkgdesc="SPICE client (remote virtual machine access)"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

glib() {
	pkgdesc="$pkgdesc (GLib libraries)"
	mkdir -p "$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/lib/girepository-1.0/
	mv "$pkgdir"/usr/lib/*-glib-*.so* \
		"$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/girepository-1.0/SpiceClientGLib-*.typelib \
		"$subpkgdir"/usr/lib/girepository-1.0/
}

sha512sums="fd567e35f6d4ebfe6ef004f358dca4c41254336f55f7dd26cf67b62b2acb4866907186bd0526b7cb52b0c24020cdc8809251127498a8d357555bb0c5d3b8f137  spice-gtk-0.42.tar.xz
4da61b94db02f4db400be45933bb6d3d3aa05137dfba78dcbc1757d9709c4252984e12f61f88c953597925e3145f5637fde1e76422513e0d1808b53c61c521c7  fix-dirty-spicy-corruption.patch"
