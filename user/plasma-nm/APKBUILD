# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=plasma-nm
pkgver=5.24.5
pkgrel=0
pkgdesc="NetworkManager integration for KDE Plasma desktop"
url="https://www.kde.org/"
arch="all"
license="(LGPL-2.1-only OR LGPL-3.0-only) AND GPL-2.0-only AND GPL-2.0+ AND LGPL-2.0+"
depends="kirigami2 mobile-broadband-provider-info prison-quick"
makedepends="cmake extra-cmake-modules kauth-dev kcodecs-dev kcompletion-dev
	kconfigwidgets-dev kcoreaddons-dev kdbusaddons-dev kdeclarative-dev
	ki18n-dev kiconthemes-dev kio-dev kitemviews-dev kjobwidgets-dev
	knotifications-dev kpackage-dev kservice-dev kwallet-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev modemmanager-qt-dev
	networkmanager-dev networkmanager-qt-dev plasma-framework-dev
	prison-dev qca-dev qt5-qtbase-dev qt5-qtdeclarative-dev solid-dev"
subpackages="$pkgname-lang"
# We don't want to pull NM into plasma-meta, so we do this as a workaround.
install_if="plasma-desktop networkmanager"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-nm-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="0b64b2f231d7e97f901251e274bebd53b22c19ddcfa67cc66947f48adebe61c6bde21f339896f267af22f4112b9a392d331345999000c43cb5b1e51680668ec8  plasma-nm-5.24.5.tar.xz"
