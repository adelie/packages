# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gnu-ghostscript
pkgver=9.14.1
pkgrel=4
pkgdesc="PostScript utilities"
url="https://www.gnu.org/software/ghostscript/"
arch="all"
license="AGPL-3.0+ WITH PDF-exception"
depends=""
makedepends="cups-dev dbus-dev fontconfig-dev freetype-dev lcms2-dev libice-dev
	libjpeg-turbo-dev libpaper-dev libpng-dev libx11-dev libxext-dev
	libxt-dev tiff-dev zlib-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs"
source="ftp://ftp.gnu.org/gnu/ghostscript/gnu-ghostscript-$pkgver.tar.xz
	do-not-use-sprintf.patch
	locksafe.patch
	ft-callback-def.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-dynamic \
		--with-system-libtiff
	make
	make so
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	make DESTDIR="$pkgdir" soinstall
	install -D -m644 base/*.h "$pkgdir"/usr/include/ghostscript
}

sha512sums="658f6ae14b29965c7959b9bd1463760d7fb03b35251446fc37101dbe7ee2866c74a0803e22b2f4488be3221d026578be1d6be7b562ff240036134fbf83edabf9  gnu-ghostscript-9.14.1.tar.xz
1c4d79633ecece9f4de7a672f2888939be7d1e0ba24f3a66500fdfbb37e081e0f323a5efa0d1547fed51206e5a95d3b0659c0ea845d0bc333f281dfb88fcc040  do-not-use-sprintf.patch
ee06c1753147bcce85e3cdc6c254ef2c75b7f1dfdc0a9bce82bc24bb8d814470ba81be5372fb4bec3a999190f53c4615226fd3dbf05969c436b480eb2ec14f37  locksafe.patch
c404dacba3e1739933b296c9ecd305f8df48e586c1d593750c5e1ca490232c8897eb20596e96262d5ae59683a5f70717ad99f4a7f5537a7b3ae9bb24f6013385  ft-callback-def.patch"
