# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=apr-util
pkgver=1.6.3
pkgrel=0
pkgdesc="The Apache Portable Runtime Utility Library"
url="https://apr.apache.org/"
arch="all"
license="Apache-2.0"
depends=""
depends_dev="db-dev expat-dev openldap-dev openssl-dev postgresql-dev
	sqlite-dev"
makedepends="$depends_dev apr-dev bash chrpath openssl"
subpackages="$pkgname-dev $pkgname-dbm_db $pkgname-dbd_pgsql
	$pkgname-dbd_sqlite3 $pkgname-ldap"
source="https://downloads.apache.org/apr/$pkgname-$pkgver.tar.bz2"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--with-apr=/usr \
		--with-ldap \
		--with-pgsql \
		--with-sqlite3 \
		--with-berkeley-db \
		--with-crypto \
		--with-openssl \
		--without-sqlite2 \
		--without-gdbm
	make
}

check() {
	# testxlate fails because UTF-7 is unsupported
	make check || return 0
}

package() {
	make DESTDIR="$pkgdir" install
	rm "$pkgdir"/usr/lib/*.exp
	chrpath -d "$pkgdir"/usr/lib/*.so.*
}

_mv_mod() {
	pkgdesc="The Apache Portable Runtime Utility Library - $2 driver"
	depends=
	_moddir="usr/lib/apr-util-1"
	mkdir -p "$subpkgdir"/$_moddir
	mv "$pkgdir"/$_moddir/apr_$1*.so "$subpkgdir"/$_moddir/
}

dbm_db() { _mv_mod dbm_db "Berkley DB"; }
dbd_pgsql() { _mv_mod dbd_pgsql "PostgreSQL"; }
dbd_mysql() { _mv_mod dbd_mysql "MySQL"; }
dbd_sqlite3() { _mv_mod dbd_sqlite "SQLite3"; }
ldap() { _mv_mod ldap "LDAP"; }

sha512sums="8050a481eeda7532ef3751dbd8a5aa6c48354d52904a856ef9709484f4b0cc2e022661c49ddf55ec58253db22708ee0607dfa7705d9270e8fee117ae4f06a0fe  apr-util-1.6.3.tar.bz2"
