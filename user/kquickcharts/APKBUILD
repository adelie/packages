# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kquickcharts
pkgver=5.94.0
pkgrel=0
pkgdesc="A QtQuick plugin providing high-performance charts"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires X11
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev qt5-qtdeclarative-dev qt5-qtquickcontrols2-dev cmake
	extra-cmake-modules"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kquickcharts-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="5877118f4d088a256eab6dbff6efcd88b25b7117d6be4f0debc6af5fe9244836fe683658a5593ee53e94d33ff08b48a31631605c5eae63827864432fd4c4d88b  kquickcharts-5.94.0.tar.xz"
