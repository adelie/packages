# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=bluedevil
pkgver=5.24.5
pkgrel=0
pkgdesc="Bluetooth framework for KDE"
url="https://userbase.kde.org/Bluedevil"
arch="all"
license="GPL-2.0+ AND LGPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only) AND GPL-3.0+"
depends="kded kirigami2 shared-mime-info"
makedepends="bluez-qt-dev cmake extra-cmake-modules kauth-dev kcodecs-dev
	kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kdbusaddons-dev kdeclarative-dev ki18n-dev kio-dev kitemviews-dev
	kjobwidgets-dev knotifications-dev kpackage-dev kservice-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev plasma-framework-dev
	qt5-qtbase-dev qt5-qtdeclarative-dev solid-dev kirigami2-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/bluedevil-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="02b24ba96169d733166ac6b906d995414bcf31b4a459d695ec018e7120b6e69ba01ec0f8bfa6e73d1fc8c965fc6fc285bbcefaea86a9dc230cd6a0247e566757  bluedevil-5.24.5.tar.xz"
