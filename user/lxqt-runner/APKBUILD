# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=lxqt-runner
pkgver=1.4.0
_lxqt=0.13.0
pkgrel=0
pkgdesc="Qt-based application launcher for LXQt"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules lxqt-build-tools>=$_lxqt
	liblxqt-dev>=${pkgver%.*} lxqt-globalkeys-dev>=${pkgver%.*}
	muparser-dev kwindowsystem-dev menu-cache-dev qt5-qttools-dev
	qt5-qtsvg-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/lxqt-runner/releases/download/$pkgver/lxqt-runner-$pkgver.tar.xz
	revert-kwindowsystem-bump.patch"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="53ead3b6cd45cbf57a01c06861a97f20ddab9f3a5f3a90b3b9c3e6c3b67344feec99e11e14a90a0d0a6beb7e0cf74e092b5664d5f8ec32f827893623e104ee12  lxqt-runner-1.4.0.tar.xz
fe7d33dbdcb4500391c586bfbd43ab3e4dc660810a482fa38dcd6142b8f50df8f3ab0b53e7c71eeb09ecdc48056d490347cf68a62d01de809d5899f6152d1ca7  revert-kwindowsystem-bump.patch"
