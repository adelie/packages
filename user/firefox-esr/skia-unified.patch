This patch is not complete.

It attempts to completely remove endian-specific surface formats from the
entire tree, then replace it with a single swizzle in Skia.

Most things are working, including most Web sites and graphics.  However,
notably, native widgets (like menus, or 'Remember password?' popups) cause
an assertion failure.

We need to ship beta6 and this gets people a browser with many unfortunate
caveats, which is better than no browser at all.

I intend to remain working with upstream on finding a way forward with the
overall idea of this patch and hope to land something eventually.

diff --git a/dom/canvas/CanvasRenderingContext2D.cpp b/dom/canvas/CanvasRenderingContext2D.cpp
--- a/dom/canvas/CanvasRenderingContext2D.cpp
+++ b/dom/canvas/CanvasRenderingContext2D.cpp
@@ -383,7 +383,7 @@ class AdjustedTargetForFilter {
     }
 
     if (!mFinalTarget->CanCreateSimilarDrawTarget(mSourceGraphicRect.Size(),
-                                                  SurfaceFormat::B8G8R8A8)) {
+                                                  SurfaceFormat::OS_RGBA)) {
       mTarget = mFinalTarget;
       mCtx = nullptr;
       mFinalTarget = nullptr;
@@ -391,7 +391,7 @@ class AdjustedTargetForFilter {
     }
 
     mTarget = mFinalTarget->CreateSimilarDrawTarget(mSourceGraphicRect.Size(),
-                                                    SurfaceFormat::B8G8R8A8);
+                                                    SurfaceFormat::OS_RGBA);
 
     if (mTarget) {
       // See bug 1524554.
@@ -419,7 +419,7 @@ class AdjustedTargetForFilter {
     }
 
     RefPtr<DrawTarget> dt = mFinalTarget->CreateSimilarDrawTarget(
-        aRect.Size(), SurfaceFormat::B8G8R8A8);
+        aRect.Size(), SurfaceFormat::OS_RGBA);
 
     if (dt) {
       // See bug 1524554.
@@ -516,7 +516,7 @@ class AdjustedTargetForShadow {
     bounds.RoundOut();
     if (!bounds.ToIntRect(&mTempRect) ||
         !mFinalTarget->CanCreateSimilarDrawTarget(mTempRect.Size(),
-                                                  SurfaceFormat::B8G8R8A8)) {
+                                                  SurfaceFormat::OS_RGBA)) {
       mTarget = mFinalTarget;
       mCtx = nullptr;
       mFinalTarget = nullptr;
@@ -524,7 +524,7 @@ class AdjustedTargetForShadow {
     }
 
     mTarget = mFinalTarget->CreateShadowDrawTarget(
-        mTempRect.Size(), SurfaceFormat::B8G8R8A8, mSigma);
+        mTempRect.Size(), SurfaceFormat::OS_RGBA, mSigma);
 
     if (mTarget) {
       // See bug 1524554.
@@ -2117,7 +2117,7 @@ CanvasRenderingContext2D::GetOptimizedSn
 }
 
 SurfaceFormat CanvasRenderingContext2D::GetSurfaceFormat() const {
-  return mOpaque ? SurfaceFormat::B8G8R8X8 : SurfaceFormat::B8G8R8A8;
+  return mOpaque ? SurfaceFormat::OS_RGBX : SurfaceFormat::OS_RGBA;
 }
 
 //
@@ -5295,7 +5295,7 @@ static already_AddRefed<SourceSurface> E
   }
 
   RefPtr<DrawTarget> subrectDT = aTargetDT->CreateSimilarDrawTarget(
-      roundedOutSourceRectInt.Size(), SurfaceFormat::B8G8R8A8);
+      roundedOutSourceRectInt.Size(), SurfaceFormat::OS_RGBA);
 
   if (subrectDT) {
     // See bug 1524554.
@@ -6061,7 +6061,7 @@ void CanvasRenderingContext2D::DrawWindo
       }
     }
     drawDT = gfxPlatform::GetPlatform()->CreateOffscreenContentDrawTarget(
-        dtSize, SurfaceFormat::B8G8R8A8);
+        dtSize, SurfaceFormat::OS_RGBA);
     if (!drawDT || !drawDT->IsValid()) {
       aError.Throw(NS_ERROR_FAILURE);
       return;
@@ -6315,7 +6315,7 @@ void CanvasRenderingContext2D::EnsureErr
 
   RefPtr<DrawTarget> errorTarget =
       gfxPlatform::GetPlatform()->CreateOffscreenCanvasDrawTarget(
-          IntSize(1, 1), SurfaceFormat::B8G8R8A8);
+          IntSize(1, 1), SurfaceFormat::OS_RGBA);
   MOZ_ASSERT(errorTarget, "Failed to allocate the error target!");
 
   sErrorTarget.set(errorTarget.forget().take());
@@ -6437,7 +6437,7 @@ void CanvasRenderingContext2D::PutImageD
     dstData = lockedBits + dirtyRect.y * dstStride + dirtyRect.x * 4;
   } else {
     sourceSurface = Factory::CreateDataSourceSurface(
-        dirtyRect.Size(), SurfaceFormat::B8G8R8A8, false);
+        dirtyRect.Size(), SurfaceFormat::OS_RGBA, false);
 
     // In certain scenarios, requesting larger than 8k image fails.  Bug
     // 803568 covers the details of how to run into it, but the full
diff --git a/dom/canvas/ImageBitmap.cpp b/dom/canvas/ImageBitmap.cpp
--- a/dom/canvas/ImageBitmap.cpp
+++ b/dom/canvas/ImageBitmap.cpp
@@ -227,7 +227,7 @@ static already_AddRefed<DataSourceSurfac
   // this rectangle are outside the area where the input bitmap was placed, then
   // they will be transparent black in output."
   // So, instead, we force the output format to be SurfaceFormat::B8G8R8A8.
-  const SurfaceFormat format = SurfaceFormat::B8G8R8A8;
+  const SurfaceFormat format = SurfaceFormat::OS_RGBA;
   const int bytesPerPixel = BytesPerPixel(format);
   const IntSize dstSize =
       IntSize(positiveCropRect.width, positiveCropRect.height);
@@ -500,6 +500,11 @@ static already_AddRefed<layers::Image> C
 
   // Convert RGBA to BGRA
   RefPtr<DataSourceSurface> rgbaDataSurface = rgbaSurface->GetDataSurface();
+
+  if (SurfaceFormat::OS_RGBA == SurfaceFormat::R8G8B8A8) {
+    return CreateImageFromSurface(rgbaDataSurface);
+  }
+
   DataSourceSurface::ScopedMap rgbaMap(rgbaDataSurface,
                                        DataSourceSurface::READ);
   if (NS_WARN_IF(!rgbaMap.IsMapped())) {
@@ -724,7 +729,7 @@ SurfaceFromElementResult ImageBitmap::Su
   bool requiresCrop = !allowUncropped && hasCropRect;
   if (wantExactSize || requiresPremult || requiresCrop || mSurface) {
     RefPtr<DrawTarget> dt = Factory::CreateDrawTarget(
-        BackendType::SKIA, IntSize(1, 1), SurfaceFormat::B8G8R8A8);
+        BackendType::SKIA, IntSize(1, 1), SurfaceFormat::OS_RGBA);
     sfer.mSourceSurface = PrepareForDrawTarget(dt);
 
     if (!sfer.mSourceSurface) {
@@ -831,7 +836,7 @@ already_AddRefed<SourceSurface> ImageBit
     // black, even if the surface is opaque, so force to an alpha format in
     // that case.
     if (!surfPortion.IsEqualEdges(mPictureRect) && isOpaque) {
-      format = SurfaceFormat::B8G8R8A8;
+      format = SurfaceFormat::OS_RGBA;
     }
 
     // If we need to pre-multiply the alpha, then we need to be able to
diff --git a/dom/canvas/OffscreenCanvasDisplayHelper.cpp b/dom/canvas/OffscreenCanvasDisplayHelper.cpp
--- a/dom/canvas/OffscreenCanvasDisplayHelper.cpp
+++ b/dom/canvas/OffscreenCanvasDisplayHelper.cpp
@@ -181,7 +181,7 @@ bool OffscreenCanvasDisplayHelper::Commi
 
   MutexAutoLock lock(mMutex);
 
-  gfx::SurfaceFormat format = gfx::SurfaceFormat::B8G8R8A8;
+  gfx::SurfaceFormat format = gfx::SurfaceFormat::OS_RGBA;
   layers::TextureFlags flags = layers::TextureFlags::IMMUTABLE;
 
   if (!mCanvasElement) {
@@ -207,7 +207,7 @@ bool OffscreenCanvasDisplayHelper::Commi
 
   if (mData.mIsOpaque) {
     flags |= layers::TextureFlags::IS_OPAQUE;
-    format = gfx::SurfaceFormat::B8G8R8X8;
+    format = gfx::SurfaceFormat::OS_RGBX;
   } else if (!mData.mIsAlphaPremult) {
     flags |= layers::TextureFlags::NON_PREMULTIPLIED;
   }
diff --git a/gfx/2d/HelpersSkia.h b/gfx/2d/HelpersSkia.h
--- a/gfx/2d/HelpersSkia.h
+++ b/gfx/2d/HelpersSkia.h
@@ -26,6 +26,9 @@ namespace gfx {
 static inline SkColorType GfxFormatToSkiaColorType(SurfaceFormat format) {
   switch (format) {
     case SurfaceFormat::B8G8R8A8:
+#if MOZ_BIG_ENDIAN()
+      //MOZ_DIAGNOSTIC_ASSERT(false, "wrong way unsupported by Skia");
+#endif
       return kBGRA_8888_SkColorType;
     case SurfaceFormat::B8G8R8X8:
       // We probably need to do something here.
@@ -37,7 +40,9 @@ static inline SkColorType GfxFormatToSki
     case SurfaceFormat::R8G8B8A8:
       return kRGBA_8888_SkColorType;
     case SurfaceFormat::A8R8G8B8:
+#if MOZ_LITTLE_ENDIAN()
       MOZ_DIAGNOSTIC_ASSERT(false, "A8R8G8B8 unsupported by Skia");
+#endif
       return kRGBA_8888_SkColorType;
     default:
       MOZ_DIAGNOSTIC_ASSERT(false, "Unknown surface format");
@@ -49,20 +54,20 @@ static inline SurfaceFormat SkiaColorTyp
     SkColorType aColorType, SkAlphaType aAlphaType = kPremul_SkAlphaType) {
   switch (aColorType) {
     case kBGRA_8888_SkColorType:
-      return aAlphaType == kOpaque_SkAlphaType ? SurfaceFormat::B8G8R8X8
-                                               : SurfaceFormat::B8G8R8A8;
+      return aAlphaType == kOpaque_SkAlphaType ? SurfaceFormat::OS_RGBX
+                                               : SurfaceFormat::OS_RGBA;
     case kRGB_565_SkColorType:
       return SurfaceFormat::R5G6B5_UINT16;
     case kAlpha_8_SkColorType:
       return SurfaceFormat::A8;
     default:
-      return SurfaceFormat::B8G8R8A8;
+      return SurfaceFormat::OS_RGBA;
   }
 }
 
 static inline SkAlphaType GfxFormatToSkiaAlphaType(SurfaceFormat format) {
   switch (format) {
-    case SurfaceFormat::B8G8R8X8:
+    case SurfaceFormat::OS_RGBX:
     case SurfaceFormat::R5G6B5_UINT16:
       return kOpaque_SkAlphaType;
     default:
diff --git a/gfx/ipc/CrossProcessPaint.cpp b/gfx/ipc/CrossProcessPaint.cpp
--- a/gfx/ipc/CrossProcessPaint.cpp
+++ b/gfx/ipc/CrossProcessPaint.cpp
@@ -107,7 +107,7 @@ PaintFragment PaintFragment::Record(dom:
   nsContentUtils::FlushLayoutForTree(ds->GetWindow());
 
   // Initialize the recorder
-  SurfaceFormat format = SurfaceFormat::B8G8R8A8;
+  SurfaceFormat format = SurfaceFormat::OS_RGBA;
   RefPtr<DrawTarget> referenceDt = Factory::CreateDrawTarget(
       gfxPlatform::GetPlatform()->GetSoftwareBackend(), IntSize(1, 1), format);
 
@@ -254,7 +254,7 @@ bool CrossProcessPaint::Start(dom::Windo
         // Create the destination draw target
         RefPtr<DrawTarget> drawTarget =
             gfxPlatform::GetPlatform()->CreateOffscreenContentDrawTarget(
-                root->mSize, SurfaceFormat::B8G8R8A8);
+                root->mSize, SurfaceFormat::OS_RGBA);
         if (!drawTarget || !drawTarget->IsValid()) {
           CPP_LOG("Couldn't create (%d x %d) surface for fragment %" PRIu64
                   ".\n",
diff --git a/gfx/ipc/GfxMessageUtils.h b/gfx/ipc/GfxMessageUtils.h
--- a/gfx/ipc/GfxMessageUtils.h
+++ b/gfx/ipc/GfxMessageUtils.h
@@ -677,7 +677,7 @@ struct ParamTraits<GeckoProcessType>
 template <>
 struct ParamTraits<mozilla::gfx::SurfaceFormat>
     : public ContiguousEnumSerializer<mozilla::gfx::SurfaceFormat,
-                                      mozilla::gfx::SurfaceFormat::B8G8R8A8,
+                                      mozilla::gfx::SurfaceFormat::OS_RGBA,
                                       mozilla::gfx::SurfaceFormat::UNKNOWN> {};
 
 template <>
diff --git a/gfx/layers/Compositor.cpp b/gfx/layers/Compositor.cpp
--- a/gfx/layers/Compositor.cpp
+++ b/gfx/layers/Compositor.cpp
@@ -36,7 +36,7 @@ class CompositorRecordedFrame final : pu
     gfx::IntSize size = mBuffer->GetSize();
 
     mSurface = gfx::Factory::CreateDataSourceSurface(
-        size, gfx::SurfaceFormat::B8G8R8A8,
+        size, gfx::SurfaceFormat::OS_RGBA,
         /* aZero = */ false);
 
     if (!mBuffer->MapAndCopyInto(mSurface, size)) {
diff --git a/gfx/layers/ImageDataSerializer.cpp b/gfx/layers/ImageDataSerializer.cpp
--- a/gfx/layers/ImageDataSerializer.cpp
+++ b/gfx/layers/ImageDataSerializer.cpp
@@ -288,16 +288,16 @@ already_AddRefed<DataSourceSurface> Data
   RefPtr<DataSourceSurface> result;
   if (aSurface) {
     MOZ_ASSERT(aSurface->GetSize() == size);
-    MOZ_ASSERT(aSurface->GetFormat() == gfx::SurfaceFormat::B8G8R8X8);
+    MOZ_ASSERT(aSurface->GetFormat() == gfx::SurfaceFormat::OS_RGBX);
     if (aSurface->GetSize() == size &&
-        aSurface->GetFormat() == gfx::SurfaceFormat::B8G8R8X8) {
+        aSurface->GetFormat() == gfx::SurfaceFormat::OS_RGBX) {
       result = aSurface;
     }
   }
 
   if (!result) {
     result =
-        Factory::CreateDataSourceSurface(size, gfx::SurfaceFormat::B8G8R8X8);
+        Factory::CreateDataSourceSurface(size, gfx::SurfaceFormat::OS_RGBX);
   }
   if (NS_WARN_IF(!result)) {
     return nullptr;
@@ -320,7 +320,7 @@ already_AddRefed<DataSourceSurface> Data
   ycbcrData.mChromaSubsampling = aDescriptor.chromaSubsampling();
 
-  gfx::ConvertYCbCrToRGB(ycbcrData, gfx::SurfaceFormat::B8G8R8X8, size,
+  gfx::ConvertYCbCrToRGB(ycbcrData, gfx::SurfaceFormat::OS_RGBX, size,
                          map.mData, map.mStride);
 
   result->Unmap();
   return result.forget();
diff --git a/gfx/layers/composite/TextureHost.cpp b/gfx/layers/composite/TextureHost.cpp
--- a/gfx/layers/composite/TextureHost.cpp
+++ b/gfx/layers/composite/TextureHost.cpp
@@ -182,7 +182,7 @@ already_AddRefed<TextureHost> CreateDumm
   aFlags &= ~TextureFlags::DEALLOCATE_CLIENT;
   aFlags |= TextureFlags::DUMMY_TEXTURE;
   UniquePtr<TextureData> textureData(BufferTextureData::Create(
-      gfx::IntSize(1, 1), gfx::SurfaceFormat::B8G8R8A8, gfx::BackendType::SKIA,
+      gfx::IntSize(1, 1), gfx::SurfaceFormat::OS_RGBA, gfx::BackendType::SKIA,
       aBackend, aFlags, TextureAllocationFlags::ALLOC_DEFAULT, nullptr));
   SurfaceDescriptor surfDesc;
   textureData->Serialize(surfDesc);
diff --git a/gfx/layers/ipc/SharedSurfacesChild.cpp b/gfx/layers/ipc/SharedSurfacesChild.cpp
--- a/gfx/layers/ipc/SharedSurfacesChild.cpp
+++ b/gfx/layers/ipc/SharedSurfacesChild.cpp
@@ -241,7 +241,7 @@ nsresult SharedSurfacesChild::ShareInter
 
   SurfaceFormat format = aSurface->GetFormat();
   MOZ_RELEASE_ASSERT(
-      format == SurfaceFormat::B8G8R8X8 || format == SurfaceFormat::B8G8R8A8,
+      format == SurfaceFormat::OS_RGBX || format == SurfaceFormat::OS_RGBA,
       "bad format");
 
   data->MarkShared(manager->GetNextExternalImageId());
diff --git a/gfx/layers/wr/WebRenderLayerManager.cpp b/gfx/layers/wr/WebRenderLayerManager.cpp
--- a/gfx/layers/wr/WebRenderLayerManager.cpp
+++ b/gfx/layers/wr/WebRenderLayerManager.cpp
@@ -512,7 +512,7 @@ void WebRenderLayerManager::MakeSnapshot
 #ifdef MOZ_WIDGET_ANDROID
       SurfaceFormat::R8G8B8A8;
 #else
-      SurfaceFormat::B8G8R8A8;
+      SurfaceFormat::OS_RGBA;
 #endif
   RefPtr<TextureClient> texture = TextureClient::CreateForRawBufferAccess(
       WrBridge(), format, aSize.ToUnknownSize(), BackendType::SKIA,
diff --git a/gfx/layers/wr/WebRenderTextureHost.cpp b/gfx/layers/wr/WebRenderTextureHost.cpp
--- a/gfx/layers/wr/WebRenderTextureHost.cpp
+++ b/gfx/layers/wr/WebRenderTextureHost.cpp
@@ -159,7 +159,7 @@ int32_t WebRenderTextureHost::GetRGBStri
     // XXX this stride is used until yuv image rendering by webrender is used.
     // Software converted RGB buffers strides are aliened to 16
     return gfx::GetAlignedStride<16>(
-        GetSize().width, BytesPerPixel(gfx::SurfaceFormat::B8G8R8A8));
+        GetSize().width, BytesPerPixel(gfx::SurfaceFormat::OS_RGBA));
   }
   return ImageDataSerializer::ComputeRGBStride(format, GetSize().width);
 }
diff --git a/gfx/thebes/gfx2DGlue.h b/gfx/thebes/gfx2DGlue.h
--- a/gfx/thebes/gfx2DGlue.h
+++ b/gfx/thebes/gfx2DGlue.h
@@ -68,9 +68,9 @@ inline gfxRect ThebesRect(const RectDoub
 
 inline gfxImageFormat SurfaceFormatToImageFormat(SurfaceFormat aFormat) {
   switch (aFormat) {
-    case SurfaceFormat::B8G8R8A8:
+    case SurfaceFormat::OS_RGBA:
       return SurfaceFormat::A8R8G8B8_UINT32;
-    case SurfaceFormat::B8G8R8X8:
+    case SurfaceFormat::OS_RGBX:
       return SurfaceFormat::X8R8G8B8_UINT32;
     case SurfaceFormat::R5G6B5_UINT16:
       return SurfaceFormat::R5G6B5_UINT16;
@@ -84,16 +84,16 @@ inline gfxImageFormat SurfaceFormatToIma
 inline SurfaceFormat ImageFormatToSurfaceFormat(gfxImageFormat aFormat) {
   switch (aFormat) {
     case SurfaceFormat::A8R8G8B8_UINT32:
-      return SurfaceFormat::B8G8R8A8;
+      return SurfaceFormat::OS_RGBA;
     case SurfaceFormat::X8R8G8B8_UINT32:
-      return SurfaceFormat::B8G8R8X8;
+      return SurfaceFormat::OS_RGBX;
     case SurfaceFormat::R5G6B5_UINT16:
       return SurfaceFormat::R5G6B5_UINT16;
     case SurfaceFormat::A8:
       return SurfaceFormat::A8;
     default:
     case SurfaceFormat::UNKNOWN:
-      return SurfaceFormat::B8G8R8A8;
+      return SurfaceFormat::OS_RGBA;
   }
 }
 
@@ -102,9 +102,11 @@ inline gfxContentType ContentForFormat(c
     case SurfaceFormat::R5G6B5_UINT16:
     case SurfaceFormat::B8G8R8X8:
     case SurfaceFormat::R8G8B8X8:
+    case SurfaceFormat::X8R8G8B8:
       return gfxContentType::COLOR;
     case SurfaceFormat::A8:
       return gfxContentType::ALPHA;
+    case SurfaceFormat::A8R8G8B8:
     case SurfaceFormat::B8G8R8A8:
     case SurfaceFormat::R8G8B8A8:
     default:
diff --git a/gfx/thebes/gfxPlatform.cpp b/gfx/thebes/gfxPlatform.cpp
--- a/gfx/thebes/gfxPlatform.cpp
+++ b/gfx/thebes/gfxPlatform.cpp
@@ -987,7 +987,7 @@ void gfxPlatform::Init() {
 
   gPlatform->mScreenReferenceDrawTarget =
       gPlatform->CreateOffscreenContentDrawTarget(IntSize(1, 1),
-                                                  SurfaceFormat::B8G8R8A8);
+                                                  SurfaceFormat::OS_RGBA);
   if (!gPlatform->mScreenReferenceDrawTarget ||
       !gPlatform->mScreenReferenceDrawTarget->IsValid()) {
     // If TDR is detected, create a draw target with software backend
diff --git a/gfx/thebes/gfxPlatformWorker.cpp b/gfx/thebes/gfxPlatformWorker.cpp
--- a/gfx/thebes/gfxPlatformWorker.cpp
+++ b/gfx/thebes/gfxPlatformWorker.cpp
@@ -64,7 +64,7 @@ RefPtr<mozilla::gfx::DrawTarget>
 gfxPlatformWorker::ScreenReferenceDrawTarget() {
   if (!mScreenReferenceDrawTarget) {
     mScreenReferenceDrawTarget = Factory::CreateDrawTarget(
-        BackendType::SKIA, IntSize(1, 1), SurfaceFormat::B8G8R8A8);
+        BackendType::SKIA, IntSize(1, 1), SurfaceFormat::OS_RGBA);
   }
   return mScreenReferenceDrawTarget;
 }
diff --git a/gfx/thebes/gfxUtils.cpp b/gfx/thebes/gfxUtils.cpp
--- a/gfx/thebes/gfxUtils.cpp
+++ b/gfx/thebes/gfxUtils.cpp
@@ -1082,10 +1082,10 @@ nsresult gfxUtils::EncodeSourceSurfaceAs
   }
 
   RefPtr<DataSourceSurface> dataSurface;
-  if (aSurface->GetFormat() != SurfaceFormat::B8G8R8A8) {
+  if (aSurface->GetFormat() != SurfaceFormat::OS_RGBA) {
     // FIXME bug 995807 (B8G8R8X8), bug 831898 (R5G6B5)
     dataSurface = gfxUtils::CopySurfaceToDataSourceSurfaceWithFormat(
-        aSurface, SurfaceFormat::B8G8R8A8);
+        aSurface, SurfaceFormat::OS_RGBA);
   } else {
     dataSurface = aSurface->GetDataSurface();
   }
diff --git a/gfx/webrender_bindings/RenderCompositorSWGL.cpp b/gfx/webrender_bindings/RenderCompositorSWGL.cpp
--- a/gfx/webrender_bindings/RenderCompositorSWGL.cpp
+++ b/gfx/webrender_bindings/RenderCompositorSWGL.cpp
@@ -7,6 +7,7 @@
 #include "RenderCompositorSWGL.h"
 
 #include "mozilla/gfx/Logging.h"
+#include "mozilla/gfx/Swizzle.h"
 #include "mozilla/widget/CompositorWidget.h"
 
 #ifdef MOZ_WIDGET_GTK
@@ -92,8 +93,8 @@ bool RenderCompositorSWGL::AllocateMappe
   gfx::SurfaceFormat format = gfx::SurfaceFormat::UNKNOWN;
   if (bufferMode != layers::BufferMode::BUFFERED && !mSurface &&
       mDT->LockBits(&data, &size, &stride, &format) &&
-      (format != gfx::SurfaceFormat::B8G8R8A8 &&
-       format != gfx::SurfaceFormat::B8G8R8X8)) {
+      (format != gfx::SurfaceFormat::OS_RGBA &&
+       format != gfx::SurfaceFormat::OS_RGBX)) {
     // We tried to lock the DT and it succeeded, but the size or format
     // of the data is not compatible, so just release it and fall back below...
     mDT->ReleaseBits(data);
@@ -127,7 +128,7 @@ bool RenderCompositorSWGL::AllocateMappe
     size = bounds.Size().ToUnknownSize();
     if (!mSurface || mSurface->GetSize() != size) {
       mSurface = gfx::Factory::CreateDataSourceSurface(
-          size, gfx::SurfaceFormat::B8G8R8A8);
+          size, gfx::SurfaceFormat::OS_RGBA);
     }
     gfx::DataSourceSurface::MappedSurface map = {nullptr, 0};
     if (!mSurface || !mSurface->Map(gfx::DataSourceSurface::READ_WRITE, &map)) {
@@ -242,6 +243,12 @@ void RenderCompositorSWGL::CommitMappedB
   }
   mDT->Flush();
 
+#if MOZ_BIG_ENDIAN()
+  gfx::SwizzleData(mMappedData, mMappedStride, gfx::SurfaceFormat::B8G8R8A8,
+                   mMappedData, mMappedStride, gfx::SurfaceFormat::A8R8G8B8,
+                   mDT->GetSize());
+#endif
+
   // Done with the DT. Hand it back to the widget and clear out any trace of it.
   mWidget->EndRemoteDrawingInRegion(mDT, mDirtyRegion);
   mDirtyRegion.SetEmpty();
diff --git a/gfx/webrender_bindings/RenderTextureHostSWGL.cpp b/gfx/webrender_bindings/RenderTextureHostSWGL.cpp
--- a/gfx/webrender_bindings/RenderTextureHostSWGL.cpp
+++ b/gfx/webrender_bindings/RenderTextureHostSWGL.cpp
@@ -36,8 +36,8 @@ bool RenderTextureHostSWGL::UpdatePlanes
     }
     GLenum internalFormat = 0;
     switch (format) {
-      case gfx::SurfaceFormat::B8G8R8A8:
-      case gfx::SurfaceFormat::B8G8R8X8:
+      case gfx::SurfaceFormat::OS_RGBA:
+      case gfx::SurfaceFormat::OS_RGBX:
         MOZ_ASSERT(colorDepth == gfx::ColorDepth::COLOR_8);
         internalFormat = LOCAL_GL_RGBA8;
         break;
diff --git a/gfx/webrender_bindings/WebRenderTypes.h b/gfx/webrender_bindings/WebRenderTypes.h
--- a/gfx/webrender_bindings/WebRenderTypes.h
+++ b/gfx/webrender_bindings/WebRenderTypes.h
@@ -105,7 +105,7 @@ inline Maybe<wr::ImageFormat> SurfaceFor
 inline gfx::SurfaceFormat ImageFormatToSurfaceFormat(ImageFormat aFormat) {
   switch (aFormat) {
     case ImageFormat::BGRA8:
-      return gfx::SurfaceFormat::B8G8R8A8;
+      return gfx::SurfaceFormat::OS_RGBA;
     case ImageFormat::R8:
       return gfx::SurfaceFormat::A8;
     case ImageFormat::R16:
diff --git a/image/imgTools.cpp b/image/imgTools.cpp
--- a/image/imgTools.cpp
+++ b/image/imgTools.cpp
@@ -425,8 +425,8 @@ static nsresult EncodeImageData(DataSour
                                 const nsACString& aMimeType,
                                 const nsAString& aOutputOptions,
                                 nsIInputStream** aStream) {
-  MOZ_ASSERT(aDataSurface->GetFormat() == SurfaceFormat::B8G8R8A8 ||
-                 aDataSurface->GetFormat() == SurfaceFormat::B8G8R8X8,
+  MOZ_ASSERT(aDataSurface->GetFormat() == SurfaceFormat::OS_RGBA ||
+                 aDataSurface->GetFormat() == SurfaceFormat::OS_RGBX,
              "We're assuming B8G8R8A8/X8");
 
   // Get an image encoder for the media type
@@ -474,13 +474,13 @@ imgTools::EncodeImage(imgIContainer* aCo
 
   RefPtr<DataSourceSurface> dataSurface;
 
-  if (frame->GetFormat() == SurfaceFormat::B8G8R8A8 ||
-      frame->GetFormat() == SurfaceFormat::B8G8R8X8) {
+  if (frame->GetFormat() == SurfaceFormat::OS_RGBA ||
+      frame->GetFormat() == SurfaceFormat::OS_RGBX) {
     dataSurface = frame->GetDataSurface();
   } else {
     // Convert format to SurfaceFormat::B8G8R8A8
     dataSurface = gfxUtils::CopySurfaceToDataSourceSurfaceWithFormat(
-        frame, SurfaceFormat::B8G8R8A8);
+        frame, SurfaceFormat::OS_RGBA);
   }
 
   NS_ENSURE_TRUE(dataSurface, NS_ERROR_FAILURE);
@@ -522,8 +522,8 @@ imgTools::EncodeScaledImage(imgIContaine
 
   // If the given surface is the right size/format, we can encode it directly.
   if (scaledSize == frame->GetSize() &&
-      (frame->GetFormat() == SurfaceFormat::B8G8R8A8 ||
-       frame->GetFormat() == SurfaceFormat::B8G8R8X8)) {
+      (frame->GetFormat() == SurfaceFormat::OS_RGBA ||
+       frame->GetFormat() == SurfaceFormat::OS_RGBX)) {
     RefPtr<DataSourceSurface> dataSurface = frame->GetDataSurface();
     if (dataSurface) {
       return EncodeImageData(dataSurface, aMimeType, aOutputOptions, aStream);
@@ -533,7 +533,7 @@ imgTools::EncodeScaledImage(imgIContaine
   // Otherwise we need to scale it using a draw target.
   // Ensure the surface is initialized to clear in case we need to blend to it.
   RefPtr<DataSourceSurface> dataSurface = Factory::CreateDataSourceSurface(
-      scaledSize, SurfaceFormat::B8G8R8A8, true);
+      scaledSize, SurfaceFormat::OS_RGBA, true);
   if (NS_WARN_IF(!dataSurface)) {
     return NS_ERROR_FAILURE;
   }
@@ -545,7 +545,7 @@ imgTools::EncodeScaledImage(imgIContaine
 
   RefPtr<DrawTarget> dt = Factory::CreateDrawTargetForData(
       BackendType::SKIA, map.GetData(), dataSurface->GetSize(), map.GetStride(),
-      SurfaceFormat::B8G8R8A8);
+      SurfaceFormat::OS_RGBA);
   if (!dt) {
     gfxWarning() << "imgTools::EncodeImage failed in CreateDrawTargetForData";
     return NS_ERROR_OUT_OF_MEMORY;
@@ -603,7 +603,7 @@ imgTools::EncodeCroppedImage(imgIContain
                 frameHeight >= aOffsetY + aHeight);
 
   RefPtr<DataSourceSurface> dataSurface = Factory::CreateDataSourceSurface(
-      IntSize(aWidth, aHeight), SurfaceFormat::B8G8R8A8,
+      IntSize(aWidth, aHeight), SurfaceFormat::OS_RGBA,
       /* aZero = */ true);
   if (NS_WARN_IF(!dataSurface)) {
     return NS_ERROR_FAILURE;
@@ -616,7 +616,7 @@ imgTools::EncodeCroppedImage(imgIContain
 
   RefPtr<DrawTarget> dt = Factory::CreateDrawTargetForData(
       BackendType::SKIA, map.GetData(), dataSurface->GetSize(), map.GetStride(),
-      SurfaceFormat::B8G8R8A8);
+      SurfaceFormat::OS_RGBA);
   if (!dt) {
     gfxWarning()
         << "imgTools::EncodeCroppedImage failed in CreateDrawTargetForData";
diff --git a/layout/base/PresShell.cpp b/layout/base/PresShell.cpp
--- a/layout/base/PresShell.cpp
+++ b/layout/base/PresShell.cpp
@@ -5124,7 +5124,7 @@ already_AddRefed<SourceSurface> PresShel
 
   RefPtr<DrawTarget> dt =
       gfxPlatform::GetPlatform()->CreateOffscreenContentDrawTarget(
-          IntSize(pixelArea.width, pixelArea.height), SurfaceFormat::B8G8R8A8);
+          IntSize(pixelArea.width, pixelArea.height), SurfaceFormat::OS_RGBA);
   if (!dt || !dt->IsValid()) {
     return nullptr;
   }
diff --git a/layout/base/nsLayoutUtils.cpp b/layout/base/nsLayoutUtils.cpp
--- a/layout/base/nsLayoutUtils.cpp
+++ b/layout/base/nsLayoutUtils.cpp
@@ -7022,9 +7022,9 @@ SurfaceFromElementResult nsLayoutUtils::
     result.mAlphaType = gfxAlphaType::Opaque;
     RefPtr<DrawTarget> ref =
         aTarget ? aTarget : gfxPlatform::ThreadLocalScreenReferenceDrawTarget();
-    if (ref->CanCreateSimilarDrawTarget(size, SurfaceFormat::B8G8R8A8)) {
+    if (ref->CanCreateSimilarDrawTarget(size, SurfaceFormat::OS_RGBA)) {
       RefPtr<DrawTarget> dt =
-          ref->CreateSimilarDrawTarget(size, SurfaceFormat::B8G8R8A8);
+          ref->CreateSimilarDrawTarget(size, SurfaceFormat::OS_RGBA);
       if (dt) {
         result.mSourceSurface = dt->Snapshot();
       }
@@ -7102,12 +7102,12 @@ SurfaceFromElementResult nsLayoutUtils::
                                  : gfxPlatform::GetPlatform()
                                        ->ThreadLocalScreenReferenceDrawTarget();
     if (!ref->CanCreateSimilarDrawTarget(displaySize,
-                                         SurfaceFormat::B8G8R8A8)) {
+                                         SurfaceFormat::OS_RGBA)) {
       return result;
     }
 
     RefPtr<DrawTarget> dt =
-        ref->CreateSimilarDrawTarget(displaySize, SurfaceFormat::B8G8R8A8);
+        ref->CreateSimilarDrawTarget(displaySize, SurfaceFormat::OS_RGBA);
     if (!dt) {
       return result;
     }
@@ -7380,9 +7380,9 @@ SurfaceFromElementResult nsLayoutUtils::
     RefPtr<DrawTarget> ref =
         aTarget ? aTarget
                 : gfxPlatform::GetPlatform()->ScreenReferenceDrawTarget();
-    if (ref->CanCreateSimilarDrawTarget(size, SurfaceFormat::B8G8R8A8)) {
+    if (ref->CanCreateSimilarDrawTarget(size, SurfaceFormat::OS_RGBA)) {
       RefPtr<DrawTarget> dt =
-          ref->CreateSimilarDrawTarget(size, SurfaceFormat::B8G8R8A8);
+          ref->CreateSimilarDrawTarget(size, SurfaceFormat::OS_RGBA);
       if (dt) {
         result.mSourceSurface = dt->Snapshot();
       }
diff --git a/layout/painting/nsCSSRenderingGradients.cpp b/layout/painting/nsCSSRenderingGradients.cpp
--- a/layout/painting/nsCSSRenderingGradients.cpp
+++ b/layout/painting/nsCSSRenderingGradients.cpp
@@ -1160,7 +1160,7 @@ bool nsCSSGradientRenderer::TryPaintTile
   {
     RefPtr<gfx::DrawTarget> tileTarget =
         aContext.GetDrawTarget()->CreateSimilarDrawTarget(
-            tileSize, gfx::SurfaceFormat::B8G8R8A8);
+            tileSize, gfx::SurfaceFormat::OS_RGBA);
     if (!tileTarget || !tileTarget->IsValid()) {
       return false;
     }
diff --git a/layout/painting/nsDisplayList.cpp b/layout/painting/nsDisplayList.cpp
--- a/layout/painting/nsDisplayList.cpp
+++ b/layout/painting/nsDisplayList.cpp
@@ -5043,7 +5043,7 @@ void nsDisplayBlendMode::Paint(nsDisplay
   // we're going to draw to. This will include the same transform as
   // is currently on |dt|.
   RefPtr<DrawTarget> temp =
-      dt->CreateClippedDrawTarget(rect, SurfaceFormat::B8G8R8A8);
+      dt->CreateClippedDrawTarget(rect, SurfaceFormat::OS_RGBA);
   if (!temp) {
     return;
   }
@@ -6870,7 +6870,7 @@ void nsDisplayTransform::Paint(nsDisplay
   RefPtr<DrawTarget> untransformedDT =
       gfxPlatform::GetPlatform()->CreateOffscreenContentDrawTarget(
           IntSize(pixelBounds.Width(), pixelBounds.Height()),
-          SurfaceFormat::B8G8R8A8, true);
+          SurfaceFormat::OS_RGBA, true);
   if (!untransformedDT || !untransformedDT->IsValid()) {
     return;
   }
diff --git a/layout/painting/nsImageRenderer.cpp b/layout/painting/nsImageRenderer.cpp
--- a/layout/painting/nsImageRenderer.cpp
+++ b/layout/painting/nsImageRenderer.cpp
@@ -469,7 +469,7 @@ ImgDrawResult nsImageRenderer::Draw(nsPr
       return ImgDrawResult::SUCCESS;
     }
     RefPtr<DrawTarget> tempDT = ctx->GetDrawTarget()->CreateSimilarDrawTarget(
-        tmpDTRect.Size(), SurfaceFormat::B8G8R8A8);
+        tmpDTRect.Size(), SurfaceFormat::OS_RGBA);
     if (!tempDT || !tempDT->IsValid()) {
       gfxDevCrash(LogReason::InvalidContext)
           << "ImageRenderer::Draw problem " << gfx::hexa(tempDT);
diff --git a/layout/svg/FilterInstance.cpp b/layout/svg/FilterInstance.cpp
--- a/layout/svg/FilterInstance.cpp
+++ b/layout/svg/FilterInstance.cpp
@@ -666,7 +666,7 @@ void FilterInstance::BuildSourcePaint(So
 
   RefPtr<DrawTarget> offscreenDT =
       gfxPlatform::GetPlatform()->CreateOffscreenContentDrawTarget(
-          neededRect.Size(), SurfaceFormat::B8G8R8A8);
+          neededRect.Size(), SurfaceFormat::OS_RGBA);
   if (!offscreenDT || !offscreenDT->IsValid()) {
     return;
   }
@@ -714,7 +714,7 @@ void FilterInstance::BuildSourceImage(Dr
   }
 
   RefPtr<DrawTarget> offscreenDT;
-  SurfaceFormat format = SurfaceFormat::B8G8R8A8;
+  SurfaceFormat format = SurfaceFormat::OS_RGBA;
   if (aDest->CanCreateSimilarDrawTarget(neededRect.Size(), format)) {
     offscreenDT = aDest->CreateSimilarDrawTargetForFilter(
         neededRect.Size(), format, aFilter, aSource, aSourceRect, Point(0, 0));
diff --git a/layout/svg/SVGMaskFrame.cpp b/layout/svg/SVGMaskFrame.cpp
--- a/layout/svg/SVGMaskFrame.cpp
+++ b/layout/svg/SVGMaskFrame.cpp
@@ -68,7 +68,7 @@ already_AddRefed<SourceSurface> SVGMaskF
   RefPtr<DrawTarget> maskDT;
   if (maskType == StyleMaskType::Luminance) {
     maskDT = aParams.dt->CreateClippedDrawTarget(maskSurfaceRect,
-                                                 SurfaceFormat::B8G8R8A8);
+                                                 SurfaceFormat::OS_RGBA);
   } else {
     maskDT =
         aParams.dt->CreateClippedDrawTarget(maskSurfaceRect, SurfaceFormat::A8);
diff --git a/layout/svg/SVGPatternFrame.cpp b/layout/svg/SVGPatternFrame.cpp
--- a/layout/svg/SVGPatternFrame.cpp
+++ b/layout/svg/SVGPatternFrame.cpp
@@ -370,7 +370,7 @@ already_AddRefed<SourceSurface> SVGPatte
   }
 
   RefPtr<DrawTarget> dt = aDrawTarget->CreateSimilarDrawTargetWithBacking(
-      surfaceSize, SurfaceFormat::B8G8R8A8);
+      surfaceSize, SurfaceFormat::OS_RGBA);
   if (!dt || !dt->IsValid()) {
     return nullptr;
   }
diff --git a/layout/svg/SVGUtils.cpp b/layout/svg/SVGUtils.cpp
--- a/layout/svg/SVGUtils.cpp
+++ b/layout/svg/SVGUtils.cpp
@@ -488,7 +488,7 @@ class MixModeBlender {
 
     RefPtr<DrawTarget> targetDT =
         mSourceCtx->GetDrawTarget()->CreateSimilarDrawTarget(
-            drawRect.Size(), SurfaceFormat::B8G8R8A8);
+            drawRect.Size(), SurfaceFormat::OS_RGBA);
     if (!targetDT || !targetDT->IsValid()) {
       return nullptr;
     }
diff --git a/widget/gtk/DMABufSurface.cpp b/widget/gtk/DMABufSurface.cpp
--- a/widget/gtk/DMABufSurface.cpp
+++ b/widget/gtk/DMABufSurface.cpp
@@ -948,8 +948,8 @@ bool DMABufSurfaceRGBA::HasAlpha() {
 }
 
 gfx::SurfaceFormat DMABufSurfaceRGBA::GetFormat() {
-  return HasAlpha() ? gfx::SurfaceFormat::B8G8R8A8
-                    : gfx::SurfaceFormat::B8G8R8X8;
+  return HasAlpha() ? gfx::SurfaceFormat::OS_RGBA
+                    : gfx::SurfaceFormat::OS_RGBX;
 }
 
 // GL uses swapped R and B components so report accordingly.
@@ -1662,7 +1662,7 @@ DMABufSurfaceYUV::GetAsSourceSurface() {
   LOGDMABUF(("DMABufSurfaceYUV::GetAsSourceSurface UID %d", mUID));
 
   gfx::IntSize size(GetWidth(), GetHeight());
-  const auto format = gfx::SurfaceFormat::B8G8R8A8;
+  const auto format = gfx::SurfaceFormat::OS_RGBA;
   RefPtr<gfx::DataSourceSurface> source =
       gfx::Factory::CreateDataSourceSurface(size, format);
   if (NS_WARN_IF(!source)) {
@@ -1692,7 +1692,7 @@ nsresult DMABufSurfaceYUV::BuildSurfaceD
   LOGDMABUF(("DMABufSurfaceYUV::BuildSurfaceDescriptorBuffer UID %d", mUID));
 
   gfx::IntSize size(GetWidth(), GetHeight());
-  const auto format = gfx::SurfaceFormat::B8G8R8A8;
+  const auto format = gfx::SurfaceFormat::OS_RGBA;
 
   uint8_t* buffer = nullptr;
   int32_t stride = 0;
diff --git a/widget/gtk/WindowSurfaceX11.cpp b/widget/gtk/WindowSurfaceX11.cpp
--- a/widget/gtk/WindowSurfaceX11.cpp
+++ b/widget/gtk/WindowSurfaceX11.cpp
@@ -24,13 +24,13 @@ gfx::SurfaceFormat WindowSurfaceX11::Get
     case 32:
       if (aVisual->red_mask == 0xff0000 && aVisual->green_mask == 0xff00 &&
           aVisual->blue_mask == 0xff) {
-        return gfx::SurfaceFormat::B8G8R8A8;
+        return gfx::SurfaceFormat::OS_RGBA;
       }
       break;
     case 24:
       if (aVisual->red_mask == 0xff0000 && aVisual->green_mask == 0xff00 &&
           aVisual->blue_mask == 0xff) {
-        return gfx::SurfaceFormat::B8G8R8X8;
+        return gfx::SurfaceFormat::OS_RGBX;
       }
       break;
     case 16:
