# Contributor: Marek Benc <dusxmt@gmx.com>
# Maintainer: Marek Benc <dusxmt@gmx.com>
pkgname=mksh
pkgver=59c
pkgrel=0
pkgdesc="MirBSD Korn Shell, a free Korn Shell interpreter"
url="http://mirbsd.de/mksh"
arch="all"
license="MirOS AND Unicode-DFS-2016"
depends=""
checkdepends="ed perl"
makedepends=""
subpackages="$pkgname-doc"
install="mksh.post-install mksh.post-upgrade mksh.pre-deinstall"
source="https://www.mirbsd.org/MirOS/dist/mir/${pkgname}/${pkgname}-R${pkgver}.tgz
	fix-deprecated-fgrep.patch
	"
builddir="$srcdir"/$pkgname

build() {
	# Build the main shell:
	/bin/sh ./Build.sh -r
	mv test.sh test_mksh.sh

	# Build the compatibility/legacy shell:
	CFLAGS="$CFLAGS -DMKSH_BINSHPOSIX -DMKSH_BINSHREDUCED" \
		/bin/sh ./Build.sh -r -L
	mv test.sh test_lksh.sh

	# Build the HTML FAQ:
	/bin/sh ./FAQ2HTML.sh
}

check() {
	msg "Running the test suite for mksh"
	./test_mksh.sh

	msg "Running the test suite for lksh:"
	./test_lksh.sh
}

package() {
	mkdir -p "$pkgdir"/bin
	install -m 755 mksh "$pkgdir"/bin
	install -m 755 lksh "$pkgdir"/bin

	mkdir -p "$pkgdir"/usr/share/man/man1/
	install -m 644 mksh.1 "$pkgdir"/usr/share/man/man1/
	install -m 644 lksh.1 "$pkgdir"/usr/share/man/man1/

	mkdir -p "$pkgdir"/usr/share/doc/mksh/examples/
	install -m 644 dot.mkshrc "$pkgdir"/usr/share/doc/mksh/examples/
	install -m 644 FAQ.htm    "$pkgdir"/usr/share/doc/mksh/
}

sha512sums="f56b6956f9e1dd88ddce2294301a5eb698050d9d4f49286fdcd8f9df8554eabbcc71d37e2bf3eb7234e3968a17231cc6de8aa7efbf17768834a90b14e8cdf340  mksh-R59c.tgz
46210a286add06ac46adbc57c2150eb98ae67af7f79c509e0d11443cb33ce6617c1fa587179bb1955a832549515e8b1def1e32784caf91b54f7d5cfee1b43970  fix-deprecated-fgrep.patch"
