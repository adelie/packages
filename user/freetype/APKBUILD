# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=freetype
pkgver=2.13.2
pkgrel=0
pkgdesc="TrueType font rendering library"
url="https://www.freetype.org/"
arch="all"
license="FTL OR GPL-2.0+"
options="!check"
depends=""
makedepends="zlib-dev libpng-dev bzip2-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="http://download.savannah.gnu.org/releases/freetype/freetype-$pkgver.tar.xz"

# secfixes:
#   2.13.2:
#     - CVE-2022-27406
#     - CVE-2022-27405
#     - CVE-2022-27404
#   2.10.4-r0:
#     - CVE-2020-15999
#   2.9.1-r0:
#     - CVE-2018-6942
#   2.7.1-r1:
#     - CVE-2017-8105
#     - CVE-2017-8287

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--disable-static \
		--with-bzip2 \
		--with-png \
		--enable-freetype-config
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="a5917edaa45cb9f75786f8a4f9d12fdf07529247e09dfdb6c0cf7feb08f7588bb24f7b5b11425fb47f8fd62fcb426e731c944658f6d5a59ce4458ad5b0a50194  freetype-2.13.2.tar.xz"
