# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libkomparediff2
pkgver=22.04.2
pkgrel=0
pkgdesc="KDE diff library"
url="https://kde.org/applications/development/org.kde.kompare"
arch="all"
license="GPL-2.0+ AND GPL-2.0-only AND LGPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev cmake extra-cmake-modules kauth-dev kcodecs-dev
	kcompletion-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev ki18n-dev
	kio-dev kitemviews-dev kjobwidgets-dev kservice-dev kwidgetsaddons-dev
	kwindowsystem-dev kxmlgui-dev solid-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkomparediff2-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="fb8e0e10c0e77f9e547f6af610c3add9dca51ce55f055f4501ddc0a60288dc9a623314659016f32370c2d2aea41f26664a7ccec4dc50c45ca20c68d549d50d96  libkomparediff2-22.04.2.tar.xz"
