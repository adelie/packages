# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kleopatra
pkgver=22.04.2
pkgrel=0
pkgdesc="Certificate manager and cryptography GUI"
url="https://www.kde.org/applications/utilities/kleopatra/"
arch="all"
options="!check"  # Test requires D-Bus session bus.
license="GPL-2.0-only"
depends="shared-mime-info"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev libkleo-dev kcmutils-dev
	libassuan-dev kitemmodels-dev kmime-dev knotifications-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kleopatra-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="5f1f59a42b568d22713a8f545994bfc4fcba15ab561ad5be8c05e918cd4ab0862ffaef3c6ca1c3fe5dd386ae50bc20b90747157b46d9dacbb7eb4cd7f2ca1705  kleopatra-22.04.2.tar.xz"
