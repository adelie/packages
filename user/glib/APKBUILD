# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=glib
pkgver=2.72.4
pkgrel=0
pkgdesc="Common C routines used by Gtk+ and other libs"
url="https://developer.gnome.org/glib/"
arch="all"
options="!check" # Now requires D-Bus running.
license="LGPL-2.1+"
depends="dbus"
depends_dev="perl python3 attr-dev bzip2-dev libffi-dev util-linux-dev"
checkdepends="tzdata shared-mime-info"
makedepends="$depends_dev dbus-dev meson ninja pcre-dev xmlto zlib-dev"
triggers="$pkgname.trigger=/usr/share/glib-2.0/schemas:/usr/lib/gio/modules"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-static $pkgname-dev
	$pkgname-lang $pkgname-bash-completion:bashcomp:noarch"
source="https://download.gnome.org/sources/$pkgname/${pkgver%.*}/$pkgname-$pkgver.tar.xz
	0001-gquark-fix-initialization-with-c-constructors.patch
	broken-gio-tests.patch
	i386-fpu-test.patch
	musl-no-locale.patch
	ridiculous-strerror-nonconformance.patch
	meson-sucks-and-i-hate-you-so-much-right-now.patch
	"

# secfixes:
#   2.60.4-r0:
#     - CVE-2019-12450

build() {
	meson --default-library=both \
		-Dprefix=/usr \
		-Dselinux=disabled \
		-Dman=true build
	ninja -C build
}

check() {
	# workaround if a user builds it on a computer running X11
	DISPLAY= ninja -C build test
}

package() {
	DESTDIR="$pkgdir" ninja -C build install
	rm -rf "$pkgdir"/usr/lib/charset.alias
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/bin "$subpkgdir"/usr/share
		find "$pkgdir"/usr/bin ! -name "glib-compile-schemas" -a \( \
		-name "gdbus-codegen" -o \
		-name "gobject-query" -o \
		-name "gresource" -o \
		-name "gtester*" -o \
		-name "glib-*" \) \
		-exec mv {} "$subpkgdir"/usr/bin \;
	mv "$pkgdir"/usr/share/gdb "$pkgdir"/usr/share/glib-2.0 \
		"$subpkgdir"/usr/share
}

static() {
	pkgdesc="$pkgdesc (static libraries)"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir"/usr/lib/
}

bashcomp() {
	pkgdesc="Bash completion for $pkgname"
	depends=
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"
	mkdir -p "$subpkgdir"/usr/share
	mv "$pkgdir"/usr/share/bash-completion "$subpkgdir"/usr/share
	[ "$(ls -A "$pkgdir"/usr/share)" ] || rmdir "$pkgdir"/usr/share
}

sha512sums="b4e2e0985e2184ee9656c4f1b4e15d8d1264f3d23d31349bc43d92e8432cffa48e1685c40517efb08dc5b57b8285acf65f2747deeb50e50d9cacec7160e7edf8  glib-2.72.4.tar.xz
32e5aca9a315fb985fafa0b4355e4498c1f877fc1f0b58ad4ac261fb9fbced9f026c7756a5f2af7d61ce756b55c8cd02811bb08df397040e93510056f073756b  0001-gquark-fix-initialization-with-c-constructors.patch
48d367d21a740161431c6ea2e062415e403d1a7af40515c71c60f11a8784d8725595386dc5a801c351540d67dee4186fb4af875f0165d21d13fe1a5167e02099  broken-gio-tests.patch
aa7444bbdf7b88798adc67c15cdb8b7459450c0b7357caea16b74462c5c9179ba80d4018b1e656e90a5e3be5b2e3c14e9b8c0ccbb2ee4d8c92dc8fa627518b84  i386-fpu-test.patch
db9de1fbf953afe76df9047d878c405ceeb6bd674c4382a8437612d788ff1c65c78012831888b2c4916f8125dab6bda50f1bba040744563dc8dcad30aeb1dd4d  musl-no-locale.patch
56c10a0f64cbd8ce584d428f818e7e678fdeb40a32df792843208ddfa3135d362cc2077bc9fe3bfebe13ee6af0ecf6403a593ad727e0a92276074a17a9c7029c  ridiculous-strerror-nonconformance.patch
dc1f0232433f6fb2cb2edebc8f5e15b4dcca557191af0a0ca45ff5cc5d109362e1dde4a532b08423e74c6b716801a516ad4ea105435e1332cd1a42f8131df46b  meson-sucks-and-i-hate-you-so-much-right-now.patch"
