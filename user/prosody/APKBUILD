# Contributor: Mika Havela <mika.havela@gmail.com>
# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=prosody
pkgver=0.11.13
pkgrel=0
pkgdesc="Lua based Jabber/XMPP server"
url="http://prosody.im/"
arch="all"
options="!check" # broken testsuite
license="MIT"
depends="lua-socket lua-expat lua-filesystem lua-sec lua5.3"
makedepends="linux-headers lua5.3-dev libidn-dev openssl-dev"
install="prosody.pre-install"
subpackages="$pkgname-doc $pkgname-openrc"
pkgusers="prosody"
pkggroups="prosody"
source="https://prosody.im/downloads/source/$pkgname-$pkgver.tar.gz
	prosody.cfg.lua.patch
	$pkgname.initd
	"

# secfixes:
#   0.11.13-r0:
#     - CVE-2021-32917
#     - CVE-2021-32919
#     - CVE-2021-32920
#     - CVE-2021-37601

build() {
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc/prosody \
		--ostype=linux \
		--with-lua-lib=/usr/lib \
		--with-lua-include=/usr/include \
		--lua-version=5.3 \
		--no-example-certs
	# Don't generate certs
	rm -f "$builddir"/certs/Makefile

	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install

	install -d -o prosody -g prosody "$pkgdir/var/log/prosody"
	install -d -o prosody -g prosody "$pkgdir/var/run/prosody"
	install -d -m750 -o prosody -g prosody "$pkgdir/var/lib/prosody"

	install -D -m755 "$srcdir"/"$pkgname".initd "$pkgdir"/etc/init.d/"$pkgname"
}

sha512sums="7616785536b7b51767a26963a80c961ef2403609e1e78dda3f88cf68c00d5bb899278c70a22b006b58c36c62cbb1bbd390a9298bad5b8d6524928a1cd5457813  prosody-0.11.13.tar.gz
a6ca168fe3d11ee3b05295fb36dfaf8240c60a85507032b2502f9a97d3fd055f7eee38ba6efbb8f79472fc7cdd3556922194d0bd7099f7fb809be01890acc511  prosody.cfg.lua.patch
11b0f5e4fa488e047c26aa5e51c35983100cdbf7ebbf7c8b6d003c8db7f52e797f93e4744d54b3094c82d722d5e4de62b5734376cb5e69a4c6127f8cb07a4347  prosody.initd"
