# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=kde-cli-tools
pkgver=5.24.5
pkgrel=0
pkgdesc="KDE command-like utilities"
url="https://www.kde.org/"
arch="all"
options="!check"  # MIME types for some reason think .doc == .txt
license="(GPL-2.0-only OR GPL-3.0-only) AND GPL-2.0+ AND GPL-2.0-only AND LGPL-2.1-only"
depends=""
checkdepends="shared-mime-info"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev
	qt5-qtx11extras-dev kactivities-dev kcmutils-dev kconfig-dev
	kdeclarative-dev kdesu-dev kdoctools-dev ki18n-dev kiconthemes-dev
	kinit-dev kio-dev kservice-dev kwindowsystem-dev libkworkspace-dev"
subpackages="$pkgname-lang $pkgname-doc"
source="https://download.kde.org/stable/plasma/$pkgver/kde-cli-tools-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="db89474ea3c88978123012430249677405b245628b0b12fda917b70fca9ea36e060f351420d0dd94f64e3a479a04a768064df27830884e2a8883db9104b90844  kde-cli-tools-5.24.5.tar.xz"
