# Contributor: Daniel Sabogal <dsabogalcc@gmail.com>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=numactl
pkgver=2.0.15
pkgrel=0
pkgdesc="Simple NUMA policy support"
url="https://github.com/numactl/numactl"
# ARM lacks the __NR_migrate_pages syscall
arch="all !armhf !armv7"
license="GPL-2.0+ AND LGPL-2.1"
depends=""
makedepends="autoconf automake libtool linux-headers"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools"
source="$pkgname-$pkgver.tar.gz::https://github.com/numactl/$pkgname/archive/v$pkgver.tar.gz
	version.patch
	"

prepare() {
	default_prepare
	./autogen.sh
}

build() {
	./configure \
		--prefix=/usr \
		--mandir=/usr/share/man
	make
}

check() {
	make check VERBOSE=1 TESTS='test/distance test/nodemap test/tbitmap'
}

package() {
	make DESTDIR="$pkgdir" install

	# provided by linux man-pages
	rm -r "$pkgdir"/usr/share/man/man2
}

tools() {
	pkgdesc="NUMA policy control tools"

	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr
}

sha512sums="dcc76b8d36698a740e04206471352013b39bd61357d6090fc062681fa2f31e5f0b40c8582c06fd1a97919f494002bef1fa8330b6a6784b516c39961caa2a4320  numactl-2.0.15.tar.gz
965d8b4020578a6fd1d4ff0d85d6fd941534c3b26446e10529dc9c78ab331ce9dbc129e01e34282bbf615c9b3a606434079cc830bc4024b04f6273e676f2ade7  version.patch"
