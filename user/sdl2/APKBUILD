# Contributor: August Klein <amatcoder@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sdl2
pkgver=2.0.22
pkgrel=0
pkgdesc="Low level audio, keyboard, mouse, joystick and graphics library"
url="https://www.libsdl.org/"
arch="all"
options="!check"  # No test suite.
license="zlib"
depends=""
makedepends="alsa-lib-dev libx11-dev libxcursor-dev libxext-dev libxi-dev
	libxkbcommon-dev libxrandr-dev libxrender-dev libusb-dev mesa-dev
	libxscrnsaver-dev dbus-dev eudev-dev pulseaudio-dev libglvnd-dev cmake
	libxinerama-dev wayland-dev wayland-protocols"
subpackages="$pkgname-dev"
source="https://www.libsdl.org/release/SDL2-$pkgver.tar.gz
	sdl2-soname.patch
	"
builddir="$srcdir/SDL2-$pkgver"

build() {
	# pmOS needs DirectFB for battery animations and LUKS unlocking
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="ca4b690433cd4d9d73b797da98666317128e7e817ab60e874a49d94791ea41e8a6b4fc43649593120daa0702190c0f8a6ed326c908d87375c8da9f369d994f6a  SDL2-2.0.22.tar.gz
81b5807cd9f2683231c14562e961467fcd7ba257140d1a266932d6a6a5103633e3af896c09b261620b2934d521809e08ed79d3287c0897f4783c6afc1c01a566  sdl2-soname.patch"
