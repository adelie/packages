# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Nathan Angelacos <nangel@alpinelinux.org>
# Contributor: TBK <alpine@jjtc.eu>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Lee Starnes <lee@canned-death.us>
pkgname=rspamd
pkgver=3.8.4
pkgrel=1
pkgdesc="Fast, free and open-source spam filtering system"
url="https://rspamd.com/"
arch="all"
license="Apache-2.0 AND BSD-1-Clause AND BSD-2-Clause AND BSD-3-Clause AND CC0-1.0 AND LGPL-2.1+ AND LGPL-3.0-only AND MIT AND Zlib"
pkgusers="rspamd"
pkggroups="rspamd"
depends=""
checkdepends="luarocks"
makedepends="
	cmake
	curl-dev
	file-dev
	glib-dev
	icu-dev
	libevent-dev
	libexecinfo-dev
	libgd-dev
	libsodium-dev
	lua5.3
	lua5.3-dev
	openssl-dev
	pcre2-dev
	perl
	ragel
	sqlite-dev
	zstd-dev
	"
install="$pkgname.pre-install"
subpackages="
	$pkgname-doc
	$pkgname-client
	$pkgname-libs
	$pkgname-utils::noarch
	$pkgname-controller::noarch
	$pkgname-fuzzy::noarch
	$pkgname-proxy::noarch
	$pkgname-openrc
	$pkgname-dbg
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/rspamd/rspamd/archive/$pkgver.tar.gz
	$pkgname.logrotated
	$pkgname.initd
	$pkgname.confd
	10-conf-split-workers.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	LDFLAGS="-lexecinfo" cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCONFDIR=/etc/$pkgname \
		-DRUNDIR=/run/$pkgname \
		-DRSPAMD_USER=$pkgusers \
		-DRSPAMD_GROUP=$pkggroups \
		-DENABLE_LUAJIT=OFF \
		-DLUA_INCLUDE_DIR=/usr/include/lua5.3 \
		-DLUA_LIBRARY=/usr/lib/lua5.3/liblua.so \
		-DENABLE_REDIRECTOR=ON \
		-DENABLE_URL_INCLUDE=ON \
		-DENABLE_BACKWARD=OFF \
		-DENABLE_PCRE2=ON \
		-DSYSTEM_ZSTD=ON \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		"$CMAKE_CROSSOPTS" .
	make -C build
}

check() {
	make -C build rspamd-test
}

package() {
	make -C build DESTDIR="$pkgdir" install

	cd "$pkgdir"
	local path=usr/bin
	for b in rspamd rspamc rspamadm;
	do
		rm "$path"/$b
		mv "$path"/$b-$pkgver "$path"/$b
	done

	mv "$path"/rspamd_stats "$path"/rspamd-stats
	mkdir -p ./usr/sbin
	mv usr/bin/rspamd usr/sbin/

	mkdir -p usr/share/doc/$pkgname
	mv usr/share/$pkgname/www/README.md \
		usr/share/doc/$pkgname/

	install -Dm 644 "$srcdir"/$pkgname.logrotated etc/logrotate.d/$pkgname
	install -Dm 755 "$srcdir"/$pkgname.initd etc/init.d/$pkgname
	install -Dm 644 "$srcdir"/$pkgname.confd etc/conf.d/$pkgname

	mkdir -p etc/$pkgname/local.d \
		etc/$pkgname/override.d

	install -dm 750 -o rspamd -g rspamd \
		var/lib/$pkgname
	install -dm 750 -o rspamd -g rspamd \
		var/log/$pkgname
	install -dm 755 -o rspamd -g rspamd \
		etc/$pkgname/local.d/maps.d
}

client() {
	pkgdesc="$pkgdesc (console client)"

	mkdir -p "$subpkgdir/usr/bin"
	mv "$pkgdir/usr/bin/rspamc" "$subpkgdir/usr/bin/rspamc"
}

libs() {
	pkgdesc="$pkgdesc (libraries)"

	mkdir -p "$subpkgdir/usr/lib/$subpkgname"
	mv "$pkgdir/usr/lib/$pkgname/"*.so "$subpkgdir/usr/lib/$subpkgname/"
}

utils() {
	pkgdesc="$pkgdesc (utilities)"
	depends="perl"

	mkdir -p "$subpkgdir/usr/bin"
	mv "$pkgdir/usr/bin/${pkgname}-stats" "$subpkgdir/usr/bin/"
}

fuzzy() {
	pkgdesc="$pkgdesc (local fuzzy storage)"
	license="Apache-2.0"
	depends="$pkgname"

	mkdir -p "$subpkgdir/etc/$pkgname/modules.d"
	mv "$pkgdir/etc/$pkgname"/worker-fuzzy.* "$subpkgdir/etc/$pkgname/"
	mv "$pkgdir/etc/$pkgname"/modules.d/fuzzy_* "$subpkgdir/etc/$pkgname/modules.d/"
}

controller() {
	pkgdesc="$pkgdesc (controller web interface)"
	license="MIT"
	depends="$pkgname"

	mkdir -p "$subpkgdir/usr/share/$pkgname"
	mv "$pkgdir/usr/share/$pkgname/www" "$subpkgdir/usr/share/$pkgname/www"
	mkdir -p "$subpkgdir/etc/$pkgname"
	mv "$pkgdir/etc/$pkgname"/worker-controller.* "$subpkgdir/etc/$pkgname/"
}

proxy() {
	pkgdesc="$pkgdesc (milter support)"
	license="Apache-2.0"
	depends="$pkgname"

	mkdir -p "$subpkgdir/etc/$pkgname"
	mv "$pkgdir/etc/$pkgname"/worker-proxy.* "$subpkgdir/etc/$pkgname/"
}

sha512sums="dda099dd9a17699d143fc8018b89fbecfa9659eccc26b5fb88bc3f40c47e935993dfc81f1f93bb69880d17af40870e6ea20edbeed7e3f4c12c278f5c12b56a51  rspamd-3.8.4.tar.gz
2efe28575c40d1fba84b189bb872860e744400db80dce2f6330be6c6287fb3f46e6511284729b957488bf40bcb9b0952e26df9934f5f138334bd2766075c45cb  rspamd.logrotated
782e1126d32e450a1db0ac822c127b9a763f903093f200bdf603a6a0610a853671b94c89b0bb2d8ebdfb065e0cf62be51c1c7f451e8da34e25f252a276c2b0f3  rspamd.initd
a2003ef0c9d64a44480f59302864a2dfedcbe3a0047fcbb655408bc8aae9014b6ad0ddc6b64d4abeeb21bea0f86678afd30589ac8eed83e07ad7f87710e93702  rspamd.confd
0321c76b42131943f7b53efeb6bbd1c5b732fdec4f796838568af45d245066518f8b2ccd667d5a370df539ba73dda47e66d4ce0eeb211ef6fc5942e96c2e311b  10-conf-split-workers.patch"
