# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=lynx
pkgver=2.9.2
pkgrel=0
pkgdesc="Cross-platform text-based browser"
url="https://lynx.invisible-island.net/"
arch="all"
license="GPL-2.0-only"
depends="gzip"
makedepends="glib-dev ncurses-dev openssl-dev perl utmps-dev zlib-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://invisible-island.net/archives/lynx/tarballs/${pkgname}${pkgver}.tar.bz2"
builddir="$srcdir/${pkgname}${pkgver}"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-ipv6 \
		--enable-nls \
		--with-screen=ncursesw \
		--with-ssl
	make helpdir=/usr/share/doc/lynx/help \
		docdir=/usr/share/doc/lynx
}

check() {
	./lynx -version
}

package() {
	make DESTDIR="$pkgdir" install install-help install-doc \
		helpdir=/usr/share/doc/lynx/help \
		docdir=/usr/share/doc/lynx
}

sha512sums="bb9ed23a5f8664ca8bccd95cc55683849aa707f601e209d84e0acaed151b78cd772316f527881022ff120049c4fc586c05d579f9d616a2b108d9ddfaa5b2159d  lynx2.9.2.tar.bz2"
