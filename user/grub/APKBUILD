# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=grub
pkgver=2.12
_pkgver=$(printf '%s' "$pkgver" | tr _ \~)
pkgrel=0
pkgdesc="Boot loader with support for Linux, Multiboot and more"
url="https://www.gnu.org/software/grub/"
arch="all"
# strip handled by grub Makefiles, abuild strip breaks xen pv-grub
options="!check !strip"  # Cannot test boot loader.
license="GPL-3.0+"
depends=""
makedepends="bison flex freetype-dev fuse-dev linux-headers lvm2-dev python3
	unifont xz"
# [22:02] <@awilfox> [[sroracle]]: grub breaks without its locale files present
# [22:02] <@awilfox> it cannot be split
subpackages="$pkgname-dev $pkgname-doc $pkgname-mount"

flavors=""
case "$CARCH" in
x86|pmmx)	flavors="efi bios";;
x86_64)		flavors="efi bios xenhost";;
aarch64)	flavors="efi";;
arm*)		flavors="efi uboot";;
ppc*)		flavors="ieee1275"; makedepends="$makedepends powerpc-utils" ;;
sparc*)		flavors="ieee1275";;
mips*)		flavors="arc";;
riscv*)		flavors="efi";;
esac
for f in $flavors; do
	subpackages="$subpackages $pkgname-$f"
done

install="$pkgname.post-upgrade"

source="https://ftp.gnu.org/gnu/$pkgname/$pkgname-$_pkgver.tar.xz
	adelie-branding.patch
	grub2-accept-empty-module.patch
	0002-revert-gawk.patch
	0010-fix-gcc-no-pie-specs.patch
	xfs.patch
	grub-xen-host_grub.cfg
	default-grub
	update-grub
	quirk-01_radeon_agpmode
	"
builddir="$srcdir/$pkgname-$_pkgver"

_build_flavor() {
	flavor="$1"
	shift
	_configure="$@"
	CFLAGS="${CFLAGS} -fno-reorder-functions" # Workaround for image#403
	case $CTARGET_ARCH in
		ppc64) export CFLAGS="${CFLAGS} -mno-altivec";;  # Workaround for http://savannah.gnu.org/bugs/?52629
	esac

	msg "Building grub for platform $flavor"
	mkdir -p "$srcdir"/build-$flavor
	cd "$srcdir"/build-$flavor
	$builddir/configure \
		--build=$CBUILD \
		--host=$CHOST \
		--target=$CTARGET \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-nls \
		--disable-werror \
		$_configure
	make
}

build() {
	# missing from source tarball, will cause build error
	printf "depends bli part_gpt\n" > "$builddir"/grub-core/extra_deps.lst
	f=""
	for f in $flavors; do
		case "$f" in
		bios)		_build_flavor $f --with-platform=pc;;
		efi)		_build_flavor $f --with-platform=efi --disable-efiemu;;
		xenhost)	_build_flavor $f --with-platform=xen;;
		*)		_build_flavor $f --with-platform=$f;;
		esac
	done
}

_install_xen() {
	grub_dir=`mktemp -d`
	cfg=`mktemp`
	grub_memdisk=`mktemp`

	mkdir -p $grub_dir/boot/grub
	echo 'normal (memdisk)/grub.cfg' > $cfg
	sed -e "s/@@PVBOOT_ARCH@@/$CARCH/g" \
		$srcdir/grub-xen-host_grub.cfg \
			> $grub_dir/grub.cfg
	tar -cf - -C $grub_dir grub.cfg > $grub_memdisk

	./grub-mkimage \
		-O $CARCH-xen \
		-c $cfg \
		-d ./grub-core ./grub-core/*.mod \
		-m $grub_memdisk \
		-o $pkgdir/grub-$CARCH-xen.bin

	rm -r "$grub_dir"
	rm "$cfg" "$grub_memdisk"
}

_install_flavor() {
	flavor="$1"
	cd "$srcdir"/build-$flavor
	case $flavor in
	xenhost)	_install_xen;;
	*)	 	make DESTDIR="$pkgdir" install-strip;;
	esac
}

package() {
	# install BIOS & EFI version into the same directory
	# and overwrite similar files.
	for f in $flavors; do
		_install_flavor $f
	done

	rm -f "$pkgdir"/usr/lib/charset.alias
	install -D -m644 "$srcdir"/default-grub "$pkgdir"/etc/default/grub
	install -D -m755 "$srcdir"/update-grub "$pkgdir"/usr/sbin
	# remove grub-install warning of missing directory
	mkdir -p "$pkgdir"/usr/share/locale

	for i in "$srcdir"/quirk-*; do
		install -Dm755 "$i" \
			"$pkgdir"/etc/grub-quirks.d/"${i##"$srcdir"/quirk-}"
	done

	mkdir -p "$pkgdir"/etc/easy-boot.d
	ln -s ../../usr/sbin/update-grub \
		"$pkgdir"/etc/easy-boot.d/50-grub
}

bios() {
	pkgdesc="$pkgdesc (BIOS version)"
	depends="$pkgname"
	mkdir -p "$subpkgdir"/usr/lib/grub
	mv "$pkgdir"/usr/lib/grub/*-pc "$subpkgdir"/usr/lib/grub/
}

efi() {
	pkgdesc="$pkgdesc (EFI version)"
	depends="$pkgname efibootmgr efivar"
	mkdir -p "$subpkgdir"/usr/lib/grub
	mv "$pkgdir"/usr/lib/grub/*-efi "$subpkgdir"/usr/lib/grub/
}

xenhost() {
	pkgdesc="$pkgdesc (Xen host version)"
	mkdir -p "$subpkgdir"/usr/lib/grub-xen
	mv "$pkgdir"/*-xen.bin "$subpkgdir"/usr/lib/grub-xen/
}

ieee1275() {
	pkgdesc="$pkgdesc (IEEE-1275 OpenFirmware version)"
	case $CTARGET_ARCH in
		ppc*) depends="$pkgname powerpc-utils"
	esac
	mkdir -p "$subpkgdir"/usr/lib/grub
	mv "$pkgdir"/usr/lib/grub/*-ieee1275 "$subpkgdir"/usr/lib/grub/
}

uboot() {
	pkgdesc="$pkgdesc (U-Boot version)"
	mkdir -p "$subpkgdir"/usr/lib/grub
	mv "$pkgdir"/usr/lib/grub/*-uboot "$subpkgdir"/usr/lib/grub/
}

mount() {
	pkgdesc="Utility to mount filesystems using GRUB modules"
	depends="$pkgname"
	mkdir -p "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/bin/grub-mount "$subpkgdir"/usr/bin/
}

sha512sums="761c060a4c3da9c0e810b0ea967e3ebc66baa4ddd682a503ae3d30a83707626bccaf49359304a16b3a26fc4435fe6bea1ee90be910c84de3c2b5485a31a15be3  grub-2.12.tar.xz
4a6c909a1150b057fb0563f7a0646db33e16f84d6a8c443ef7db4003efd56c211a52f87699c5c95a6ccde65f6db46a8711a5b784f38479db804938dfe3115439  adelie-branding.patch
f1daae3015c84af94aeffce20de1068c3de272aff59f809e956a085cf289c371c61b64ad1d440765c98a4d674761ea67d2986cc4b1d0dfffcf71caaf94c6f600  grub2-accept-empty-module.patch
18502c79128d88154885a6e595bb42d4167f4e08e89e5bd9471234d4e4ba0d6a6ab7dc50a324e9c71ee8a92f3dee8f1a800942adf62ac8bc4041c6a154b19beb  0002-revert-gawk.patch
4b369af412a303464fb538f0f1d7c51dfba4481448409204290e7959397243cb544ecaa8bd9db096788e1eccae0c4c6fac1af9b7819c34f20d01a0eb6c5105ff  0010-fix-gcc-no-pie-specs.patch
60f884595a713e52052eb1741bd2a9f42110736b689c045d9fb23d08970fb8475f25bfdf056552040a7b5559817289774f987c825ef006e556d79f718935376f  xfs.patch
4e7394e0fff6772c89683039ccf81099ebbfe4f498e6df408977a1488fd59389b6e19afdbf0860ec271e2b2aea0df7216243dcc8235d1ca3af0e7f4d0a9d60a4  grub-xen-host_grub.cfg
1cb675fa0af9a1fd8d1a868e3e9de3bfef7eada66c5df0cfef23eca33e93e676eed6e1f67c679690f3e581565b350a9f3ba01aa9b89de66c970ea61ca21bcd65  default-grub
8e5f1cf91da9fd956695438509bb26e45f38170ca90d99a996e859053d72c82b7e4b02cb8979f047fc53498036f47b5400bf54687c390793995a05ded4216d55  update-grub
78b7ec141a364994c7de181e47fedca820add9960c56c7adf4c14ee11d5249a0887fd788ecd5d24b9bdd102b7c40395181e2f7c3fe5ab795dd7c0057ba1115c5  quirk-01_radeon_agpmode"
