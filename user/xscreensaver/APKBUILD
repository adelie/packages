# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=xscreensaver
pkgver=6.08
pkgrel=0
pkgdesc="X Screensaver suite"
url="https://www.jwz.org/xscreensaver/"
arch="all"
options="!check suid"  # No test suite.
license="MIT"
depends=""
makedepends="bc gtk+3.0-dev intltool libice-dev libjpeg-turbo-dev libx11-dev
	libxft-dev libxi-dev libxinerama-dev libxml2-dev libxml2-utils glu-dev
	libxrandr-dev libxt-dev linux-pam-dev mesa-dev libglvnd-dev xorgproto-dev
	desktop-file-utils elogind-dev xdg-utils"
subpackages="$pkgname-doc $pkgname-elogind"
source="https://www.jwz.org/xscreensaver/xscreensaver-$pkgver.tar.gz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--with-pam \
		--with-shadow \
		--with-elogind
	make
}

package() {
	mkdir -p "$pkgdir"/etc/pam.d
	make install_prefix="$pkgdir" install
}

elogind() {
	pkgdesc="$pkgdesc (elogind integration)"
	install_if="$pkgname=$pkgver-r$pkgrel elogind"
	mkdir -p "$subpkgdir"/usr/libexec/$pkgname
	mv "$pkgdir"/usr/libexec/$pkgname/xscreensaver-systemd \
		"$subpkgdir"/usr/libexec/$pkgname/
}

sha512sums="a333d4921ed3a8219f3b672dd68543aea31fcd64ea6766143f1c7f6e5ed3bc7122355635d5afba1632457ad71a11cabdc7b2756431e4f1dc7e0426ae0896e6db  xscreensaver-6.08.tar.gz"
