# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=fuse
pkgver=2.9.9
pkgrel=0
pkgdesc="Linux Filesystem in Userspace (legacy version)"
url="https://github.com/libfuse/libfuse"
arch="all"
options="suid !check"  # No test suite.
license="GPL-2.0-only AND LGPL-2.1-only AND (GPL-2.0-only OR BSD-2-Clause)"
depends="fuse-common"
makedepends=""
subpackages="$pkgname-dev $pkgname-doc"
source="https://github.com/libfuse/libfuse/releases/download/$pkgname-$pkgver/$pkgname-$pkgver.tar.gz
	fix-realpath.patch
"

# secfixes:
#   2.9.8-r0:
#   - CVE-2018-10906

build() {
	UDEV_RULES_PATH='/lib/udev/rules.d' ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-static \
		--disable-example \
		--enable-lib \
		--enable-util
	make
}

package() {
	make DESTDIR="$pkgdir" install
	rm -r "$pkgdir"/dev "$pkgdir"/etc/init.d
}

sha512sums="3d82fafd04f2924299bb09d2bb144935fa004a43fb9a6568e2e1cc5a60fded2efa1711029a0d7f12b3e6e4501f7217f2ee3545646d89da8dece083cb390f23e2  fuse-2.9.9.tar.gz
5672ceb35acabb4bd97b6efc30614f22def62882fe198e2a8598a074d45b6b5337c082a5e09b5e399b6e5212dc1fbde9071c2a5051534c32091444c350b9c657  fix-realpath.patch"
