From 63ae4ecc780ba42901e6934302b75bd18d3bc5eb Mon Sep 17 00:00:00 2001
From: Albert Astals Cid <aacid@kde.org>
Date: Wed, 13 Apr 2022 01:25:44 +0200
Subject: [PATCH 1/3] PdfImport: Fix compile with newer poppler

Brings a dependency on poppler-qt5 to be able to include the version
header, honestly it's not strictly needed, one could do a
check_cxx_source_compiles, but I don't care about Calligra enough to
spend more time making it compile while it's using poppler the wrong
way.
---
 CMakeLists.txt                    | 1 +
 filters/karbon/pdf/CMakeLists.txt | 2 +-
 filters/karbon/pdf/PdfImport.cpp  | 9 +++++++++
 3 files changed, 11 insertions(+), 1 deletion(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index bdd9ed74406..d8c86810b54 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -998,6 +998,7 @@ calligra_drop_product_on_bad_condition( FILTER_WPG_TO_ODG
 calligra_drop_product_on_bad_condition( FILTER_PDF_TO_SVG
     NOT_WIN "not supported on Windows"
     PopplerXPDFHeaders_FOUND "poppler xpdf headers not found"
+    Poppler_FOUND "poppler qt5 headers not found"
     )
 
 calligra_drop_product_on_bad_condition( FILTER_HTML_TO_ODS
diff --git a/filters/karbon/pdf/CMakeLists.txt b/filters/karbon/pdf/CMakeLists.txt
index 94d4071da3d..ef360f44359 100644
--- a/filters/karbon/pdf/CMakeLists.txt
+++ b/filters/karbon/pdf/CMakeLists.txt
@@ -19,7 +19,7 @@ set(pdf2svg_PART_SRCS PdfImportDebug.cpp PdfImport.cpp SvgOutputDev.cpp )
 add_library(calligra_filter_pdf2svg MODULE ${pdf2svg_PART_SRCS})
 calligra_filter_desktop_to_json(calligra_filter_pdf2svg calligra_filter_pdf2svg.desktop)
 
-target_link_libraries(calligra_filter_pdf2svg komain Poppler::Core)
+target_link_libraries(calligra_filter_pdf2svg komain Poppler::Core Poppler::Qt5)
 
 install(TARGETS calligra_filter_pdf2svg DESTINATION ${PLUGIN_INSTALL_DIR}/calligra/formatfilters)
 
diff --git a/filters/karbon/pdf/PdfImport.cpp b/filters/karbon/pdf/PdfImport.cpp
index 286f5fa78bc..c171c754116 100644
--- a/filters/karbon/pdf/PdfImport.cpp
+++ b/filters/karbon/pdf/PdfImport.cpp
@@ -30,6 +30,10 @@
 
 #include <kpluginfactory.h>
 
+#include <poppler-version.h>
+
+#define POPPLER_VERSION_MACRO ((POPPLER_VERSION_MAJOR << 16) | (POPPLER_VERSION_MINOR << 8) | (POPPLER_VERSION_MICRO))
+
 // Don't show this warning: it's an issue in poppler
 #ifdef __GNUC__
 #pragma GCC diagnostic ignored "-Wunused-parameter"
@@ -73,8 +77,13 @@ KoFilter::ConversionStatus PdfImport::convert(const QByteArray& from, const QByt
     if (! globalParams)
         return KoFilter::NotImplemented;
 
+#if POPPLER_VERSION_MACRO < QT_VERSION_CHECK(22, 03, 0)
     GooString * fname = new GooString(QFile::encodeName(m_chain->inputFile()).data());
     PDFDoc * pdfDoc = new PDFDoc(fname, 0, 0, 0);
+#else
+    std::unique_ptr<GooString> fname = std::make_unique<GooString>(QFile::encodeName(m_chain->inputFile()).data());
+    PDFDoc * pdfDoc = new PDFDoc(std::move(fname));
+#endif
     if (! pdfDoc) {
 #ifdef HAVE_POPPLER_PRE_0_83
         delete globalParams;
-- 
2.35.1

From feb28e5fbd4e3b41c74da1220bc14826bcf9b3c7 Mon Sep 17 00:00:00 2001
From: Dag Andersen <dag.andersen@kdemail.net>
Date: Wed, 13 Apr 2022 14:45:33 +0200
Subject: [PATCH 2/3] PdfImport: Fix compile with newer poppler

Also fixes odg2pdf filter.

Same solution as commit 236bacbe13739414e919de868283b0caf2df5d8a
by accid@kde.org.
---
 filters/karbon/pdf/CMakeLists.txt    | 2 +-
 filters/karbon/pdf/Pdf2OdgImport.cpp | 9 +++++++++
 filters/karbon/pdf/SvgOutputDev.cpp  | 9 +++++++++
 3 files changed, 19 insertions(+), 1 deletion(-)

diff --git a/filters/karbon/pdf/CMakeLists.txt b/filters/karbon/pdf/CMakeLists.txt
index ef360f44359..849baa70f12 100644
--- a/filters/karbon/pdf/CMakeLists.txt
+++ b/filters/karbon/pdf/CMakeLists.txt
@@ -29,6 +29,6 @@ set(pdf2odg_PART_SRCS PdfImportDebug.cpp Pdf2OdgImport.cpp SvgOutputDev.cpp)
 add_library(calligra_filter_pdf2odg MODULE ${pdf2odg_PART_SRCS})
 calligra_filter_desktop_to_json(calligra_filter_pdf2odg calligra_filter_pdf2odg.desktop)
 
-target_link_libraries(calligra_filter_pdf2odg kopageapp karbonui Poppler::Core)
+target_link_libraries(calligra_filter_pdf2odg kopageapp karbonui Poppler::Core Poppler::Qt5)
 
 install(TARGETS calligra_filter_pdf2odg DESTINATION ${PLUGIN_INSTALL_DIR}/calligra/formatfilters)
diff --git a/filters/karbon/pdf/Pdf2OdgImport.cpp b/filters/karbon/pdf/Pdf2OdgImport.cpp
index 745239c9c21..b5f3722b320 100644
--- a/filters/karbon/pdf/Pdf2OdgImport.cpp
+++ b/filters/karbon/pdf/Pdf2OdgImport.cpp
@@ -40,6 +40,8 @@
 
 #include <kpluginfactory.h>
 
+#include <poppler-version.h>
+
 // Don't show this warning: it's an issue in poppler
 #ifdef __GNUC__
 #pragma GCC diagnostic ignored "-Wunused-parameter"
@@ -49,6 +51,8 @@
 #include <PDFDoc.h>
 #include <GlobalParams.h>
 
+#define POPPLER_VERSION_MACRO ((POPPLER_VERSION_MAJOR << 16) | (POPPLER_VERSION_MINOR << 8) | (POPPLER_VERSION_MICRO))
+
 K_PLUGIN_FACTORY_WITH_JSON(Pdf2OdgImportFactory, "calligra_filter_pdf2odg.json",
                            registerPlugin<Pdf2OdgImport>();)
 
@@ -86,8 +90,13 @@ KoFilter::ConversionStatus Pdf2OdgImport::convert(const QByteArray& from, const
     if (! globalParams)
         return KoFilter::NotImplemented;
 
+#if POPPLER_VERSION_MACRO < QT_VERSION_CHECK(22, 03, 0)
     GooString * fname = new GooString(QFile::encodeName(m_chain->inputFile()).data());
     PDFDoc * pdfDoc = new PDFDoc(fname, 0, 0, 0);
+#else
+    std::unique_ptr<GooString> fname = std::make_unique<GooString>(QFile::encodeName(m_chain->inputFile()).data());
+    PDFDoc * pdfDoc = new PDFDoc(std::move(fname));
+#endif
     if (! pdfDoc) {
 #ifdef HAVE_POPPLER_PRE_0_83
         delete globalParams;
diff --git a/filters/karbon/pdf/SvgOutputDev.cpp b/filters/karbon/pdf/SvgOutputDev.cpp
index b980fdf60f6..76b909e3b69 100644
--- a/filters/karbon/pdf/SvgOutputDev.cpp
+++ b/filters/karbon/pdf/SvgOutputDev.cpp
@@ -35,6 +35,10 @@
 #include <QPen>
 #include <QImage>
 
+#include <poppler-version.h>
+
+#define POPPLER_VERSION_MACRO ((POPPLER_VERSION_MAJOR << 16) | (POPPLER_VERSION_MINOR << 8) | (POPPLER_VERSION_MICRO))
+
 class SvgOutputDev::Private
 {
 public:
@@ -410,7 +414,12 @@ void SvgOutputDev::drawString(GfxState * state, const GooString * s)
     if (s->getLength() == 0)
         return;
 
+#if POPPLER_VERSION_MACRO < QT_VERSION_CHECK(22, 03, 0)
     GfxFont * font = state->getFont();
+#else
+    std::shared_ptr<GfxFont> font = state->getFont();
+#endif
+
 
     QString str;
 
-- 
2.35.1

From 2dd2c02094ab9dfd964b2363039527d414db3b28 Mon Sep 17 00:00:00 2001
From: Albert Astals Cid <aacid@kde.org>
Date: Wed, 13 Apr 2022 21:30:14 +0200
Subject: [PATCH 3/3] SvgOutputDev: Fix ifdef version for getFont API change

---
 filters/karbon/pdf/SvgOutputDev.cpp | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/filters/karbon/pdf/SvgOutputDev.cpp b/filters/karbon/pdf/SvgOutputDev.cpp
index 76b909e3b69..588cda8ecbe 100644
--- a/filters/karbon/pdf/SvgOutputDev.cpp
+++ b/filters/karbon/pdf/SvgOutputDev.cpp
@@ -414,7 +414,7 @@ void SvgOutputDev::drawString(GfxState * state, const GooString * s)
     if (s->getLength() == 0)
         return;
 
-#if POPPLER_VERSION_MACRO < QT_VERSION_CHECK(22, 03, 0)
+#if POPPLER_VERSION_MACRO < QT_VERSION_CHECK(22, 04, 0)
     GfxFont * font = state->getFont();
 #else
     std::shared_ptr<GfxFont> font = state->getFont();
-- 
2.35.1

