# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdevelop
pkgver=5.6.2
_llvmver=18
pkgrel=1
pkgdesc="KDE Integrated Development Environment (IDE)"
url="https://www.kdevelop.org/"
arch="all"
options="!check"  # Requires real X11 session.
license="GPL-2.0+"
# Yes, meson is a runtime dependency.
depends="clazy cppcheck kdevelop-pg-qt meson okteta qt5-qttools
	shared-mime-info"
makedepends="qt5-qtbase-dev qt5-qtdeclarative-dev cmake extra-cmake-modules
	karchive-dev kconfig-dev kcrash-dev kdeclarative-dev kdoctools-dev
	kguiaddons-dev ki18n-dev kiconthemes-dev kio-dev kitemmodels-dev
	kitemviews-dev kjobwidgets-dev kcmutils-dev knewstuff-dev
	knotifications-dev knotifyconfig-dev kparts-dev kservice-dev
	ktexteditor-dev kwindowsystem-dev kxmlgui-dev threadweaver-dev
	libksysguard-dev grantlee-dev qt5-qttools-dev boost-dev clang-dev
	qt5-qtwebkit-dev purpose-dev llvm$_llvmver-dev plasma-framework-dev
	okteta-dev apr-dev astyle-dev krunner-dev subversion-dev
	libkomparediff2-dev kdevelop-pg-qt-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/kdevelop/$pkgver/src/kdevelop-$pkgver.tar.xz
	libarchive-tar.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DCLANG_BUILTIN_DIR=/usr/lib/clang/8.0.1/include \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="d5ec09b3eda439436bee4dee88612deedf02caf949e74cb7d8405fbb40ae6f16ae1b5278da0574f41c43d186edbeec6b8c40918ef7f5fafa437e07a8c4ae92ce  kdevelop-5.6.2.tar.xz
c8b9cbdabb9285c183347889264e2089db5520879454fa4c85c2c1aa97612c3316c92f89bbd474d2c946c9bc8f29e4b8da79eabd6cb14e1213b0b2cdfba0c328  libarchive-tar.patch"
