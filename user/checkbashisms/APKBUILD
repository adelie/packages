# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=checkbashisms
pkgver=2.20.4
pkgrel=0
pkgdesc="Check shell scripts for POSIX compliance"
url="https://salsa.debian.org/debian/devscripts"
arch="noarch"
license="GPL-2.0+"
depends="perl"
checkdepends="shunit2"
makedepends=""
subpackages="$pkgname-doc $pkgname-bash-completion:bashcomp:noarch"
# find timestamp here: https://snapshot.debian.org/package/devscripts/
source="https://snapshot.debian.org/archive/debian/20200628T204444Z/pool/main/d/devscripts/devscripts_${pkgver}.tar.xz"
builddir="$srcdir/devscripts-$pkgver"

prepare() {
	default_prepare
	sed -i "s/###VERSION###/$pkgver/" "scripts/$pkgname.pl"
}

check() {
	./test/test_checkbashisms
}

package() {
	install -Dm755 "scripts/$pkgname.pl" "$pkgdir/usr/bin/$pkgname"
	install -Dm644 "scripts/$pkgname.1" "$pkgdir/usr/share/man/man1/$pkgname.1"
}

bashcomp() {
	pkgdesc="Bash completions for $pkgname"
	depends=""
	install_if="$pkgname=$pkgver-r$pkgrel bash-completion"

	install -Dm644 "$builddir/scripts/$pkgname.bash_completion" \
		"$subpkgdir/usr/share/bash-completion/completions/$pkgname"
}

sha512sums="49e9ae38ca3682f399a17f2108a7ee15198dc955d8c7c4c1b043456c19cb35b47b6116de9a2cfaccbdd7560e79426ca0953674e5a89ac226d21af2c1cf847e5c  devscripts_2.20.4.tar.xz"
