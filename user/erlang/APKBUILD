# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=erlang
pkgver=27.0
pkgrel=0
pkgdesc="Soft real-time system programming language"
url="https://www.erlang.org/"
arch="all"
license="Apache-2.0"
depends=""
makedepends="flex libxml2-utils libxslt-dev ncurses-dev openssl-dev perl
	unixodbc-dev"
subpackages="$pkgname-dev"
source="erlang-$pkgver.tar.gz::https://github.com/erlang/otp/archive/OTP-$pkgver.tar.gz"
builddir="$srcdir/otp-OTP-$pkgver"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-shared-zlib \
		--enable-ssl=dynamic-ssl-lib \
		--enable-threads \
		--disable-hipe
	make
}

check() {
	export ERL_TOP=$builddir

	make release_tests

	for _header in erl_fixed_size_int_types.h \
		${CHOST}/erl_int_sizes_config.h; do
		cp erts/include/$_header erts/emulator/beam/
	done
	cd release/tests/test_server
	$ERL_TOP/bin/erl -s ts install -s ts smoke_test batch -s init stop
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="16945c50c09caad9aac6808470ea2a0e282713f23e54cd4ad60510b114811b5deac427acb36cc8049443b4e3c3302c40b02c696cf3f2028f8bd3e836c8c48ea9  erlang-27.0.tar.gz"
