# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=okular
pkgver=22.04.2
pkgrel=0
pkgdesc="Universal document reader developed by KDE"
url="https://okular.kde.org/"
arch="all"
license="GPL-2.0+ AND LGPL-2.0+"
depends="kirigami2"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	qt5-qtdeclarative-dev karchive-dev kbookmarks-dev kcompletion-dev
	kconfigwidgets-dev kcoreaddons-dev kdoctools-dev kiconthemes-dev
	kio-dev kjs-dev kparts-dev kwallet-dev kwindowsystem-dev khtml-dev
	threadweaver-dev kactivities-dev poppler-qt5-dev tiff-dev qca-dev
	libjpeg-turbo-dev kpty-dev kirigami2-dev djvulibre-dev libkexiv2-dev
	libspectre-dev ebook-tools-dev libzip-dev poppler-dev qt5-qtspeech-dev
	purpose-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/okular-$pkgver.tar.xz
	es-doc-fix.patch
	"

# secfixes:
#   19.12.3-r1:
#     - CVE-2020-9359

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS -D_GNU_SOURCE" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# All other tests require X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -R '^shelltest'
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="5958fae2aa28026e2a30e63bdd4cfef05f9cfe7b1eee3e711a634b9ab278c4dcf41b6001db0976693ccaa4d2c234c03f5ed34a683c301e5153388d72f9f52e49  okular-22.04.2.tar.xz
de32eabda7ee84c4d894b02c56c7d66d8e2332688c726ad95e1b61c1e730035081ff7721275c7b7a9884aabc268ee0115d9ab8e5f52ae8838e1c09c471c81932  es-doc-fix.patch"
