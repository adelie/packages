# Maintainer: Max Rees <maxcrees@me.com>
pkgname=davmail
pkgver=5.5.1.3299
_pkgver=${pkgver%.*}-${pkgver##*.}
pkgrel=0
pkgdesc="POP/IMAP/SMTP/Caldav/Carddav/LDAP proxy for Exchange/O365 servers"
url="http://davmail.sourceforge.net/"  # No HTTPS
arch="noarch !armv7"  #1007
options="!check"  # Requires running mail servers and X11
license="GPL-2.0+ AND MIT AND (LGPL-2.1+ or Apache-2.0) AND BSD-2-Clause AND Apache-2.0 AND LGPL-2.1 AND BSD-3-Clause AND (CDDL-1.1 OR GPL-2.0 WITH Classpath-exception-2.0)"
depends="/bin/sh openjdk8-jre"
makedepends="ant desktop-file-utils"
subpackages=""
source="https://sourceforge.net/projects/davmail/files/davmail/${pkgver%.*}/davmail-src-$_pkgver.tgz
	davmail.sh
	"
builddir="$srcdir/$pkgname-src-$_pkgver"

_ant() {
	. /etc/profile.d/java.sh
	. /etc/profile.d/ant.sh
	ant "$@"
}

build() {
	_ant jar
}

check() {
	_ant test
}

package() {
	_ant prepare-dist

	install -Dm755 "$srcdir"/davmail.sh \
		"$pkgdir"/usr/bin/davmail

	rm -f dist/lib/*growl* \
		dist/lib/*x86*.jar \
		dist/lib/junit-*.jar \
		dist/lib/winrun4j-*.jar
	install -Dm644 -t "$pkgdir"/usr/lib/java/davmail/lib \
		dist/lib/* dist/davmail.jar

	desktop-file-install --dir "$pkgdir"/usr/share/applications \
		src/desktop/davmail.desktop

	for _i in 16 32 48 128; do
		case $_i in
		16) _fn=src/java/tray.png;;
		*) _fn=src/java/tray$_i.png;;
		esac
		install -Dm644 $_fn \
			"$pkgdir"/usr/share/icons/hicolor/${_i}x${_i}/apps/davmail.png
	done
}

sha512sums="0d3481f0930e09b30ae4341149b443bd92f107872d4e39dcc66e5e6c2fa6a667f9dde2c5a49bc5c96375d9e9b4b824a4211bf90a985a2cc2b1b8aa7613f02b5d  davmail-src-5.5.1-3299.tgz
e937f3bdf71cddba4678b768bc6fc87320cd2a76d324f5ef99a0248ddc989d89c25e1f0e291416acb264cb2a1dbfef6d5eeea092c95d9be3f75669d6f68d1fdd  davmail.sh"
