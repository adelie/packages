# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=qt-creator
pkgver=4.15.2
_llvmver=18
pkgrel=3
pkgdesc="Cross-platform multi-language programming IDE"
url="https://doc.qt.io/qtcreator/index.html"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.0 WITH Qt-LGPL-exception-1.1"
depends="qt5-qtquickcontrols"
makedepends="qt5-qtbase-dev qt5-qtdeclarative-dev qt5-qtserialport-dev
	qt5-qtscript-dev qt5-qttools-dev clang-dev llvm${_llvmver}-dev python3
	libexecinfo-dev"
subpackages="$pkgname-dev"
source="https://download.qt.io/official_releases/qtcreator/${pkgver%.*}/$pkgver/$pkgname-opensource-src-$pkgver.tar.xz
	llvm13.patch
	llvm15.patch
	llvm16.patch
	llvm16-deux.patch
	llvm17.patch
	llvm18.patch
	llvm19.patch
	malloc_trim.patch
	cstdint.patch
	"
ldpath="/usr/lib/qtcreator"
builddir="$srcdir/$pkgname-opensource-src-$pkgver"

build() {
	export LLVM_CONFIG="$(command -v llvm${_llvmver}-config)"
	export LLVM_INSTALL_DIR=/usr
	qmake -r "QMAKE_CFLAGS += $CFLAGS" "QMAKE_CXXFLAGS += $CXXFLAGS" "QMAKE_LFLAGS += $LDFLAGS" "QMAKE_LIBS += -lexecinfo"
	make
}

package() {
	make install INSTALL_ROOT="$pkgdir"/usr
}

sha512sums="b29d5d97a1faa8d5756069d90c18d6d367e09057b97bd75c774bc2d1e373f9f4dcc24211188259f8d397bb932a65daaacda433bced796fd165654f5c3c621258  qt-creator-opensource-src-4.15.2.tar.xz
c38cf1aea69bc52864d5aa4b6fbc01b5e91b1514a616056b270fcccc2744bb418eb93a470462823c05e00e44f44c45d8e26effd8aa72a6817402ddd9a6dd0cc4  llvm13.patch
9bcb1a67ea28fa4b3a9abf120d96c44e9949da083f0010b1a32ff27873e290a419b4994503826ba721587ba9b9bb97eb81c59e9d192e916bcf6430e21b1b7feb  llvm15.patch
0a986139b3ba25d4c5430bfde1ca10cb024666150c9dac4eb1b3f5bbf099c4017332745d2277837395b1761fca1f598a1807b6d21342b497d4800bb988236250  llvm16.patch
566307ecbc725863149c2407617ef5d2021c9cd6a5e69a8ccf3f9a7188e27fc77b5cca12e92c35255ce03f925f7e0370412c1f255e03b34dd165abee959d9eec  llvm16-deux.patch
be2a3e99e1a7b72d3b8d092edaffc60249efbbe27d5037cebba388d36f0d404e8d109489336c83f684195532eb23d73ea89f57da31b3499a1ee84a86ce3653b7  llvm17.patch
02d0ac36e4c9fd5682d45ef5f040f6ef11c25b3c9a0865cb8a1ef660dfb067a3dbeac1187246da79a506db6c24379b77a7d8510edcf10c9a56c6a9de1b507672  llvm18.patch
ce849abae28a3fd946b490e55cc44f4c19fe3ccf167e936c9e01b7c240c98b7f7af27336549ccbe18995fd2ae81366278a8345f54d3d87127232c06649af0e71  llvm19.patch
7fcb9b293daecc4c0f294d372507db9a6282479c4cf45999446193d9f35552db2532d47dedc9c5666a10855e20d0fe655f0ee0408abca8cc8ffbf121a230960f  malloc_trim.patch
7d7a739cf98c1bc4232d9d14334e3c3a319746db931aaad4d441c903351c56869015ce427e2098fbe930d56a2dd433b1869fbbf5a78f91c10a94f5a47a778ea0  cstdint.patch"
