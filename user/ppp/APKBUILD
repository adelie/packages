# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ppp
pkgver=2.4.8
pkgrel=1
pkgdesc="Point-to-Point Protocol (PPP) implementation for serial networking"
url="https://ppp.samba.org/"
arch="all"
options="!check"  # No test suite.
license="BSD-4-Clause AND GPL-2.0-only AND GPL-2.0+ AND zlib AND LGPL-2.0+"
depends=""
makedepends="bsd-compat-headers linux-pam-dev libpcap-dev openssl-dev utmps-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-openrc"
source="https://github.com/paulusmack/ppp/archive/ppp-$pkgver.tar.gz
	http://distfiles.gentoo.org/distfiles/ppp-dhcpc.tgz
	03_all_use_internal_logwtmp.patch
	04_all_mpls.patch
	06_all_killaddr-smarter.patch
	08_all_wait-children.patch
	12_all_linkpidfile.patch
	16_all_auth-fail.patch
	19_all_radius_pid_overflow.patch
	20_all_dev-ppp.patch
	24_all_passwordfd-read-early.patch
	26_all_pppd-usepeerwins.patch
	28_all_connect-errors.patch
	32_all_pado-timeout.patch
	34_all_lcp-echo-adaptive.patch
	80_all_eaptls-mppe-1.101a.patch
	85_all_dhcp-make-vars.patch
	86_all_dhcp-sys_error_to_strerror.patch
	adelie.patch
	CVE-2020-8597.patch
	dhcp.patch
	install-path.patch
	musl-fix-headers.patch
	utmpx.patch
	fix-linux-5.15-include.patch

	ppp.mod
	ppp.pamd
	pppd.initd
	"
builddir="$srcdir"/ppp-ppp-$pkgver

# secfixes:
#   2.4.8-r0:
#   - CVE-2020-8597

prepare() {
	mv "$srcdir"/dhcp "$builddir"/pppd/plugins
	default_prepare
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make COPTS="$CFLAGS -D_GNU_SOURCE" \
		LIBS="-lutmps -lskarnet -lcrypto -lssl -lpam -lpcap"
	make -C contrib/pppgetpass pppgetpass.vt
}

package() {
	make INSTROOT="$pkgdir" install

	install -Dm 0644 -t "$pkgdir"/usr/include/net/ include/net/ppp_defs.h

	install -d "$pkgdir"/etc/ppp/peers
	install -m 0600 etc.ppp/pap-secrets \
		"$pkgdir"/etc/ppp/pap-secrets.example
	install -m 0600 etc.ppp/chap-secrets \
		"$pkgdir"/etc/ppp/chap-secrets.example
	install -m 0644 etc.ppp/options "$pkgdir"/etc/ppp/options

	install -Dm 0644 "$srcdir"/ppp.pamd "$pkgdir"/etc/pam.d/ppp
	install -Dm 0755 -t "$pkgdir"/usr/bin/ scripts/pon \
		scripts/poff scripts/plog
	install -Dm 0644 -t "$pkgdir"/usr/share/man/man1/ scripts/pon.1

	install -Dm 0755 contrib/pppgetpass/pppgetpass.vt \
		"$pkgdir"/usr/sbin/pppgetpass
	install -Dm 0644 -t "$pkgdir"/usr/share/man/man8/ \
		contrib/pppgetpass/pppgetpass.8

	install -Dm 0644 "$srcdir"/ppp.mod "$pkgdir"/etc/modprobe.d/ppp.conf
	install -Dm 0755 "$srcdir"/pppd.initd "$pkgdir"/etc/init.d/pppd
}

sha512sums="a99b3b6c7bd80cd133bda4e29d33c793a76f3b67e1f8db774547e88932ce29564fad390a4f51d3fe30a75e006499b95000b042ae0f64cd360548426f8091a478  ppp-2.4.8.tar.gz
aeaf791b14f5a09c0e2079072a157e65132cbff46e608bc0724e6a5827a01da934f5006e2774eb7105f83e607a52cb4987238f4385cf6f5cc86cbe305a556738  ppp-dhcpc.tgz
4c4a5cc6fd8ce3203c41ff07fc0ce5f0468985c779fe05030898d36c404d2086ce7a49336ac58e6502fc2fd14c4de9006028fe19c500d2cac890a16a55c723e8  03_all_use_internal_logwtmp.patch
df263b6a8395be1f3aa0a87aca8fe7bcda1395208df8df73de98decdb59a70a67242a4f7a7227db2c4b470fc1b44d771967d8c91c16cfaffaa3eb41110dc3b28  04_all_mpls.patch
b49086401c1b395ee6caba0418272b2d1b2ab9dcf6f1fc7e7f1280e94fcf17c8fdb884a19f4ef911bd7849a9cceb4cc07fc75438726340cd8c17265d4c2bd4d8  06_all_killaddr-smarter.patch
807707ee9795ef4300b92bacdeba6b9e7332b6ce5a355de3ce77ddcc6dafce069f024131fd3ef282e38be3499e4230ad50fdda2b96e00891f5df1696234b775b  08_all_wait-children.patch
122b9e3dbc3a9accacb31c653b3c0d368e8cdf3d954a6c93d04ac26ca8f3cb5bfcf8a01881d1cf08e855a1d2d0bd86e7fadba22bb5ada1a86f78e6b9820e6687  12_all_linkpidfile.patch
3a23ef3619b2840eb3f1f7e14bd4765526b09acdfa8ab4d659ad8525a6f96d0cfb7c9fef042cde99ba1de8cf5caa74faa32e2a988453b4b17a70d5cc9a4bcf41  16_all_auth-fail.patch
9fdb3346ef13b250f91f1af55c0efa0f836a60abe9e62fceed30df4e431a3bccdd33b083c2662c45e2091085438ba2221cdc4ae51fc1b05a666d77f74d461214  19_all_radius_pid_overflow.patch
82c80701095a2d9707afbf5fc29bdf2fc3f92252b7de5de1d639f8607096a9d34ce90ffd0a2f770512b590a27dec56f2b03e5e9f4c9e56e1f362a2387d9fb568  20_all_dev-ppp.patch
2508cf1285a086c917ba5deffc96c1c29a511f1c1a0ef94c811b2bf139aed0218f34d7c1f1004890e105c47dffc5834a049dbe7686611d0fc8b8607ccdc86105  24_all_passwordfd-read-early.patch
3eb55fb681e7fecf4e09b53683f509d2db3779599dd60fb89f88cd644c92d779f4720114546ba458262e8239581061e4a450143338c4725ada18b7ca314e12b0  26_all_pppd-usepeerwins.patch
2e0bd81124bcd7c1234089f11e0b607b19047d279dc436ced3a4b8d793bcee4fcececd948b6a0755a02f68542c5c5e30b6f8541f90b998c09da8d50362827520  28_all_connect-errors.patch
32c77c938cd607e44bd358cbc3f0eaf99178084d936bf8e6b8aeb20e881ac9d734008cfb93ff3b1fd2783dae0c46e963c3ee1d1b128712b4ba342db069e26636  32_all_pado-timeout.patch
0bd928f45708f10835250fd6a6162f6e148dca172c810511c1f4b1fe56602c4869e7f622e95a3f4a41e5502ddefd0cf6054cd57211bc83426d5da5b5f10dac26  34_all_lcp-echo-adaptive.patch
977b247e87557c4c57a871470c40670457e285ca59357dabab854ab16cc5578526ddf880410aa2efc7078e16d32d7afea4b69928af14ac4449523f6a591284f1  80_all_eaptls-mppe-1.101a.patch
2d294bfe455648949cedb47a12a07913f0395aadbe2566c1e90d70fc37baa8635a667ab45195a697567f8d52de88771c499adffee82cde2e9e318ed858b6007b  85_all_dhcp-make-vars.patch
44d5528c057d0abf2b45ba04943a52b6b94209434a06aa565f8a74acdd660efd86fe13280d540383168eaedad9f627673410bb8468860b64adb3145030e12889  86_all_dhcp-sys_error_to_strerror.patch
350ed598c9690aa2f357ead34eb17a67dc5e34a5bd7529809288c5e0151728882158f32734041f2005413ac701db84d76501edabdcc6c64750dfce2927e2aa2a  adelie.patch
e5638fd6e7b573552a7354fad3c91a8bde86760fb17d510e7e925ee8c1f60f7d39efe95134cb0a1c46b1e9fb14c987f224b4adbb1f3c75633b2bea0eccaff7c6  CVE-2020-8597.patch
6d22e4b601dd3eabd55751408b03c4036815ca64184f9c9f07f2368d2f5a6a9081ce7a88e5c7d5ee60708c430c9cf55070e105ce00adb1325b60cddada4bf562  dhcp.patch
fb4ae2c2ba4ecdd1c85f6e5f730fd9659cf1fbc7a8a004b09042adafee7e4af6238df8deb3dbd3dc9c69407d9ebc4c82e1792a43b4aaf8ac18ebe18268b50597  install-path.patch
ba240d483ada6865bf52497ce00cb9ae8d750e8f650e501d0d068b132d364ae4d09ac2e290b903e246aa35d33cf984aa29a6be41906df355b1c1e2b7637dc8b0  musl-fix-headers.patch
723ff3dd0aee13f9878559aa433b314af6043523a2bafd5957258809a645942f7d34b5bd659869a1528cf7b1a462ad7cc2dbf18e7986220f5f685f2c1ea1d36b  utmpx.patch
cb73d66244e457bb8b01f5ddc3132f1599542e872033f392841d7218749de6863083eb2746e43da7bbdabb292c012016e59dc29963040a0dfea5a8bfec287d36  fix-linux-5.15-include.patch
58bf5d6d286a08bd6dd595b39ee425efedd5745dddf33a9c90505891546eb46f4cf1306d83911bef61bc4611816aa0f6aef5d3e0f14c2f4ddd0a588780570041  ppp.mod
e30a397392d467ac3c78f6533f3adff5de7d38f0372d8d6f7b186db4ec69ddf12463d467d8a86eb5867effeb7dd6bd16942a98fb3a3ab59ff754a123e16d0938  ppp.pamd
bd6f43588b037367ffdb57f5e331492dcaa5969003e219c2dc8b90e6be1aa407282ff6114b91d1379ebeff766983fa0622456520cc0ac592b4f0b1496acf21bf  pppd.initd"
