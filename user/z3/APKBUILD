# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=z3
pkgver=4.8.17
pkgrel=0
pkgdesc="Mathematical theorem prover"
url=" "
arch="all"
license="MIT"
depends=""
makedepends="cmake gmp-dev python3-dev cmd:which"
subpackages="$pkgname-dev py3-$pkgname:py3:noarch"
source="https://github.com/Z3Prover/z3/archive/z3-$pkgver.tar.gz"
builddir="$srcdir/z3-z3-$pkgver"

case "$CARCH" in
pmmx) options="$options textrels";;
esac

build() {
	cmake \
		-DCMAKE_INSTALL_PREFIX="/usr" \
		-DCMAKE_INSTALL_LIBDIR="lib" \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DZ3_BUILD_LIBZ3_SHARED=TRUE \
		-DZ3_USE_LIB_GMP=TRUE \
		-DZ3_BUILD_PYTHON_BINDINGS=TRUE \
		-DZ3_INSTALL_PYTHON_BINDINGS=TRUE \
		-DPYTHON_EXECUTABLE=/usr/bin/python3 \
		-DZ3_INCLUDE_GIT_DESCRIBE=FALSE \
		-DZ3_INCLUDE_GIT_HASH=FALSE \
		-Bbuild .
	make -C build
}

check() {
	make -C build test-z3
	build/test-z3 /a
}

package() {
	make -C build DESTDIR="$pkgdir" install
}

py3() {
	pkgdesc="$pkgdesc (Python bindings)"
	depends="python3"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/python3* "$subpkgdir"/usr/lib/
}

sha512sums="95517014ec1798c2552253dd5cde6f955896ab297a4f56294f4bc6f2c5428069015f513c6eb9a090a809cfcf4cb1cc38cc83818f19b5b1051e4e6c06f973747d  z3-4.8.17.tar.gz"
