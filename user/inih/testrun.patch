From 59461b32ee99e1573ecee98c1122815187ef6e82 Mon Sep 17 00:00:00 2001
From: matoro <matoro@users.noreply.github.com>
Date: Tue, 7 May 2024 15:41:36 -0400
Subject: [PATCH] Add meson support for tests

Allows tests to use "meson test" instead of a shell script.  This also
means that they now fully respect user-specified toolchain (e.g. clang
compiler, custom CFLAGS, etc).  Running C++ test is conditional on
enabling INIReader support in the build.

```
 1/16 test_multi                               OK              0.05s
 2/16 test_multi_max_line                      OK              0.04s
 3/16 test_single                              OK              0.04s
 4/16 test_disallow_inline_comments            OK              0.03s
 5/16 test_stop_on_first_error                 OK              0.03s
 6/16 test_handler_lineno                      OK              0.02s
 7/16 test_heap                                OK              0.06s
 8/16 test_string                              OK              0.06s
 9/16 test_heap_max_line                       OK              0.05s
10/16 test_heap_realloc                        OK              0.05s
11/16 test_heap_realloc_max_line               OK              0.05s
12/16 test_heap_string                         OK              0.04s
13/16 test_call_handler_on_new_section         OK              0.04s
14/16 test_allow_no_value                      OK              0.03s
15/16 test_alloc                               OK              0.02s
16/16 test_INIReaderExample                    OK              0.02s

Ok:                 16
Expected Fail:      0
Fail:               0
Unexpected Pass:    0
Skipped:            0
Timeout:            0
```
---
 .github/workflows/tests.yml | 13 ++++++++++++-
 examples/meson.build        | 10 ++++++++++
 meson.build                 | 15 ++++++++++++---
 tests/meson.build           | 25 +++++++++++++++++++++++++
 tests/runtest.sh            |  6 ++++++
 5 files changed, 65 insertions(+), 4 deletions(-)
 create mode 100644 examples/meson.build
 create mode 100644 tests/meson.build
 create mode 100755 tests/runtest.sh

diff --git a/.github/workflows/tests.yml b/.github/workflows/tests.yml
index 4971e43..b34ab59 100644
--- a/.github/workflows/tests.yml
+++ b/.github/workflows/tests.yml
@@ -11,7 +11,7 @@ jobs:
     runs-on: ubuntu-latest
 
     steps:
-    - uses: actions/checkout@v2
+    - uses: actions/checkout@v4
 
     - name: Run Diff Tests
       run: |
@@ -20,3 +20,14 @@ jobs:
         cd ../examples
         ./cpptest.sh
         git diff --exit-code
+
+  build-meson:
+    runs-on: ubuntu-latest
+
+    steps:
+    - uses: actions/checkout@v4
+    - uses: actions/setup-python@v5
+    - uses: BSFishy/meson-build@v1.0.3
+      with:
+        action: test
+        meson-version: 1.4.1
diff --git a/examples/meson.build b/examples/meson.build
new file mode 100644
index 0000000..bb1979e
--- /dev/null
+++ b/examples/meson.build
@@ -0,0 +1,10 @@
+runtest = files(join_paths(meson.project_source_root(), 'tests', 'runtest.sh'))
+
+tests = {
+  'INIReaderExample': { 'args': [] },
+}
+
+foreach name, properties : tests
+  exe = executable('unittest_' + name, src_inih, src_INIReader, 'INIReaderExample.cpp', cpp_args : ['-Wall', properties['args']])
+  test('test_' + name, runtest, depends : [exe], args : [files('cpptest.txt'), exe.full_path()])
+endforeach
diff --git a/meson.build b/meson.build
index d440cbc..4e4fe4b 100644
--- a/meson.build
+++ b/meson.build
@@ -2,7 +2,8 @@ project('inih',
     ['c'],
     license : 'BSD-3-Clause',
     version : '58',
-    default_options : ['cpp_std=c++11']
+    default_options : ['cpp_std=c++11'],
+    meson_version: '>=0.56.0'
 )
 
 #### options ####
@@ -71,8 +72,10 @@ endif
 #### inih ####
 inc_inih = include_directories('.')
 
+src_inih = files('ini.c')
+
 lib_inih = library('inih',
-    ['ini.c'],
+    [src_inih],
     include_directories : inc_inih,
     c_args : [arg_static, extra_args],
     install : distro_install,
@@ -96,13 +99,17 @@ inih_dep = declare_dependency(
     include_directories : inc_inih
 )
 
+subdir('tests')
+
 #### INIReader ####
 if get_option('with_INIReader')
     add_languages('cpp')
     inc_INIReader = include_directories('cpp')
 
+    src_INIReader = files(join_paths('cpp', 'INIReader.cpp'))
+
     lib_INIReader = library('INIReader',
-        ['cpp/INIReader.cpp'],
+        src_INIReader,
         cpp_args : extra_args,
         include_directories : inc_INIReader,
         dependencies : inih_dep,
@@ -126,4 +133,6 @@ if get_option('with_INIReader')
         include_directories : inc_INIReader,
         compile_args : extra_args
     )
+
+    subdir('examples')
 endif
diff --git a/tests/meson.build b/tests/meson.build
new file mode 100644
index 0000000..d0a2c52
--- /dev/null
+++ b/tests/meson.build
@@ -0,0 +1,25 @@
+runtest = files(join_paths(meson.project_source_root(), 'tests', 'runtest.sh'))
+
+tests = {
+  'multi': { 'args': [] },
+  'multi_max_line': { 'args': ['-DINI_MAX_LINE=20'] },
+  'single': { 'args': ['-DINI_ALLOW_MULTILINE=0'] },
+  'disallow_inline_comments': { 'args': ['-DINI_ALLOW_INLINE_COMMENTS=0'] },
+  'stop_on_first_error': { 'args': ['-DINI_STOP_ON_FIRST_ERROR=1'] },
+  'handler_lineno': { 'args': ['-DINI_HANDLER_LINENO=1'] },
+  'string': { 'src': 'unittest_string.c', 'args': ['-DINI_MAX_LINE=20'] },
+  'heap': { 'args': ['-DINI_USE_STACK=0'] },
+  'heap_max_line': { 'args': ['-DINI_USE_STACK=0', '-DINI_MAX_LINE=20', '-DINI_INITIAL_ALLOC=20'] },
+  'heap_realloc': { 'args': ['-DINI_USE_STACK=0', '-DINI_ALLOW_REALLOC=1', '-DINI_INITIAL_ALLOC=5'] },
+  'heap_realloc_max_line': { 'args': ['-DINI_USE_STACK=0', '-DINI_MAX_LINE=20', '-DINI_ALLOW_REALLOC=1', '-DINI_INITIAL_ALLOC=5'] },
+  'heap_string': { 'src': 'unittest_string.c', 'args': ['-DINI_USE_STACK=0', '-DINI_MAX_LINE=20', '-DINI_INITIAL_ALLOC=20'] },
+  'call_handler_on_new_section': { 'args': ['-DINI_CALL_HANDLER_ON_NEW_SECTION=1'] },
+  'allow_no_value': { 'args': ['-DINI_ALLOW_NO_VALUE=1'] },
+  'alloc': { 'src': 'unittest_alloc.c', 'args': ['-DINI_CUSTOM_ALLOCATOR=1', '-DINI_USE_STACK=0', '-DINI_ALLOW_REALLOC=1', '-DINI_INITIAL_ALLOC=12'] }
+}
+
+foreach name, properties : tests
+  test_src = 'src' in properties ? properties['src'] : 'unittest.c'
+  exe = executable('unittest_' + name, src_inih, test_src, c_args : ['-Wall', properties['args']])
+  test('test_' + name, runtest, depends : [exe], args : [files('baseline_' + name + '.txt'), exe.full_path()])
+endforeach
diff --git a/tests/runtest.sh b/tests/runtest.sh
new file mode 100755
index 0000000..9db945b
--- /dev/null
+++ b/tests/runtest.sh
@@ -0,0 +1,6 @@
+#!/usr/bin/env bash
+
+set -euo pipefail
+
+cd "$(dirname "${1}")"
+diff "${1}" <("${2}")
