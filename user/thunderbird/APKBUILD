# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=thunderbird
pkgver=128.4.0
pkgrel=0
_llvmver=18
pkgdesc="Email client from Mozilla"
url="https://www.thunderbird.net/"
arch="all !ppc"  # #837
options="!check"  # Tests disabled
license="MPL-2.0"
depends=""
# moz build system stuff
# system-libs
# actual deps
makedepends="
	autoconf2.13 cargo cbindgen clang llvm${_llvmver}-dev node ncurses-dev
	perl python3 rust cmd:which

	alsa-lib-dev bzip2-dev icu-dev libevent-dev libffi-dev libpng-dev
	libjpeg-turbo-dev nspr-dev nss-dev pulseaudio-dev zlib-dev

	dbus-glib-dev fts-dev gconf-dev gtk+3.0-dev hunspell-dev
	libnotify-dev libsm-dev libxcomposite-dev libxdamage-dev
	libxrender-dev libxt-dev nasm nss-static sqlite-dev
	startup-notification-dev unzip yasm zip gtk+2.0-dev
	"
_tbver="$pkgver"'esr'
source="https://archive.mozilla.org/pub/thunderbird/releases/$_tbver/source/thunderbird-$_tbver.source.tar.xz
	mozconfig

	bad-google-code.patch
	fix-mutex-build.patch
	fix-seccomp-bpf.patch
	icu-75.patch
	jpeg-link.patch
	js-endian.patch
	mozilla-build-arm.patch
	pmmx-double-format.patch
	ppc32-fix.patch
	rust-32bit.patch
	shut-up-warning.patch
	skia-endian.patch
	skia-unified.patch
	stackwalk-x86-ppc.patch
	webrender.patch
	without-jit.patch

	thunderbird.desktop
	"
somask="liblgpllibs.so
	libmozgtk.so
	libmozsandbox.so
	libmozsqlite3.so
	libmozwayland.so
	libxul.so
	"
_mozappdir=/usr/lib/thunderbird
ldpath="$_mozappdir"

# secfixes:
#   68.6.0-r0:
#     - CVE-2019-20503
#     - CVE-2020-6805
#     - CVE-2020-6806
#     - CVE-2020-6807
#     - CVE-2020-6811
#     - CVE-2020-6812
#     - CVE-2020-6814
#   68.7.0-r0:
#     - CVE-2020-6819
#     - CVE-2020-6820
#     - CVE-2020-6821
#     - CVE-2020-6822
#     - CVE-2020-6825
#   68.9.0-r0:
#     - CVE-2020-6831
#     - CVE-2020-12387
#     - CVE-2020-12392
#     - CVE-2020-12395
#     - CVE-2020-12397

prepare() {
	default_prepare
	cp "$srcdir"/mozconfig "$builddir"/mozconfig
	echo "ac_add_options --host=\"$CHOST\"" >> "$builddir"/mozconfig
	echo "ac_add_options --target=\"$CTARGET\"" >> "$builddir"/mozconfig
	echo "mk_add_options MOZ_MAKE_FLAGS=\"-j$JOBS\"" >> "$builddir"/mozconfig

	# arch-specific configuration
	case "$CARCH" in
		pmmx|x86*|arm*|aarch64)
			echo "ac_add_options --disable-elf-hack" >> "$builddir"/mozconfig
			;;
		s390x)
			echo "ac_add_options --disable-startupcache" >> "$builddir"/mozconfig
			;;
	esac

	# 32-bit memory ceiling, #1012, #1057
	case "${CARCH}" in
		pmmx|x86|ppc|i528|m68k|mips32*|arm*)
			echo "ac_add_options --disable-debug-symbols" >> "$builddir"/mozconfig
			echo "ac_add_options --enable-strip" >> "$builddir"/mozconfig
			CFLAGS="${CFLAGS} -g0";
			;;
	esac

	echo "ac_add_options --enable-optimize=\"$CFLAGS\"" >> "$builddir"/mozconfig
}

build() {
	export CFLAGS="$CFLAGS -Wno-dangling-pointer -Wno-array-bounds -Wno-comment -Wno-maybe-uninitialized -Wno-misleading-indentation"
	export CXXFLAGS="$CFLAGS -Wno-class-memaccess -Wno-changes-meaning"
	export PATH="/usr/lib/llvm${_llvmver}/bin:${PATH}" #1230
	export SHELL=/bin/sh
	export BUILD_OFFICIAL=1
	export MOZILLA_OFFICIAL=1
	export USE_SHORT_LIBNAME=1
	export MACH_BUILD_PYTHON_NATIVE_PACKAGE_SOURCE=system

	# set rpath so linker finds the libs
	export LDFLAGS="$LDFLAGS -Wl,-rpath,${_mozappdir}"

	export UNIXCONFDIR="$srcdir"

	./mach build
}

run() {
	cd "$builddir"/obj-$CHOST/dist/bin
	export LD_LIBRARY_PATH=.
	export PROFILE_DIR="$builddir"/obj-$CHOST/tmp/profile-default
	[ -d $PROFILE_DIR ] || ./thunderbird -no-remote -CreateProfile "Test $PROFILE_DIR"
	./thunderbird -no-remote -profile $PROFILE_DIR
}

package() {
	export MACH_BUILD_PYTHON_NATIVE_PACKAGE_SOURCE=system
	DESTDIR="$pkgdir" ./mach install

	install -m755 -d ${pkgdir}/usr/share/applications
	install -m755 -d ${pkgdir}/usr/share/pixmaps

	for png in comm/mail/branding/thunderbird/default*.png; do
		local i="${_png%.png}"
		i=${i##*/default}
		install -D -m644 "$png" \
			"$pkgdir"/usr/share/icons/hicolor/${i}x${i}/apps/thunderbird.png
	done

	install -m644 "$builddir"/comm/mail/branding/thunderbird/default256.png \
		${pkgdir}/usr/share/pixmaps/thunderbird.png
	install -m644 ${startdir}/thunderbird.desktop \
		${pkgdir}/usr/share/applications/thunderbird.desktop
}

sha512sums="ad031b3a9b738598358cead23cf8438435016222cd9a474c31892dc1b3db43d2d5d3a10c9639df770dc76eb3c0bc9db8be8beab84828d54ee50fc1e03f0da0a5  thunderbird-128.4.0esr.source.tar.xz
fec4e343db973f90a38cc86e145c8ab0c8bec8f6eed36ff47f9986b154ea44f0e72ea1b5c9862e772505b3371dc0a9265fa1a6bed59007a5845a1dc622fce4e8  mozconfig
1fff71be77aa34791ef8aa972b5dc5f131d79f4b895ebbfb59bc223078764d6f79c82a3a4e4c717a3893ecbe4907d556f27e97d2692e41f02365f06da759a71a  bad-google-code.patch
5e6c3fa027052cc43ba161cf5c12016b07723e7c0b002a42431bbd5d716d512d5245ab6befbb56c37c7d244441533f810f4e7b583f83bbe965981931f1435a94  fix-mutex-build.patch
9f2892eff9d09f3eed1f9a9ee98ba9c6a6826c30ccdeabfefb8733abc98e7b612418827262423568e830c6354a2b8242f471bc200b7079f21862bb00947da716  fix-seccomp-bpf.patch
0a84e8cb6604fdb7601745b60c04ee74733632ada32bfd85eb32db8fee095cfb79c82033e5e3cf3bbc7b7f4df753e86ffe18e26e63a6603a93c3bb5a459aabd3  icu-75.patch
a2400c8df888637d9ebe33d82dc2fb95792685c9938b5486230a5c15bc819cbb850a2bdcb54a775516652fc8ffd6b40f8c591ab7941211f9516064101380ebbf  jpeg-link.patch
d40e6c1a6e62d5e1dc350dc0bbaf2b8488b19ea0911532784f1df1199a7a5af0aae81b0b0d77c4398a2d903d86b5560f4442386eb446d9af8d58e73eea475e4f  js-endian.patch
9f5730ee3a29b86786089abcfffab32bf2210f1a1ec43260ae824a08acda687dbfa37306b82a4978dd022bcf747eab155f7f3c5f73a1375f1c3c4fcd106dae3e  mozilla-build-arm.patch
1864c6c63c647f3ed1b3ca4df7f52a2c7ec4c65f2e067a9842d24a242f01e047d59ffbf88ad3740d830df1eb0bade929e245e640db135fe14f4c0d879269cb7e  pmmx-double-format.patch
06a3f4ee6d3726adf3460952fcbaaf24bb15ef8d15b3357fdd1766c7a62b00bd53a1e943b5df7f4e1a69f4fae0d44b64fae1e027d7812499c77894975969ea10  ppc32-fix.patch
e1c8a85c46a9cc804eb6b5946d35b63fa2cfc7ad8b4f250c5eaf1b590b139a66d9a833246bc26ae2e340fffb81b6761cf51c600181895bd49feb9a3b8fbfe331  rust-32bit.patch
c2248297df00633771da7e6b9f2caba3dd5108d93617f652fa8b2fb4e6975821e5fee866c00713f8fce08e760fa456e2dd19a8ef7370e5a2c65dc09614526be9  shut-up-warning.patch
11f7ce86376b54281082fa5b9d3fefee8046eabe38a0ec17266764b4f62cb90f4744245c1ca93d73ed74cf81c066dab7ddcb3fe9704be8f91e0a2a4f664b203a  skia-endian.patch
a6a14104ee6985867efb1937a262aba1538ed4b069bba56e400d75f51d84d29363d8bbb2183ee27785758ad3a8b8ae4350e23dc854d50c92cb2147eddf83b9bb  skia-unified.patch
9df89366bd51670f534591e4c169186703ad3166a493f522e58f129fb79af375d71baaf0f6b238126792118a2d3ed0f011b010e39497ab5901ee5bea63a84fa2  stackwalk-x86-ppc.patch
e1d9e30a1c8d1c6bdd3530183400d7d245e437ce18bb5b262bec1d01932b02b8eaededd892efcdd277eaccb357aede4537972382be9f00eedf1785acebbd1ca6  webrender.patch
ed78e15030d79b10026ebae26e44a68d16c2a8567082072b5099b0dde4f79ae217a21737e0d9c7ec6a3cc99de3ab3c9083cca7d82889d559bb18cc15fa7e6f2a  without-jit.patch
95a2b1deb4f6c90750fdd2bfe8ca0a7879a5b267965091705a6beb0a0a4b1ccad75d11df7b9885543ca4232ff704e975c6946f4c11804cb71c471e06f9576001  thunderbird.desktop"
