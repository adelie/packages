# Contributor: Ermine <mustela@erminea.space>
# Contributor: Ariadne Conill <ariadne@dereferenced.org>
# Maintainer: Ermine <mustela@erminea.space>
pkgname=ifupdown-ng
pkgver=0.12.1
pkgrel=0
pkgdesc="Tools for managing network configuration"
url="https://github.com/ifupdown-ng/ifupdown-ng"
arch="all"
license="ISC AND BSD-2-Clause"
depends="kyua atf"
makedepends="scdoc"
subpackages="$pkgname-doc $pkgname-openrc $pkgname-iproute2 $pkgname-ppp
	$pkgname-wireguard $pkgname-ethtool $pkgname-wifi"
source="https://distfiles.ariadne.space/ifupdown-ng/ifupdown-ng-$pkgver.tar.xz
	ifupdown-ng.initd
	"

# We only install the core executors right now.  The other executors are
# still being ported to run under ifupdown-ng natively, and so we will
# use the ifupdown scripts for now for those cases.  However, it is nice
# to make the automatic dependency resolution work, and ifupdown-ng
# provides some stubs to enable that.
_executor_stubs="bridge bond"

# These are optional executors which do not conflict with what we are
# presently shipping.  These executors require iproute2.
_executor_iproute2="vrf gre vxlan mpls"

# These are all optional executors we plan to use.
_executor_opt="ppp wireguard wireguard-quick ethtool wifi $_executor_iproute2"

build() {
	make
	make docs
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" EXECUTOR_SCRIPTS_OPT="$_executor_opt" \
		EXECUTOR_SCRIPTS_STUB="$_executor_stubs" install install_docs
	install -Dm755 "$srcdir"/ifupdown-ng.initd "$pkgdir"/etc/init.d/ifupdown-ng
}

iproute2() {
	pkgdesc="ifupdown-ng integration for iproute2"
	depends="iproute2"
	install_if="$pkgname=$pkgver-r$pkgrel $depends"

	mkdir -p "$subpkgdir"/usr/libexec/ifupdown-ng
	for exec in $_executor_iproute2; do
		mv "$pkgdir"/usr/libexec/ifupdown-ng/$exec \
			"$subpkgdir"/usr/libexec/ifupdown-ng/$exec
	done
}

ppp() {
	pkgdesc="ifupdown-ng integration for ppp"
	depends="ppp"
	install_if="$pkgname=$pkgver-r$pkgrel $depends"

	mkdir -p "$subpkgdir"/usr/libexec/ifupdown-ng
	mv "$pkgdir"/usr/libexec/ifupdown-ng/ppp \
		"$subpkgdir"/usr/libexec/ifupdown-ng/ppp
}

wireguard() {
	pkgdesc="ifupdown-ng integration for wireguard"
	depends="wireguard-tools"
	install_if="$pkgname=$pkgver-r$pkgrel $depends"

	mkdir -p "$subpkgdir"/usr/libexec/ifupdown-ng
	mv "$pkgdir"/usr/libexec/ifupdown-ng/wireguard \
		"$subpkgdir"/usr/libexec/ifupdown-ng/wireguard
	mv "$pkgdir"/usr/libexec/ifupdown-ng/wireguard-quick \
		"$subpkgdir"/usr/libexec/ifupdown-ng/wireguard-quick
}

ethtool() {
	pkgdesc="ifupdown-ng integration for ethtool"
	depends="ethtool"
	install_if="$pkgname=$pkgver-r$pkgrel $depends"

	mkdir -p "$subpkgdir"/usr/libexec/ifupdown-ng
	mv "$pkgdir"/usr/libexec/ifupdown-ng/ethtool \
		"$subpkgdir"/usr/libexec/ifupdown-ng/ethtool
}

wifi() {
	pkgdesc="ifupdown-ng integration for wifi"
	depends="wpa_supplicant"
	install_if="$pkgname=$pkgver-r$pkgrel $depends"

	mkdir -p "$subpkgdir"/usr/libexec/ifupdown-ng
	mv "$pkgdir"/usr/libexec/ifupdown-ng/wifi \
		"$subpkgdir"/usr/libexec/ifupdown-ng/wifi
}

doc() {
	default_doc

	mkdir -p "$subpkgdir"/etc/network
	mv "$pkgdir"/etc/network/ifupdown-ng.conf.example \
		"$subpkgdir"/etc/network/ifupdown-ng.conf.example
}
sha512sums="c2c1e27fbd53eeb4150e544a893352f13845b491850e982a5880160c3be03b628794f7989c802ff823ac3e8c6c34dc4ab97c0abcefdda85f06fcd64ced8ac60c  ifupdown-ng-0.12.1.tar.xz
aa1dd3d31872d7ec308b534044e490136de47a3e72575a96522f60c5740ee3b953a17e05013359d27985ac16b048ff8fff76e8ac1103802a4fb75a31d2185d45  ifupdown-ng.initd"
