# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libdrm
pkgver=2.4.123
pkgrel=0
pkgdesc="Userspace interface to kernel DRM services"
url="https://dri.freedesktop.org/"
arch="all"
# Requires a computer with gfx, no X running, build user in 'video' group..
options="!check"
license="MIT"
depends=""
depends_dev="linux-headers"
checkdepends="bash cunit-dev"
makedepends="$depends_dev bash cairo-dev eudev-dev libpciaccess-dev libpthread-stubs
	libatomic_ops-dev meson ninja py3-docutils xmlto"
subpackages="$pkgname-dev $pkgname-doc"
source="https://dri.freedesktop.org/$pkgname/$pkgname-$pkgver.tar.xz"

build() {
	case $CARCH in
	arm*|aarch64*) _arch_conf="
		-Domap=enabled
		-Dexynos=enabled
		-Detnaviv=enabled
		-Dtegra=enabled
		" ;;
	esac

	meson \
		-Dprefix=/usr \
		-Dman-pages=enabled \
		-Dudev=true \
		-Dvalgrind=disabled \
		$_arch_conf \
		build
	ninja -C build
}

check() {
	ninja -C build test
}

package() {
	DESTDIR="$pkgdir" ninja -C build install
}

sha512sums="d3e6ba26bbd609fd87ca131690547eeb6a903c0a8c28b7f5cd5d0947619da09f31daf7bf4b6c38bf5e5dc173e2ccba476338ef682d8cf06d6b71ba73fc9b948d  libdrm-2.4.123.tar.xz"
