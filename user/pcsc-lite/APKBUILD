# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Contributor: Timo Teräs <timo.teras@iki.fi>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=pcsc-lite
pkgver=1.9.5
pkgrel=0
pkgdesc="Middleware to access a smart card using SCard API (PC/SC)"
url="https://pcsclite.apdu.fr/"
arch="all"
license="BSD-3-Clause AND GPL-3.0+ AND BSD-2-Clause AND MIT"
depends=""
depends_dev="eudev-dev"
makedepends="$depends_dev perl perl-dev libcap-ng-utils polkit-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs $pkgname-openrc"
install="pcsc-lite.pre-install pcsc-lite.pre-upgrade"
source="https://pcsclite.apdu.fr/files/pcsc-lite-$pkgver.tar.bz2
	pcscd.initd"

build() {
	./configure --prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-libusb \
		--enable-libudev \
		--disable-maintainer-mode \
		--disable-silent-rules \
		--without-systemdsystemunitdir \
		--enable-ipcdir=/run/pcscd \
		--enable-usbdropdir=/usr/lib/pcsc/drivers \
		--disable-libsystemd \
		--enable-polkit
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
        install -D -m755 "$srcdir"/pcscd.initd \
		"$pkgdir"/etc/init.d/pcscd
	mkdir -p "$pkgdir"/usr/lib/pcsc/drivers

	# grant sysfs access
	filecap "$pkgdir"/usr/sbin/pcscd dac_override
}

libs() {
	pkgdesc="Middleware to access a smart card using SCard API (PC/SC) (libraries)"
	mkdir -p "$subpkgdir"/usr
	mv "$pkgdir"/usr/lib "$subpkgdir"/usr
}

dev() {
	default_dev
	# move back the /usr/lib/libpcsclite.so
	# see http://bugs.alpinelinux.org/issues/3236 (and 6392)
	mv "$subpkgdir"/usr/lib/libpcsclite.so "$pkgdir"/usr/lib/libpcsclite.so
}

sha512sums="0315c2cf97cc9da0f5faf115f24e523b5a1746cea250a4fe6c4d5d7b2fbfc7c3ea0f068611072ca84866c672eb679e8067101437573148ccd1ac5ad26b18cd78  pcsc-lite-1.9.5.tar.bz2
6810ab357b5bcf07bf1ad76ed17ebb8e92be4599303a4e697b87a05d43b8d2bec5d3c29d44d8ddb5d031910ea62ca52a9868e6de9a255227eaeb29d1a7ee0041  pcscd.initd"
