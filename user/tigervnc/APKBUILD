# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=tigervnc
pkgver=1.13.1
pkgrel=0
pkgdesc="High-performance, platform-neutral VNC remote desktop application"
url="https://tigervnc.org/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0"
depends=""
makedepends="cmake fltk-dev fontconfig-dev gnutls-dev intltool
	libjpeg-turbo-dev libx11-dev libxcursor-dev libxfixes-dev libxft-dev
	libxrender-dev libxtst-dev linux-pam-dev pixman-dev zlib-dev"
subpackages="$pkgname-lang $pkgname-doc"
source="tigervnc-$pkgver.tar.gz::https://github.com/TigerVNC/tigervnc/archive/v$pkgver.tar.gz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS -faligned-new -Wno-c++11-compat -Wno-maybe-uninitialized" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-Wno-dev \
		${CMAKE_CROSSOPTS} .
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="9190dbcd3b57ba52286c158c0675104d68463d7e3ea8e23493514b64451ddb511f3daf0f177339bc231155daea376d9c8dc58216663e10aa12f67468f4559da5  tigervnc-1.13.1.tar.gz"
