# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=kde-gtk-config
pkgver=5.24.5
pkgrel=0
pkgdesc="KDE System Settings panel for configuring GTK+ application styles"
url="https://www.kde.org/"
arch="all"
license="GPL-3.0+"
depends="gsettings-desktop-schemas xsettingsd"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev gtk+2.0-dev gtk+3.0-dev
	kauth-dev kcodecs-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kdbusaddons-dev kdecoration-dev kguiaddons-dev ki18n-dev qt5-qtsvg-dev
	kwidgetsaddons-dev gsettings-desktop-schemas-dev sassc"
subpackages=""
source="https://download.kde.org/stable/plasma/$pkgver/kde-gtk-config-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="0cc7a76aca458ec093b55ea148637a82fa53467f3944d4192f4138f7eb93c88a2a65cd64e7438814f11182a390d39629982850b6939902738fd1d2a2af65319d  kde-gtk-config-5.24.5.tar.xz"
