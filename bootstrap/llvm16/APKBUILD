# Contributor: Travis Tilley <ttilley@gmail.com>
# Contributor: Mitch Tishmack <mitch.tishmack@gmail.com>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
_pkgname=llvm
pkgver=16.0.6
_majorver=${pkgver%%.*}
pkgname=$_pkgname$_majorver
pkgrel=0
pkgdesc="Low Level Virtual Machine compiler system, version $_majorver"
url="https://llvm.org/"
arch="all"
options="!checkroot !dbg"
license="NCSA AND (Apache-2.0 WITH LLVM-exception)"
depends=""
depends_dev="$pkgname=$pkgver-r$pkgrel libexecinfo-dev libxml2-dev"
checkdepends="cmd:which"
makedepends="binutils-dev chelf chrpath cmake file libexecinfo-dev libffi-dev
	libxml2-dev python3 zlib-dev"
subpackages="$pkgname-static $pkgname-libs $pkgname-dev
	$pkgname-test-utils:_test_utils"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/llvm-project-$pkgver.src.tar.xz
	llvm-fix-build-with-musl-libc.patch
	disable-FileSystemTest.CreateDir-perms-assert.patch
	dyld-elf-ppc32.patch
	exegesis-mcjit.patch
	graph-diff-simple.txt
	hexagon.patch
	macho32.patch
	musl-ppc64-elfv2.patch
	ppc-gcc-bug.patch
	python3-test.patch
	roundeven.patch
	"
builddir="$srcdir/$_pkgname-project-$pkgver.src/llvm"

# ARM has few failures in test suite that we don't care about currently and
# also it takes forever to run them on the builder.
case "$CARCH" in
	arm*) options="$options !check";;
esac

# Whether is this package the default (latest) LLVM version.
_default_llvm="no"

if [ "$_default_llvm" = yes ]; then
	provides="llvm=$pkgver-r$pkgrel"
	replaces="llvm"
fi

# NOTE: It seems that there's no (sane) way how to change includedir, sharedir
# etc. separately, just the CMAKE_INSTALL_PREFIX. Standard CMake variables and
# even  LLVM-specific variables, that are related to these paths, actually
# don't work (in llvm 3.7).
#
# When building a software that depends on LLVM, utility llvm-config should be
# used to discover where is LLVM installed. It provides options to print
# path of bindir, includedir, and libdir separately, but in its source, all
# these paths are actually hard-coded against INSTALL_PREFIX. We can patch it
# and move paths manually, but I'm really not sure what it may break...
#
# Also note that we should *not* add version suffix to files in llvm bindir!
# It breaks build system of some software that depends on LLVM, because they
# don't expect these files to have a sufix.
#
# So, we install all the LLVM files into /usr/lib/llvm$_majorver.
# BTW, Fedora and Debian do the same thing.
#
_prefix="usr/lib/llvm$_majorver"

prepare() {
	default_prepare

	# https://bugs.llvm.org//show_bug.cgi?id=31870
	rm test/tools/llvm-symbolizer/print_context.c

	case $CARCH in
	pmmx|x86|ppc|armhf|armv7)
		# Appears to not work when building 32-bit code on 64-bit host.
		rm test/tools/llvm-size/radix.test;;
	esac

	case $CARCH in
	ppc)
		# unknown issue, only used for bootstrapping (#1244)
		for _failing_test in prettyprint_types.s \
					simplified-template-names.s; do
			rm test/tools/llvm-dwarfdump/X86/$_failing_test;
		done;;
	esac

	# appears to be an issue on musl and glibc, but only fails on musl:
	# https://github.com/NixOS/nixpkgs/blob/bb7e9e46/pkgs/development/compilers/llvm/8/llvm.nix#L74
	rm -f test/CodeGen/AArch64/wineh4.mir

	#1053
	rm -fr \
		test/tools/UpdateTestChecks \
		test/ExecutionEngine/MCJIT/load-object-a.ll \
		;
	#1202
	case $CARCH in
	pmmx)
		mv "$srcdir"/graph-diff-simple.txt \
		test/tools/llvm-xray/X86/graph-diff-simple.txt ;;
	esac
}

build() {
	# Auto-detect it by guessing either.
	ffi_include_dir="$(pkg-config --cflags-only-I libffi | sed 's|^-I||g')"

	case $CTARGET_ARCH in
	pmmx)
		# The standard precision breaks more than it fixes; there are
		# 4 failures in standard mode and only (different) 1 in fast.
		export CXXFLAGS="$CXXFLAGS -fexcess-precision=fast";;
	esac

	# Disabled m68k because the endianness fix from llvm18 doesn't work.
	cmake -G "Unix Makefiles" -Wno-dev \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/$_prefix \
		-DCMAKE_VERBOSE_MAKEFILE=NO \
		-DFFI_INCLUDE_DIR="$ffi_include_dir" \
		-DLLVM_BINUTILS_INCDIR=/usr/include \
		-DLLVM_BUILD_DOCS=OFF \
		-DLLVM_BUILD_EXAMPLES=OFF \
		-DLLVM_BUILD_EXTERNAL_COMPILER_RT=ON \
		-DLLVM_BUILD_LLVM_DYLIB=ON \
		-DLLVM_BUILD_TESTS=ON \
		-DLLVM_DEFAULT_TARGET_TRIPLE="$CBUILD" \
		-DLLVM_ENABLE_ASSERTIONS=OFF \
		-DLLVM_ENABLE_FFI=ON \
		-DLLVM_ENABLE_LIBCXX=OFF \
		-DLLVM_ENABLE_PIC=ON \
		-DLLVM_ENABLE_RTTI=ON \
		-DLLVM_ENABLE_SPHINX=OFF \
		-DLLVM_ENABLE_TERMINFO=ON \
		-DLLVM_ENABLE_ZLIB=ON \
		-DLLVM_HOST_TRIPLE="$CHOST" \
		-DLLVM_INCLUDE_BENCHMARKS=OFF \
		-DLLVM_INCLUDE_EXAMPLES=OFF \
		-DLLVM_INSTALL_UTILS=ON \
		-DLLVM_LINK_LLVM_DYLIB=ON \
		-DLLVM_TARGETS_TO_BUILD='AArch64;AMDGPU;ARM;BPF;Hexagon;Lanai;Mips;MSP430;PowerPC;RISCV;Sparc;SystemZ;WebAssembly;X86;XCore' \
		-Bbuild .

	make -C build llvm-tblgen
	make -C build

	python3 utils/lit/setup.py build
}

check() {
	make -C build check-llvm
}

package() {
	make DESTDIR="$pkgdir" -C build install

	cd "$pkgdir"/$_prefix

	# Remove RPATHs.
	file lib/*.so bin/* \
		| awk -F: '$2~/ELF/{print $1}' \
		| xargs -r chrpath -d

	# Thread 3 requires a lot of stack space to LTO when targeting ARM.
	# Note that this occurs even when crossing (on a ppc64 host).
	chelf -s 1048576 bin/llvm-lto2

	# Symlink files from /usr/lib/llvm*/bin to /usr/bin.
	mkdir -p "$pkgdir"/usr/bin
	for full_name in bin/*; do
		bin_name=${full_name##*/}
		# If this package provides=llvm (i.e. it's the default/latest
		# llvm package), omit version infix/suffix.
		if [ "$_default_llvm" = yes ]; then
			link_name=$bin_name
		# ..otherwise, add version infix/suffix to the executable name.
		else case "$bin_name" in
			llvm-*) link_name="llvm$_majorver-${bin_name#llvm-}";;
			*) link_name="$bin_name$_majorver";;
		esac
		fi
		case "$name" in
			FileCheck | obj2yaml | yaml2obj) continue;;
		esac
		ln -s ../lib/llvm$_majorver/bin/$bin_name "$pkgdir"/usr/bin/$link_name
	done

	# Move /usr/lib/$pkgname/include/ into /usr/include/$pkgname/
	# and symlink it back.
	mkdir -p "$pkgdir"/usr/include/$pkgname
	mv include/* "$pkgdir"/usr/include/$pkgname/
	rmdir include
	ln -s ../../include/$pkgname include

	ln -s "$pkgdir"/usr/lib/cmake/llvm ../$pkgname/lib/cmake/llvm
}

static() {
	pkgdesc="LLVM $_majorver static libraries"
	_common_subpkg

	mkdir -p "$subpkgdir"/$_prefix/lib
	mv "$pkgdir"/$_prefix/lib/*.a "$subpkgdir"/$_prefix/lib/
	strip -d "$subpkgdir"/$_prefix/lib/*.a
}

libs() {
	pkgdesc="LLVM $_majorver runtime library"
	main_soname="libLLVM-$_majorver.so"
	ver_soname="libLLVM-$pkgver.so"
	_common_subpkg

	# libLLVM should be in /usr/lib. This is needed for binaries that are
	# dynamically linked with libLLVM, so they can find it on default path.
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/$_prefix/lib/$main_soname "$subpkgdir"/usr/lib/
	ln -s $main_soname "$subpkgdir"/usr/lib/$ver_soname
	strip "$subpkgdir"/usr/lib/$ver_soname #797

	# And also symlink it back to the LLVM prefix.
	mkdir -p "$subpkgdir"/$_prefix/lib
	ln -s ../../$main_soname "$subpkgdir"/$_prefix/lib/$main_soname
	ln -s ../../$main_soname "$subpkgdir"/$_prefix/lib/$ver_soname
}

dev() {
	_common_subpkg
	default_dev

	mkdir -p "$subpkgdir"/$_prefix/bin

	mv "$pkgdir"/$_prefix/lib "$subpkgdir"/$_prefix/
	mv "$pkgdir"/$_prefix/include "$subpkgdir"/$_prefix/

	mv "$pkgdir"/$_prefix/bin/llvm-config "$subpkgdir"/$_prefix/bin/
}

_test_utils() {
	pkgdesc="LLVM $_majorver utilities for executing LLVM and Clang style test suites"
	depends="python3"
	_common_subpkg
	replaces=""

	litver="$(python3 "$builddir"/utils/lit/setup.py --version 2>/dev/null \
		| sed 's/\.dev.*$//')"
	test -n "$litver" || return 1
	provides="$provides lit=$litver-r$pkgrel"

	cd "$builddir"/build

	install -D -m 755 bin/FileCheck "$subpkgdir"/$_prefix/bin/FileCheck
	install -D -m 755 bin/count "$subpkgdir"/$_prefix/bin/count
	install -D -m 755 bin/not "$subpkgdir"/$_prefix/bin/not

	python3 ../utils/lit/setup.py install --prefix=/usr --root="$subpkgdir"
	ln -s ../../../bin/lit "$subpkgdir"/$_prefix/bin/lit
}

_common_subpkg() {
	if [ "$_default_llvm" = yes ]; then
		replaces="llvm${subpkgname#$pkgname}"
		provides="$replaces=$pkgver-r$pkgrel"
	fi
}

sha512sums="89a67ebfbbc764cc456e8825ecfa90707741f8835b1b2adffae0b227ab1fe5ca9cce75b0efaffc9ca8431cae528dc54fd838867a56a2b645344d9e82d19ab1b7  llvm-project-16.0.6.src.tar.xz
e6fec46f3a1183774e4a47b81f3287adc5aa28ebdd52f614c1111fb157cd3137d1b86e04ca76a92b192c900eb5fa4c44f0c00984da14f0a9bb3ee8e488773e00  llvm-fix-build-with-musl-libc.patch
8e694af50a9f1f8a3dcf7c0e1ce1165237011767e3514271984877882962e492057efd16f0e6e303ee534a48c039c8ec00ed8a34eb9e9826b0324100292695f9  disable-FileSystemTest.CreateDir-perms-assert.patch
962ea62f96927f397a234c8770fd5a41d5c8ef528de56da6da76908369ee0e9faca2fcba46b3957a928b8138b46c7c0d8c5f353088009e1921f9c3cfffd96081  dyld-elf-ppc32.patch
5f24f99e31b8e0d1d7a7253be028df55f3524ff600c037404a4870ed22c02467f378b222d7fdd3d7af5b1ef006a90ec0c32c44122771e2ea8763eb62dcfca9dc  exegesis-mcjit.patch
a1533f4b9d35e1069fd065fbeb5fedce96fa49fdb007f994d8921d8a04cf2caadca276bbd9553ff86040b0f9dfce489c385fa2a7434e2dee3732942e6914db4e  graph-diff-simple.txt
807587a0c897d47a01a846c5c4f7cbf1f2d16437a163b66ee2381a7147e9d04b0141b2c76d022db61903d0d2841ddb267ba98c66c9e41501ca41837659743df8  hexagon.patch
de9791cc476817782b553c4edab696132da4ed2b9a3d80dbf1b85b7dc7a8c5e4caf14f2f26e33c27cd80a91b36709d3b24caea910c0a6315ffbb297cb748468d  macho32.patch
bec1fe4761dae33a8012a54b7bbdd179cac82febcd46ace1844772c06f207661c903f05fcb7a5ebbbece3e33a88b20a5d9b31b40e8a8cd5c626aec04cb6735be  musl-ppc64-elfv2.patch
bda83678acb97be4d4236179792b3555b8000242fa6bd9e2e78dcd4f6182395df7fa9b8742fde946f19b668615ab37db8670340d43683787d36c540937bca523  ppc-gcc-bug.patch
89ef5fbab039b017a5652656adf17b680525f1c5dd0b1afc8034fe0b34a2ca196d87640f54cc826356cfcd88c6e5f10754b7e38f04ca49ede4f3864080b787bd  python3-test.patch
5e0868a96ecd3344a4badb3c23e7e9060a96e3fc7cb4a092febca93eff8c040839041c7a3c8a29955541b585081f84ab8c1ced26198ac93ad2d9db616faf1088  roundeven.patch"
