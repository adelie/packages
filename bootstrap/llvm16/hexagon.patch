From 37605662a99bd0815e2f2e452eb6ab1ce53ecffd Mon Sep 17 00:00:00 2001
From: "A. Wilcox" <AWilcox@Wilcox-Tech.com>
Date: Mon, 30 May 2022 02:51:34 -0500
Subject: [PATCH] [Hexagon][Tests] Fix tests on Linux/musl

When running on a host system using musl, the target triple is defined
as hexagon-unknown-linux-musl by default.  The Linux ABI differs from
the non-Linux one with varargs, so this causes the tests to fail.

Closes BZ49592, PR48936.

Signed-off-by: A. Wilcox <AWilcox@Wilcox-Tech.com>
---
 test/CodeGen/Hexagon/csr-stubs-spill-threshold.ll | 2 +-
 test/CodeGen/Hexagon/long-calls.ll                | 2 +-
 test/CodeGen/Hexagon/mlong-calls.ll               | 2 +-
 test/CodeGen/Hexagon/pic-regusage.ll              | 2 +-
 test/CodeGen/Hexagon/runtime-stkchk.ll            | 2 +-
 test/CodeGen/Hexagon/swp-memrefs-epilog.ll        | 2 +-
 test/CodeGen/Hexagon/vararg-formal.ll             | 2 +-
 7 files changed, 7 insertions(+), 7 deletions(-)

diff --git a/test/CodeGen/Hexagon/csr-stubs-spill-threshold.ll b/test/CodeGen/Hexagon/csr-stubs-spill-threshold.ll
index afbef217911a..d317d7eac800 100644
--- a/test/CodeGen/Hexagon/csr-stubs-spill-threshold.ll
+++ b/test/CodeGen/Hexagon/csr-stubs-spill-threshold.ll
@@ -1,4 +1,4 @@
-; RUN: llc -mtriple=hexagon  -O2 -spill-func-threshold=2 < %s | FileCheck %s
+; RUN: llc -march=hexagon -mtriple=hexagon-unknown-linux-gnu -O2 -spill-func-threshold=2 < %s | FileCheck %s
 
 declare i32 @f0(i32, i32, i32, i32, i32, i32)
 
diff --git a/test/CodeGen/Hexagon/long-calls.ll b/test/CodeGen/Hexagon/long-calls.ll
index 628362783c9c..886405a2d91a 100644
--- a/test/CodeGen/Hexagon/long-calls.ll
+++ b/test/CodeGen/Hexagon/long-calls.ll
@@ -1,4 +1,4 @@
-; RUN: llc -mtriple=hexagon -enable-save-restore-long -hexagon-initial-cfg-cleanup=0 < %s | FileCheck %s
+; RUN: llc -march=hexagon -mtriple=hexagon-unknown-linux-gnu -enable-save-restore-long -hexagon-initial-cfg-cleanup=0 < %s | FileCheck %s
 
 ; Check that the -long-calls feature is supported by the backend.
 
diff --git a/test/CodeGen/Hexagon/mlong-calls.ll b/test/CodeGen/Hexagon/mlong-calls.ll
index d76b87f987fe..383486dfe63d 100644
--- a/test/CodeGen/Hexagon/mlong-calls.ll
+++ b/test/CodeGen/Hexagon/mlong-calls.ll
@@ -1,4 +1,4 @@
-; RUN: llc -hexagon-long-calls -mtriple=hexagon -enable-save-restore-long=true < %s | FileCheck %s
+; RUN: llc -hexagon-long-calls -march=hexagon -mtriple=hexagon-unknown-linux-gnu -enable-save-restore-long=true < %s | FileCheck %s
 
 ; CHECK: call ##f1
 ; CHECK: jump ##__restore
diff --git a/test/CodeGen/Hexagon/pic-regusage.ll b/test/CodeGen/Hexagon/pic-regusage.ll
index 9d3b6cec39e3..077063e36550 100644
--- a/test/CodeGen/Hexagon/pic-regusage.ll
+++ b/test/CodeGen/Hexagon/pic-regusage.ll
@@ -1,4 +1,4 @@
-; RUN: llc -mtriple=hexagon -relocation-model=pic < %s | FileCheck %s
+; RUN: llc -march=hexagon -mtriple=hexagon-unknown-linux-gnu -relocation-model=pic < %s | FileCheck %s
 
 ; Force the use of R14 (by clobbering everything else in the inline asm).
 ; Make sure that R14 is not set before the __save call (which will clobber
diff --git a/test/CodeGen/Hexagon/runtime-stkchk.ll b/test/CodeGen/Hexagon/runtime-stkchk.ll
index 66e93d02ef51..2f3f2ad10f6a 100644
--- a/test/CodeGen/Hexagon/runtime-stkchk.ll
+++ b/test/CodeGen/Hexagon/runtime-stkchk.ll
@@ -1,4 +1,4 @@
-; RUN: llc -mtriple=hexagon -mcpu=hexagonv55 -enable-stackovf-sanitizer < %s | FileCheck %s
+; RUN: llc -march=hexagon -mcpu=hexagonv55 -mtriple=hexagon-unknown-linux-gnu -enable-stackovf-sanitizer < %s | FileCheck %s
 
 ; CHECK-LABEL: foo_1
 ; CHECK: __runtime_stack_check
diff --git a/test/CodeGen/Hexagon/swp-memrefs-epilog.ll b/test/CodeGen/Hexagon/swp-memrefs-epilog.ll
index 20e39dd08fd7..b34dfbc31e9d 100644
--- a/test/CodeGen/Hexagon/swp-memrefs-epilog.ll
+++ b/test/CodeGen/Hexagon/swp-memrefs-epilog.ll
@@ -1,4 +1,4 @@
-; RUN: llc -march=hexagon -O2 -fp-contract=fast < %s -pipeliner-experimental-cg=true | FileCheck %s
+; RUN: llc -march=hexagon -mtriple=hexagon-unknown-linux-gnu -O2 -fp-contract=fast < %s -pipeliner-experimental-cg=true | FileCheck %s
 
 ; Test that the memoperands for instructions in the epilog are updated
 ; correctly. Previously, the pipeliner updated the offset for the memoperands
diff --git a/test/CodeGen/Hexagon/vararg-formal.ll b/test/CodeGen/Hexagon/vararg-formal.ll
index 6bba65fcab16..fb3132929bcf 100644
--- a/test/CodeGen/Hexagon/vararg-formal.ll
+++ b/test/CodeGen/Hexagon/vararg-formal.ll
@@ -1,4 +1,4 @@
-; RUN: llc -march=hexagon < %s | FileCheck %s
+; RUN: llc -march=hexagon -mtriple=hexagon-unknown-linux-gnu < %s | FileCheck %s
 
 ; Make sure that the first formal argument is not loaded from memory.
 ; CHECK-NOT: memw
-- 
2.36.0

