# Maintainer: Samuel Holland <samuel@sholland.org>
pkgname=rust
pkgver=1.76.0
_bootver=1.75.0-r0
_llvmver=18
pkgrel=0
pkgdesc="The Rust Programming Language"
url="https://www.rust-lang.org"
arch="all"
options="!check"  # Failures on aarch64 and ppc64.
license="(Apache-2.0 OR MIT) AND (NCSA OR MIT) AND BSD-2-Clause AND BSD-3-Clause"
depends="$pkgname-std=$pkgver-r$pkgrel gcc musl-dev"
makedepends="
	curl-dev
	llvm$_llvmver-dev
	llvm$_llvmver-test-utils
	openssl-dev
	python3
	cargo-bootstrap=$_bootver
	rust-bootstrap=$_bootver
	rustfmt-bootstrap=$_bootver
	zlib-dev
	"
provides="$pkgname-bootstrap=$pkgver-r$pkgrel"
subpackages="
	$pkgname-dbg
	$pkgname-std
	$pkgname-analysis
	$pkgname-doc
	$pkgname-gdb::noarch
	$pkgname-lldb::noarch
	$pkgname-src::noarch
	cargo
	cargo-clippy:_cargo_clippy
	cargo-fmt:_cargo_fmt
	cargo-doc:_cargo_doc:noarch
	cargo-bash-completion:_cargo_bashcomp:noarch
	cargo-zsh-completion:_cargo_zshcomp:noarch
	rustfmt
	"
source="https://static.rust-lang.org/dist/rustc-$pkgver-src.tar.xz
	0001-Fix-LLVM-build.patch
	0002-Fix-linking-to-zlib-when-cross-compiling.patch
	0003-Fix-rustdoc-when-cross-compiling-on-musl.patch
	0004-Remove-musl_root-and-CRT-fallback-from-musl-targets.patch
	0005-Prefer-libgcc_eh-over-libunwind-for-musl.patch
	0006-Link-libssp_nonshared.a-on-all-musl-targets.patch
	0007-test-failed-doctest-output-Fix-normalization.patch
	0008-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
	0009-Ignore-broken-and-non-applicable-tests.patch
	0010-Link-stage-2-tools-dynamically-to-libstd.patch
	0011-Move-debugger-scripts-to-usr-share-rust.patch
	0012-Add-foxkit-target-specs.patch
	powerpc-atomics.patch
	"
builddir="$srcdir/rustc-$pkgver-src"
_rlibdir="/usr/lib/rustlib/$CTARGET/lib"

build() {
	cat > config.toml <<- EOF
		changelog-seen = 2
		[build]
		doc-stage = 2
		build-stage = 2
		test-stage = 2
		build = "$CBUILD"
		host = [ "$CHOST" ]
		target = [ "$CTARGET" ]
		cargo = "/usr/bin/cargo"
		rustc = "/usr/bin/rustc"
		rustfmt = "/usr/bin/rustfmt"
		docs = true
		compiler-docs = false
		submodules = false
		python = "python3"
		locked-deps = true
		vendor = true
		extended = true
		tools = [ "analysis", "cargo", "clippy", "rustfmt", "src" ]
		verbose = 1
		sanitizers = false
		profiler = false
		cargo-native-static = false
		[install]
		prefix = "/usr"
		[rust]
		optimize = true
		debug = false
		codegen-units = 1
		debuginfo-level = 1
		debuginfo-level-rustc = 0
		debuginfo-level-tests = 0
		backtrace = true
		incremental = false
		parallel-compiler = false
		channel = "stable"
		description = "Adelie ${pkgver}-r${pkgrel}"
		rpath = false
		verbose-tests = true
		optimize-tests = true
		codegen-tests = true
		dist-src = false
		lld = false
		use-lld = false
		llvm-tools = false
		backtrace-on-ice = true
		remap-debuginfo = false
		jemalloc = false
		llvm-libunwind = "no"
		new-symbol-mangling = true
		[target.$CTARGET]
		cc = "$CTARGET-gcc"
		cxx = "$CTARGET-g++"
		ar = "ar"
		ranlib = "ranlib"
		linker = "$CTARGET-gcc"
		llvm-config = "/usr/lib/llvm$_llvmver/bin/llvm-config"
		crt-static = false
		[dist]
		src-tarball = false
		compression-formats = [ "xz" ]
	EOF

	LLVM_LINK_SHARED=1 \
	RUST_BACKTRACE=1 \
	python3 x.py dist -j ${JOBS:-2}
}

check() {
	LLVM_LINK_SHARED=1 \
	python3 x.py test -j ${JOBS:-2} --no-doc --no-fail-fast || true
}

package() {
	cd "$builddir"/build/dist

	tar xf rust-$pkgver-$CTARGET.tar.xz
	rust-$pkgver-$CTARGET/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--sysconfdir="$pkgdir"/etc \
		--disable-ldconfig
	tar xf rust-src-$pkgver.tar.xz
	rust-src-$pkgver/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--disable-ldconfig

	rm "$pkgdir"/usr/lib/rustlib/components \
	   "$pkgdir"/usr/lib/rustlib/install.log \
	   "$pkgdir"/usr/lib/rustlib/manifest-* \
	   "$pkgdir"/usr/lib/rustlib/rust-installer-version \
	   "$pkgdir"/usr/lib/rustlib/uninstall.sh
}

std() {
	pkgdesc="Standard library for Rust"
	depends="musl-utils"

	_mv "$pkgdir"$_rlibdir/*.so "$subpkgdir"$_rlibdir

	mkdir -p "$subpkgdir"/etc/ld.so.conf.d
	echo "$_rlibdir" > "$subpkgdir"/etc/ld.so.conf.d/$pkgname.conf
}

analysis() {
	pkgdesc="Compiler analysis data for the Rust standard library"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"${_rlibdir%/*}/analysis "$subpkgdir"${_rlibdir%/*}
}

gdb() {
	pkgdesc="GDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname gdb"
	install_if="$pkgname=$pkgver-r$pkgrel gdb"

	_mv "$pkgdir"/usr/bin/rust-gdb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/bin/rust-gdbgui "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/gdb_*.py "$subpkgdir"/usr/share/rust
}

lldb() {
	pkgdesc="LLDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname lldb py3-lldb"
	install_if="$pkgname=$pkgver-r$pkgrel lldb"

	_mv "$pkgdir"/usr/bin/rust-lldb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/lldb_*.py "$subpkgdir"/usr/share/rust
}

src() {
	pkgdesc="$pkgdesc (source code)"
	depends=""

	_mv "$pkgdir"/usr/lib/rustlib/src/rust "$subpkgdir"/usr/src
	rmdir -p "$pkgdir"/usr/lib/rustlib/src 2>/dev/null || true

	mkdir -p "$subpkgdir"/usr/lib/rustlib/src
	ln -s ../../../src/rust "$subpkgdir"/usr/lib/rustlib/src/rust
}

cargo() {
	pkgdesc="The Rust package manager"
	provides="cargo-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel $pkgname"

	_mv "$pkgdir"/usr/bin/cargo "$subpkgdir"/usr/bin
}

_cargo_clippy() {
	pkgdesc="A collection of Rust lints (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo"

	_mv "$pkgdir"/usr/bin/cargo-clippy \
	    "$pkgdir"/usr/bin/clippy-driver \
	    "$subpkgdir"/usr/bin
}

_cargo_fmt() {
	pkgdesc="Format Rust code (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo rustfmt"
	install_if="cargo=$pkgver-r$pkgrel rustfmt=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/cargo-fmt "$subpkgdir"/usr/bin
}

_cargo_bashcomp() {
	pkgdesc="Bash completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel bash-completion"

	_mv "$pkgdir"/etc/bash_completion.d/cargo \
	    "$subpkgdir"/usr/share/bash-completion/completions
	rmdir -p "$pkgdir"/etc/bash_completion.d 2>/dev/null || true
}

_cargo_zshcomp() {
	pkgdesc="ZSH completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel zsh"

	_mv "$pkgdir"/usr/share/zsh/site-functions/_cargo \
	    "$subpkgdir"/usr/share/zsh/site-functions/_cargo
	rmdir -p "$pkgdir"/usr/share/zsh/site-functions 2>/dev/null || true
}

_cargo_doc() {
	pkgdesc="The Rust package manager (documentation)"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel docs"

	# XXX: This is hackish!
	_mv "$pkgdir"/../$pkgname-doc/usr/share/man/man1/cargo* \
	    "$subpkgdir"/usr/share/man/man1
}

rustfmt() {
	pkgdesc="Format Rust code"
	provides="rustfmt-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/rustfmt "$subpkgdir"/usr/bin
}

_mv() {
	local dest; for dest; do true; done  # get last argument
	mkdir -p "$dest"
	mv "$@"
}

sha512sums="92e16cfdeb91bde341fe6c2774d92868275b07aa1d46d870ddc9291eadfe4ea9af93e06586fa7d6b8d60534903945cbbe706d354c90272712989c58d2bf174bf  rustc-1.76.0-src.tar.xz
259dcf71d17afafddceea8cb682a38f64772e83f18124070b7e522867be5262c8206455ed4b515954af5efa5b939f0065dfe26f0256dbc75a7bb38f41c5a0731  0001-Fix-LLVM-build.patch
1cb5b3eb10f9b3e05ab2ab30fa42a69009b30e7bd6a1ca81af6a5d4a7dfeebed2d5a3f2212ea72ec80b97ff374ffd0cbeba67de3265548ac3ac033dcaf5f7bc9  0002-Fix-linking-to-zlib-when-cross-compiling.patch
619a0150bdc59ef8d844e9eff907e51015003164d17012b38bcac642618efb65e25ea4b1ead06d4b023caf02d4eb8dbd481daa04b42d0002a9986f05854c0ca4  0003-Fix-rustdoc-when-cross-compiling-on-musl.patch
d171f308fe09e74cb0f4cde122b9115b21044d12b9ae408a85d2ba0551f198955a419fe36cd29220863943987c3052a5cee1a42faa25dce018e14cd03763c35f  0004-Remove-musl_root-and-CRT-fallback-from-musl-targets.patch
b9412b30b05ddffac82e7c0c727434f66b18149383ab0ec4571ed9c4e27bd80a6fedd907c34b4f72b76abc84ee0e43f6bfc4941679f6384e0a524e1535eca453  0005-Prefer-libgcc_eh-over-libunwind-for-musl.patch
b34187a82cd3db02b8aac8f18147d653a7191b841e145460dfbff518a7747a6aa1c08721703929bfc7e446c9ed9512383a5eefaad2cb5c02d16e8f00d12627db  0006-Link-libssp_nonshared.a-on-all-musl-targets.patch
47de5895cbb9787445bc5e94dcf36887baf3a3ac78730caa8d9bc245024b6ad591cf25cca79819dc3d5c8ca8c8af6d47e2631a7763797d4181a40e946ac97cd9  0007-test-failed-doctest-output-Fix-normalization.patch
6850174cebb3dfb2f41e277b0b46db9cabe18c904f39a6775f8cd12dc6c237a7de820a5dc8c538f9b965c069b4197141f0613add096574a7df4ca2d1ae01ab4d  0008-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
5742aebf7b0e63f38bb98bc19b8d6f045332569b8c3a10e42aa8290fb4b56f7a791789f971ab6088ca376f2863648022c3ae040887481e86df67dfd323ae65b5  0009-Ignore-broken-and-non-applicable-tests.patch
e8f6013806aab2b14f5d77c5a1ab42326260bcacdfa83b9f5426178fcb97d90befee4da51f772b0c78d514bef10ea43fca3a9d0b0c017b039851143e17b579b1  0010-Link-stage-2-tools-dynamically-to-libstd.patch
4c0d4353ab85354b22d09071020596846acb574f26f768667b96726a4bac2b59c868cff332dca188ad60ba34783cc4b7ba1677836307d63709b35ed9c92818bb  0011-Move-debugger-scripts-to-usr-share-rust.patch
07b9b127afb7d372e858e3095bf490ef61297c20fd09fbe7261b70714eb8abd9217824c2cee0fbde90995333baf3d688101e284e698f7ebc9672109eb76c20e4  0012-Add-foxkit-target-specs.patch
931a08d7714f28424506ea8d733c4d496810bc9992f11d6b3162e688ac36c3be3e25ece63b3d9ce26cf8858dc1231d826ab166ec992e61bc3649041b5d2d819b  powerpc-atomics.patch"
