# Maintainer: Samuel Holland <samuel@sholland.org>
pkgname=rust
pkgver=1.60.0
_bootver=1.60.0
_llvmver=14
pkgrel=0
pkgdesc="The Rust Programming Language"
url="https://www.rust-lang.org"
arch="all"
options="!check"  # Failures on aarch64 and ppc64.
license="(Apache-2.0 OR MIT) AND (NCSA OR MIT) AND BSD-2-Clause AND BSD-3-Clause"
depends="$pkgname-std=$pkgver-r$pkgrel gcc musl-dev"
makedepends="
	curl-dev
	llvm$_llvmver-dev
	llvm$_llvmver-test-utils
	openssl-dev
	python3
	zlib-dev
	"
provides="$pkgname-bootstrap=$pkgver-r$pkgrel"
subpackages="
	$pkgname-dbg
	$pkgname-std
	$pkgname-analysis
	$pkgname-doc
	$pkgname-gdb::noarch
	$pkgname-lldb::noarch
	$pkgname-src::noarch
	cargo
	cargo-clippy:_cargo_clippy
	cargo-fmt:_cargo_fmt
	cargo-doc:_cargo_doc:noarch
	cargo-bash-completion:_cargo_bashcomp:noarch
	cargo-zsh-completion:_cargo_zshcomp:noarch
	rls
	rustfmt
	"
_targets="
	aarch64-foxkit-linux-musl
	armv7-foxkit-linux-musleabihf
	i586-foxkit-linux-musl
	powerpc-foxkit-linux-musl
	powerpc64-foxkit-linux-musl
	x86_64-foxkit-linux-musl
	"
source="https://static.rust-lang.org/dist/rustc-$pkgver-src.tar.xz
	0001-Fix-LLVM-build.patch
	0002-Fix-linking-to-zlib-when-cross-compiling.patch
	0003-Fix-rustdoc-when-cross-compiling-on-musl.patch
	0004-Use-static-native-libraries-when-linking-static-exec.patch
	0005-Remove-musl_root-and-CRT-fallback-from-musl-targets.patch
	0006-Prefer-libgcc_eh-over-libunwind-for-musl.patch
	0007-Link-libssp_nonshared.a-on-all-musl-targets.patch
	0008-test-failed-doctest-output-Fix-normalization.patch
	0009-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
	0010-test-use-extern-for-plugins-Don-t-assume-multilib.patch
	0011-Ignore-broken-and-non-applicable-tests.patch
	0012-Link-stage-2-tools-dynamically-to-libstd.patch
	0013-Move-debugger-scripts-to-usr-share-rust.patch
	0014-Add-foxkit-target-specs.patch
	0015-Use-OpenPOWER-ABI-on-BE-PowerPC-64-musl.patch
	0040-rls-atomics.patch
	powerpc-atomics.patch
	"
for t in $_targets; do
	source="$source
		https://distfiles.adelielinux.org/source/rust/cargo-$_bootver-$t.tar.xz
		https://distfiles.adelielinux.org/source/rust/rust-std-$_bootver-$t.tar.xz
		https://distfiles.adelielinux.org/source/rust/rustc-$_bootver-$t.tar.xz
		"
done
builddir="$srcdir/rustc-$pkgver-src"
_rlibdir="/usr/lib/rustlib/$CTARGET/lib"

prepare() {
	default_prepare

	$srcdir/cargo-$_bootver-$CBUILD/install.sh \
		--destdir=$srcdir/prebuilt \
		--prefix=/ \
		--disable-ldconfig
	$srcdir/rust-std-$_bootver-$CBUILD/install.sh \
		--destdir=$srcdir/prebuilt \
		--prefix=/ \
		--disable-ldconfig
	$srcdir/rustc-$_bootver-$CBUILD/install.sh \
		--destdir=$srcdir/prebuilt \
		--prefix=/ \
		--disable-ldconfig
}

build() {
	cat > config.toml <<- EOF
		changelog-seen = 2
		[build]
		doc-stage = 2
		build-stage = 2
		test-stage = 2
		build = "$CBUILD"
		host = [ "$CHOST" ]
		target = [ "$CTARGET" ]
		cargo = "$srcdir/prebuilt/bin/cargo"
		rustc = "$srcdir/prebuilt/bin/rustc"
		rustfmt = "$srcdir/prebuilt/bin/rustfmt"
		docs = true
		compiler-docs = false
		submodules = false
		python = "python3"
		locked-deps = true
		vendor = true
		extended = true
		tools = [ "analysis", "cargo", "clippy", "rls", "rustfmt", "src" ]
		verbose = 1
		sanitizers = false
		profiler = false
		cargo-native-static = false
		[install]
		prefix = "/usr"
		[rust]
		optimize = true
		debug = false
		codegen-units = 1
		debuginfo-level = 2
		debuginfo-level-rustc = 0
		debuginfo-level-tests = 0
		backtrace = true
		incremental = false
		parallel-compiler = false
		channel = "stable"
		description = "Adelie ${pkgver}-r${pkgrel}"
		rpath = false
		verbose-tests = true
		optimize-tests = true
		codegen-tests = true
		dist-src = false
		lld = false
		use-lld = false
		llvm-tools = false
		backtrace-on-ice = true
		remap-debuginfo = false
		jemalloc = false
		llvm-libunwind = "no"
		new-symbol-mangling = true
		[target.$CTARGET]
		cc = "$CTARGET-gcc"
		cxx = "$CTARGET-g++"
		ar = "ar"
		ranlib = "ranlib"
		linker = "$CTARGET-gcc"
		llvm-config = "/usr/lib/llvm$_llvmver/bin/llvm-config"
		crt-static = false
		[dist]
		src-tarball = false
		compression-formats = [ "xz" ]
	EOF

	LLVM_LINK_SHARED=1 \
	RUST_BACKTRACE=1 \
	python3 x.py dist -j ${JOBS:-2}
}

check() {
	LLVM_LINK_SHARED=1 \
	python3 x.py test -j ${JOBS:-2} --no-doc --no-fail-fast
}

package() {
	cd "$builddir"/build/dist

	tar xf rust-$pkgver-$CTARGET.tar.xz
	rust-$pkgver-$CTARGET/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--sysconfdir="$pkgdir"/etc \
		--disable-ldconfig
	tar xf rust-src-$pkgver.tar.xz
	rust-src-$pkgver/install.sh \
		--destdir="$pkgdir" \
		--prefix=/usr \
		--disable-ldconfig

	rm "$pkgdir"/usr/lib/rustlib/components \
	   "$pkgdir"/usr/lib/rustlib/install.log \
	   "$pkgdir"/usr/lib/rustlib/manifest-* \
	   "$pkgdir"/usr/lib/rustlib/rust-installer-version \
	   "$pkgdir"/usr/lib/rustlib/uninstall.sh
}

std() {
	pkgdesc="Standard library for Rust"
	depends="musl-utils"

	_mv "$pkgdir"$_rlibdir/*.so "$subpkgdir"$_rlibdir

	mkdir -p "$subpkgdir"/etc/ld.so.conf.d
	echo "$_rlibdir" > "$subpkgdir"/etc/ld.so.conf.d/$pkgname.conf
}

analysis() {
	pkgdesc="Compiler analysis data for the Rust standard library"
	depends="$pkgname=$pkgver-r$pkgrel $pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"${_rlibdir%/*}/analysis "$subpkgdir"${_rlibdir%/*}
}

gdb() {
	pkgdesc="GDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname gdb"
	install_if="$pkgname=$pkgver-r$pkgrel gdb"

	_mv "$pkgdir"/usr/bin/rust-gdb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/bin/rust-gdbgui "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/gdb_*.py "$subpkgdir"/usr/share/rust
}

lldb() {
	pkgdesc="LLDB pretty printers for Rust"
	license="Apache-2.0 OR MIT"
	depends="$pkgname lldb py3-lldb"
	install_if="$pkgname=$pkgver-r$pkgrel lldb"

	_mv "$pkgdir"/usr/bin/rust-lldb "$subpkgdir"/usr/bin
	_mv "$pkgdir"/usr/share/rust/lldb_*.py "$subpkgdir"/usr/share/rust
}

src() {
	pkgdesc="$pkgdesc (source code)"
	depends=""

	_mv "$pkgdir"/usr/lib/rustlib/src/rust "$subpkgdir"/usr/src
	rmdir -p "$pkgdir"/usr/lib/rustlib/src 2>/dev/null || true

	mkdir -p "$subpkgdir"/usr/lib/rustlib/src
	ln -s ../../../src/rust "$subpkgdir"/usr/lib/rustlib/src/rust
}

cargo() {
	pkgdesc="The Rust package manager"
	provides="cargo-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel $pkgname"

	_mv "$pkgdir"/usr/bin/cargo "$subpkgdir"/usr/bin
}

_cargo_clippy() {
	pkgdesc="A collection of Rust lints (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo"

	_mv "$pkgdir"/usr/bin/cargo-clippy \
	    "$pkgdir"/usr/bin/clippy-driver \
	    "$subpkgdir"/usr/bin
}

_cargo_fmt() {
	pkgdesc="Format Rust code (cargo plugin)"
	depends="$pkgname-std=$pkgver-r$pkgrel cargo rustfmt"
	install_if="cargo=$pkgver-r$pkgrel rustfmt=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/cargo-fmt "$subpkgdir"/usr/bin
}

_cargo_bashcomp() {
	pkgdesc="Bash completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel bash-completion"

	_mv "$pkgdir"/etc/bash_completion.d/cargo \
	    "$subpkgdir"/usr/share/bash-completion/completions
	rmdir -p "$pkgdir"/etc/bash_completion.d 2>/dev/null || true
}

_cargo_zshcomp() {
	pkgdesc="ZSH completion for cargo"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel zsh"

	_mv "$pkgdir"/usr/share/zsh/site-functions/_cargo \
	    "$subpkgdir"/usr/share/zsh/site-functions/_cargo
	rmdir -p "$pkgdir"/usr/share/zsh/site-functions 2>/dev/null || true
}

_cargo_doc() {
	pkgdesc="The Rust package manager (documentation)"
	license="Apache-2.0 OR MIT"
	depends=""
	install_if="cargo=$pkgver-r$pkgrel docs"

	# XXX: This is hackish!
	_mv "$pkgdir"/../$pkgname-doc/usr/share/man/man1/cargo* \
	    "$subpkgdir"/usr/share/man/man1
}

rls() {
	pkgdesc="The Rust language server"
	license="Apache-2.0 OR MIT"
	depends="$pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/rls "$subpkgdir"/usr/bin
}

rustfmt() {
	pkgdesc="Format Rust code"
	provides="rustfmt-bootstrap=$pkgver-r$pkgrel"
	depends="$pkgname-std=$pkgver-r$pkgrel"

	_mv "$pkgdir"/usr/bin/rustfmt "$subpkgdir"/usr/bin
}

_mv() {
	local dest; for dest; do true; done  # get last argument
	mkdir -p "$dest"
	mv "$@"
}

sha512sums="d0c113e8c2c67bf10773c9403dc4c4700c4deb2fb287bfec51e565d3473d2b481d8ae2c90b272cd67b3a87d7443ea25a34c7b40ba8cd7106bf5d71126ab141c3  rustc-1.60.0-src.tar.xz
c65d52bbc3745fcc5c4fd314cc7f00be6778579983002d3f8d173611d034686293f318b07e3327d2d09d2e81f092c2f287d6aede7494e94f12e99d2575875e27  0001-Fix-LLVM-build.patch
e28d0f44ab0b4af68ed8fbef394ca362bdbbfee7011342bfd5e4195069e716b581bd50e21494ce51f6d8f7a4797d1342fb807c910a275fb394ca503f345a8664  0002-Fix-linking-to-zlib-when-cross-compiling.patch
7d174cd27e618212e534459455b9017636f56e9a9db81413a714f718a988a9d1fc7b4c0b98d4a2f0b1971d662c7df98be370e0d10e81d3e50d37589fe3b671fa  0003-Fix-rustdoc-when-cross-compiling-on-musl.patch
0ea1605f7e7b94a317cead8b2552fa0d95b6bd41a4727d892fa284ccb5b718aa32a625abcaa4c651a324437c4c06e53da464c6cb21518663bda05ded4ba12815  0004-Use-static-native-libraries-when-linking-static-exec.patch
dad4fc3ece2e093ed90e10b04a7c1fcdcec3e6fcfc5818401db19ec20365f581dc2ed1f771abca64e24edf3e6bc9c67fbecb85e1470a00359a00a47eab3b45f6  0005-Remove-musl_root-and-CRT-fallback-from-musl-targets.patch
65116731811c178e980b1d980d524645897bc3f81e9f9ad8051235fa4bad7bbf20519173057e9d7b3f9c8459cc35dd2902c7ccd9e5070587848ea9886793ea35  0006-Prefer-libgcc_eh-over-libunwind-for-musl.patch
21550b68581c46847ccd5fb20551441b8ce41e948a596bfaffa6f81fcb1a273756e76bcf7ac033241b248b93661a09a9962ae7ffd93b7850d13e5e51695ec705  0007-Link-libssp_nonshared.a-on-all-musl-targets.patch
9242b001e8c4a9ae5a7a726e43724c3e07813e3105fae50efab5f289f15fb6493cb6f15694f5a0289d5a7effc0704f2fe715af981c50d54a805393c0c72e2cf7  0008-test-failed-doctest-output-Fix-normalization.patch
45bcce759138df475f8fbeb8089420bd38a399f5a018ebdf6d5a79e7c714c9ec770c765c32dda0ab4e368c5dd226f474b6894d70b14e41bea57284eeeb1a2f58  0009-test-sysroot-crates-are-unstable-Fix-test-when-rpath.patch
23f622841ba2030b02f52ed560c4be6800469e8021aa71842a702860c97b90edadf5de593ea4f6b9a158d7d0e4c83b52b419f770a8dac2b69633d643468c54d5  0010-test-use-extern-for-plugins-Don-t-assume-multilib.patch
73282af6c50155a29c2c98117a1ce6d28be42d04c25dc8ea2388de6923361fb7ae44d16bb81c57bb3d326d434f11ae6cdf72b8a0293421e8d0eaa931398e52d3  0011-Ignore-broken-and-non-applicable-tests.patch
2f139d1d544b2f1248f352bf3cadb93ca5935338a671480045979579ebab8f0f6b8f7509256fb0f31083291c58310f83c32b9034ca2229dd6dbacb6a743eae74  0012-Link-stage-2-tools-dynamically-to-libstd.patch
62b08c73e62da98fa02c26f47a5009f9ebcab95090d51cc55ae005827f366de5b556c32c995f55f9025fadeff4e0485ed0908526845a02abe5ae8b1b86839130  0013-Move-debugger-scripts-to-usr-share-rust.patch
530bae858f9db0049f4b4666afb05cadf25f37181597274a7a456fc51b32f04523da75ee7ab9ab1d8226c7e7ddd2b771bb9d2052cab430e482d2d1ee6b34eafc  0014-Add-foxkit-target-specs.patch
7fee0667793d5d5ee5cb600e24129c81de50511814e8f1a2f16bb47350087c5f42a01671415b10b01e414e9ff03008892a35c6cb616737175217bdccd1fa3f1e  0015-Use-OpenPOWER-ABI-on-BE-PowerPC-64-musl.patch
a2a05a64ca57dcde23c11ed8bd6ce530d80b99464c895c256b4e9cba02997121205a091de43f516563d5d618ea522d89639507ba94961807ada743a3099fe8d8  0040-rls-atomics.patch
99db0614b821a1ef767f68396253f7a620a491adf29ea01887cf4614e4dbb63c2544e1d7a6ad63a629995a2093cb26b4013f9a321dab65a235187e825c51c6a8  powerpc-atomics.patch
d9140531c720f128371a7abe1b15dcafd44859e52ef64c18cbebad16fa60bb928671eced22ef2b6ec46e2826bc7a1f4e1eeffcd5f187b26b74b41e543d515a01  cargo-1.60.0-aarch64-foxkit-linux-musl.tar.xz
b620eb8855190bb6d56d79decfef1bc90f073fb11f61cf236443c43b774502499735cc18b778886df226bdffbb834bc39ef8c910877849600377c97d0ba33cee  rust-std-1.60.0-aarch64-foxkit-linux-musl.tar.xz
d436a1b5bccac1917ba8ef3110c66e81d146369833729081ffb56004fbbde2becee871dc636f13b722517fb443e74d8f532c7eaa92f8e1f65608c9f5bd2d5ab3  rustc-1.60.0-aarch64-foxkit-linux-musl.tar.xz
114063a3624c2c7bc65f4176209baa13cf633a64b75fc7c962cc999f5caf3ee5b9f60e3ad2c6a93df3ea1e00ac45259ad076d8ec04a0e4d9511b21c804ae5eb5  cargo-1.60.0-armv7-foxkit-linux-musleabihf.tar.xz
9819a4f9e7c8125896cd42fed02e84678218937cd0a8758e5b16011d4486305211b777c191cc1f9ac68f3100b7b94bd6a874a3f5f63400851bac90db055c9f6b  rust-std-1.60.0-armv7-foxkit-linux-musleabihf.tar.xz
40a57b5a05e31145e7664db674c8793ab874cd651bcd7577aea9225904413f085086102abfe244bdc78b19b8d6e6d0bc3a7009294b9021a1befa8f2f0216f68d  rustc-1.60.0-armv7-foxkit-linux-musleabihf.tar.xz
9c8679b6611a4b356e500fe4f4b1b3523db09ddf9abec9b39ca2e0436a0b34558790348f7594132a8a5d6b8d4390a24252ae9afa7c16962f7b3609fa970c9775  cargo-1.60.0-i586-foxkit-linux-musl.tar.xz
a37fa51e63cfbb357b197f8c4d0ba3673e8eee5bd788cca8a79836248451f6e34304bc93960b758848089e2c99d6ae8fd20ad9f9838af53b2243d396666f6ea9  rust-std-1.60.0-i586-foxkit-linux-musl.tar.xz
b66f58da3a9f72a051342df3e57e4a204f44570c5e05c2bd2e00d22051403fac57f0edbaf595d05f1d57ac91dcb5bdab01a32c081b9b4d45c9768f58fff85a0e  rustc-1.60.0-i586-foxkit-linux-musl.tar.xz
649c554666a2ed39db0176daf1f76ea51093cf2aa56c692830cff22c9e79ce622be9117c1026efed5a0fdcbe65efe57f5233cc2e88126dc708527f8ee9d03687  cargo-1.60.0-powerpc-foxkit-linux-musl.tar.xz
f262de70c936bc33ec4836a66f0320ca0a99b19c8996174dba2596a4d373cfe6e19acfe12b255fbd0fe54fe2a8a5ca31e85f7974c3504d58b219f6bca6164efa  rust-std-1.60.0-powerpc-foxkit-linux-musl.tar.xz
f52d03674577fca4b48f9ea3d5d60170a63a4f378ab8cdffca62bb3c6e9850371ad37d125bdf155e15edae1ca7a86ef7bef6ec02216755a96cef35f3a98cdb50  rustc-1.60.0-powerpc-foxkit-linux-musl.tar.xz
8a70eec1ccbad10022b6bbdd4c2be096130fdccad776f21608c074653afc3dc68aee0ee2475d9df877c447109584d36f94936dc22eead3700b6cee59ac5ae7e8  cargo-1.60.0-powerpc64-foxkit-linux-musl.tar.xz
72848344807e1dff8b872848c7449631397f8167619a6916ffeb4e0a43ecc394ec32c407cf2c7a85613690abba1d39087568a307f99e0e5a9afe656133c85765  rust-std-1.60.0-powerpc64-foxkit-linux-musl.tar.xz
7ce9d9b10e50e96b377fe1e3e1bab2b422432b234fe217c2df5bbd766d384bdeceb015e0a847a45ab2906144e09cdd2d95857941a4f46ede78597d04438e7aa5  rustc-1.60.0-powerpc64-foxkit-linux-musl.tar.xz
249322775192152da6578122cd608cc2649edbbb9df54ea1132b2ec0b353b4ad9e2ac3295c8fb32777b3a255a4a8569feca4a63f5e1723299f53265a9d32945a  cargo-1.60.0-x86_64-foxkit-linux-musl.tar.xz
c55fb5896fdabd091f14dfdb872cec249358dfffe8a5fd8987a3ee193cdfc741dd47e5577718e00386abf4015e0717747cc377d11a467f8774a4d0e9cd4657a9  rust-std-1.60.0-x86_64-foxkit-linux-musl.tar.xz
f63c79a5f51c5369747d2bc3bccd06ddac3d2c36b68fd058f499217d64abddad00902c5b59be4601bd587ff9761fc95f9af8a6182868b125d9a9fdae80db4ade  rustc-1.60.0-x86_64-foxkit-linux-musl.tar.xz"
