From 741978c5a73185b817cb2052413c0ef644dfad16 Mon Sep 17 00:00:00 2001
From: Samuel Holland <samuel@sholland.org>
Date: Fri, 8 Sep 2017 00:05:18 -0500
Subject: [PATCH 04/14] Use static native libraries when linking static executables

On ELF targets like Linux, gcc/ld will create a dynamically-linked
executable without warning, even when passed `-static`, when asked to
link to a `.so`. Avoid this confusing and unintended behavior by always
using the static version of libraries when trying to link static
executables.
---
 compiler/rustc_codegen_ssa/src/back/link.rs | 20 ++++++++++++++++----
 1 file changed, 16 insertions(+), 4 deletions(-)

diff --git a/compiler/rustc_codegen_ssa/src/back/link.rs b/compiler/rustc_codegen_ssa/src/back/link.rs
index 6cce95427463c..ef3fcef87e2a0 100644
--- a/compiler/rustc_codegen_ssa/src/back/link.rs
+++ b/compiler/rustc_codegen_ssa/src/back/link.rs
@@ -2064,7 +2064,7 @@ fn linker_with_args<'a>(
     // together with their respective upstream crates, and in their originally specified order.
     // This may be slightly breaking due to our use of `--as-needed` and needs a crater run.
     if sess.opts.unstable_opts.link_native_libraries {
-        add_upstream_native_libraries(cmd, sess, codegen_results);
+        add_upstream_native_libraries(cmd, sess, codegen_results, crate_type);
     }
 
     // Link with the import library generated for any raw-dylib functions.
@@ -2705,8 +2705,7 @@ fn add_upstream_rust_crates<'a>(
     }
 }
 
-/// Link in all of our upstream crates' native dependencies. Remember that all of these upstream
-/// native dependencies are all non-static dependencies. We've got two cases then:
+/// Link in all of our upstream crates' native dependencies. We have two cases:
 ///
 /// 1. The upstream crate is an rlib. In this case we *must* link in the native dependency because
 /// the rlib is just an archive.
@@ -2724,6 +2723,7 @@ fn add_upstream_native_libraries(
     cmd: &mut dyn Linker,
     sess: &Session,
     codegen_results: &CodegenResults,
+    crate_type: CrateType,
 ) {
     let mut last = (None, NativeLibKind::Unspecified, None);
     for &cnum in &codegen_results.crate_info.used_crates {
@@ -2748,7 +2748,19 @@ fn add_upstream_native_libraries(
                 NativeLibKind::Dylib { as_needed } => {
                     cmd.link_dylib(name, verbatim, as_needed.unwrap_or(true))
                 }
-                NativeLibKind::Unspecified => cmd.link_dylib(name, verbatim, true),
+                NativeLibKind::Unspecified => {
+                    // On some targets, like Linux, linking a static executable inhibits using
+                    // dylibs at all. Force native libraries to be static, even if for example
+                    // an upstream rlib was originally linked against a native shared library.
+                    if crate_type == config::CrateType::Executable
+                        && sess.crt_static(Some(crate_type))
+                        && !sess.target.options.crt_static_allows_dylibs
+                    {
+                        cmd.link_staticlib(name, verbatim)
+                    } else {
+                        cmd.link_dylib(name, verbatim, true)
+                    }
+                },
                 NativeLibKind::Framework { as_needed } => {
                     cmd.link_framework(name, as_needed.unwrap_or(true))
                 }
